public abstract class Creature extends Thing
{
	protected Thing stomach = null;
	
	// Constructor
	public Creature(String newName) 
	{
		super(newName);
	}

	public void eat(Thing aThing)
	{
		System.out.println(this.toString() + " has just eaten a " + aThing.toString() + ".");
		stomach = aThing;
	}
	
	public abstract void move();
	
	public void whatDidYouEat()
	{
		if (stomach == null)
		{
			System.out.println(this.toString() + " has had nothing to eat!");
		}
		else
		{
			System.out.println(this.toString() + " has eaten a " + stomach.toString() + ".");
		}
	}
}
