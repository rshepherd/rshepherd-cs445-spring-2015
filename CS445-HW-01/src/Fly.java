public class Fly extends Creature implements Flyer
{
	// Constructor
	public Fly(String newName) 
	{
		super(newName);
	}
	
	public void eat(Thing aThing)
	{
		if (aThing instanceof Creature) // can't eat a creature
		{
			System.out.println(this.toString() + " won\'t eat a " + aThing.toString() + ".");
		}
		else // can eat a Thing
		{
			super.eat(aThing);
		}
	}
	
	public void move()
	{
		fly();
	}
	
	public void fly()
	{
		System.out.println(this.toString() + " is buzzing around in flight.");
	}

}
