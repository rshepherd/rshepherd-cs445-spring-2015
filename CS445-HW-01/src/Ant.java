public class Ant extends Creature
{
	// Constructor
	public Ant(String newName) 
	{
		super(newName);
	}
	
	public void move()
	{
		System.out.println(this.toString() + " is crawling around.");
	}
}
