public class Bat extends Creature implements Flyer
{
	// Constructor
	public Bat(String newName) 
	{
		super(newName);
	}
	
	public void eat(Thing aThing)
	{
		if (aThing instanceof Creature) // can eat a Creature
		{
			super.eat(aThing);
		}
		else // can't eat a Thing
		{
			System.out.println(this.toString() + " won\'t eat a " + aThing.toString() + ".");
		}
	}
	
	public void move()
	{
		fly();
	}
	
	public void fly()
	{
		System.out.println(this.toString() + " is swooping through the dark.");
	}

}
