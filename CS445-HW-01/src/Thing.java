public class Thing {
	private String name;
	
	// Constructor
	public Thing(String newName)
	{
		name = newName;
	}
	
	// Accessor(s)
	public String getName()
	{
		return name;
	}

	public String toString()
	{
		if (!(this instanceof Creature))
		{
			return name;
		}
		else
		{
			String className = this.getClass().getSimpleName();
			return name + " " + className;
		}
	}
}
