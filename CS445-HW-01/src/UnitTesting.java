import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.*;
import static org.junit.Assert.*;

public class UnitTesting {

	// Classes
	Thing thing1       = new Thing("myThing1");
	Thing thingFood    = new Thing("thingFood");
	Ant   creatureFood = new Ant("creatureFood");
	Ant   ant          = new Ant("myAnt");
	Bat   bat          = new Bat("myBat");
	Fly   fly          = new Fly("myFly");
	Tiger tiger        = new Tiger("myTiger");
	
	// Captures the output for comparison for unit testing
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();
	String lineSep = System.getProperty("line.separator");
	
	@Before
	public void setUpStreams()
	{
		// redirects output to our byte array so we can compare it
		System.setOut(new PrintStream(output));
	}
	
	@Test
	public void TestThing() // getName(), toString()
	{
		assertEquals(thing1.getName(), "myThing1");
		assertEquals(thing1.toString(), "myThing1");
	}
	
	@Test
	public void TestAnt() // move(), eat(), whatDidYouEat(), etc.
	{
		// move
		ant.move();
		assertEquals(output.toString(), "myAnt Ant is crawling around." + lineSep); output.reset();
		
		// eat & whatDidYouEat (nothing in stomach)
		ant.whatDidYouEat();
		assertEquals(output.toString(), "myAnt Ant has had nothing to eat!" + lineSep); output.reset();
		
		// eat & whatDidYouEat (thing in stomach)
		ant.eat(thingFood);
		assertEquals(output.toString(), "myAnt Ant has just eaten a thingFood." + lineSep); output.reset();
		ant.whatDidYouEat();
		assertEquals(output.toString(), "myAnt Ant has eaten a thingFood." + lineSep); output.reset();
		
		// eat & whatDidYouEat (creature in stomach)
		ant.eat(creatureFood);
		assertEquals(output.toString(), "myAnt Ant has just eaten a creatureFood Ant." + lineSep); output.reset();
		ant.whatDidYouEat();
		assertEquals(output.toString(), "myAnt Ant has eaten a creatureFood Ant." + lineSep); output.reset();
		
		// Thing methods
		assertEquals(ant.getName(), "myAnt");
		assertEquals(ant.toString(), "myAnt Ant");
	}
	
	@Test
	public void TestBat() // move(), eat(), whatDidYouEat(), etc.
	{
		// move
		bat.move();
		assertEquals(output.toString(), "myBat Bat is swooping through the dark." + lineSep); output.reset();
		
		// eat & whatDidYouEat (nothing in stomach)
		bat.whatDidYouEat();
		assertEquals(output.toString(), "myBat Bat has had nothing to eat!" + lineSep); output.reset();
		
		// eat & whatDidYouEat (thing in stomach)
		bat.eat(thingFood);
		assertEquals(output.toString(), "myBat Bat won\'t eat a thingFood." + lineSep); output.reset();
		bat.whatDidYouEat();
		assertEquals(output.toString(), "myBat Bat has had nothing to eat!" + lineSep); output.reset();
		
		// eat & whatDidYouEat (creature in stomach)
		bat.eat(creatureFood);
		assertEquals(output.toString(), "myBat Bat has just eaten a creatureFood Ant." + lineSep); output.reset();
		bat.whatDidYouEat();
		assertEquals(output.toString(), "myBat Bat has eaten a creatureFood Ant." + lineSep); output.reset();
		
		// Thing methods
		assertEquals(bat.getName(), "myBat");
		assertEquals(bat.toString(), "myBat Bat");
	}
	
	@Test
	public void TestFly() // move(), eat(), whatDidYouEat(), etc.
	{
		// move
		fly.move();
		assertEquals(output.toString(), "myFly Fly is buzzing around in flight." + lineSep); output.reset();
		
		// eat & whatDidYouEat (nothing in stomach)
		fly.whatDidYouEat();
		assertEquals(output.toString(), "myFly Fly has had nothing to eat!" + lineSep); output.reset();
		
		// eat & whatDidYouEat (thing in stomach)
		fly.eat(thingFood);
		assertEquals(output.toString(), "myFly Fly has just eaten a thingFood." + lineSep); output.reset();
		fly.whatDidYouEat();
		assertEquals(output.toString(), "myFly Fly has eaten a thingFood." + lineSep); output.reset();
		
		// eat & whatDidYouEat (creature in stomach)
		fly.eat(creatureFood);
		assertEquals(output.toString(), "myFly Fly won\'t eat a creatureFood Ant." + lineSep); output.reset();
		fly.whatDidYouEat();
		assertEquals(output.toString(), "myFly Fly has eaten a thingFood." + lineSep); output.reset();
		
		// Thing methods
		assertEquals(fly.getName(), "myFly");
		assertEquals(fly.toString(), "myFly Fly");
	}
	
	@Test
	public void TestTiger() // move(), eat(), whatDidYouEat(), etc.
	{
		// move
		tiger.move();
		assertEquals(output.toString(), "myTiger Tiger has just pounced." + lineSep); output.reset();
		
		// eat & whatDidYouEat (nothing in stomach)
		tiger.whatDidYouEat();
		assertEquals(output.toString(), "myTiger Tiger has had nothing to eat!" + lineSep); output.reset();
		
		// eat & whatDidYouEat (thing in stomach)
		tiger.eat(thingFood);
		assertEquals(output.toString(), "myTiger Tiger has just eaten a thingFood." + lineSep); output.reset();
		tiger.whatDidYouEat();
		assertEquals(output.toString(), "myTiger Tiger has eaten a thingFood." + lineSep); output.reset();
		
		// eat & whatDidYouEat (creature in stomach)
		tiger.eat(creatureFood);
		assertEquals(output.toString(), "myTiger Tiger has just eaten a creatureFood Ant." + lineSep); output.reset();
		tiger.whatDidYouEat();
		assertEquals(output.toString(), "myTiger Tiger has eaten a creatureFood Ant." + lineSep); output.reset();
		
		// Thing methods
		assertEquals(tiger.getName(), "myTiger");
		assertEquals(tiger.toString(), "myTiger Tiger");
	}
	
	@Test
	public void TestMain()
	{
		// main method is already a test of sorts... should be fine if there's no error thrown
		TestCreature.main(null);
	}
	
}
