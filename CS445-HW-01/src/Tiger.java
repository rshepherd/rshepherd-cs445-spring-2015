public class Tiger extends Creature
{
	// Constructor
	public Tiger(String newName) 
	{
		super(newName);
	}
	
	public void move()
	{
		System.out.println(this.toString() + " has just pounced.");
	}
}
