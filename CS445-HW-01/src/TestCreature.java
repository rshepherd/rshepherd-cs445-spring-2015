public class TestCreature
{
	public static void main(String[] args)
	{
		final int THING_COUNT = 10;
		final int CREATURE_COUNT = 6;
		
		Thing[] thingArray = new Thing[THING_COUNT];
		Creature[] creatureArray = new Creature[CREATURE_COUNT];
		
		// Add Things and Creatures
		for (int i = 0; i < 8; i++)
		{
			int num = (i/4 +1);
			switch(i%4)
			{
				case(0):
					thingArray[i] = new Thing("Rock" + num);
					break;
				case(1):
					thingArray[i] = new Thing("Book" + num);
					break;
				case(2):
					thingArray[i] = new Thing("Fruit" + num);
					break;
				default:
					thingArray[i] = new Thing("Lightbulb" + num);
					break;
			}
		}
		creatureArray[0] = new Ant("Adam");
		creatureArray[1] = new Bat("Billy");
		creatureArray[2] = new Fly("Felix");
		creatureArray[3] = new Tiger("Tigger");
		
		// Print validation messages
		System.out.println("Things:\n");
		int itr8r = 0;
		for (itr8r = 0; itr8r < THING_COUNT; itr8r++)
		{
			if (thingArray[itr8r] != null)
				System.out.println(thingArray[itr8r].toString());
		}
		System.out.println("\nCreatures:\n");
		for (itr8r = 0; itr8r < CREATURE_COUNT; itr8r++)
		{
			if (creatureArray[itr8r] != null)
				System.out.println(creatureArray[itr8r].toString());
		}		
		
		// Testing creature methods
		System.out.println("\nTesting move():\n");
		for (itr8r = 0; itr8r < CREATURE_COUNT; itr8r++)
		{
			if (creatureArray[itr8r] != null)
				creatureArray[itr8r].move();
		}
		System.out.println("\nTesting eat() and whatDidYouEat():\n");
		for (itr8r = 0; itr8r < CREATURE_COUNT; itr8r++)
		{
			if (itr8r%2 == 0)
			{
				if (creatureArray[itr8r] != null)
				{
					creatureArray[itr8r].whatDidYouEat();
					if (thingArray[0] != null)
					{
						creatureArray[itr8r].eat(thingArray[0]);
						creatureArray[itr8r].whatDidYouEat();
					}
					creatureArray[itr8r].eat(creatureArray[0]);
					creatureArray[itr8r].whatDidYouEat();
				}
			}
			else
			{
				if (creatureArray[itr8r] != null)
				{
					creatureArray[itr8r].whatDidYouEat();
					creatureArray[itr8r].eat(creatureArray[0]);
					creatureArray[itr8r].whatDidYouEat();
					if (thingArray[0] != null)
					{
						creatureArray[itr8r].eat(thingArray[0]);
						creatureArray[itr8r].whatDidYouEat();
					}
				}
			}
		}
		System.out.println();
	}
	
}
