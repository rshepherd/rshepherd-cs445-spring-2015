package test.java;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import main.java.classes.*;
import main.java.wineClub.Vin;

import org.junit.Before;
import org.junit.Test;

public class ClassesTest
{	
	// Captures the output for comparison for unit testing
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();
	String lineSep = System.getProperty("line.separator");
	
	@Before
	public void setUpStreams()
	{
		// redirects output to our byte array so we can compare it
		System.setOut(new PrintStream(output));
	}
		
	@Test
	public void testDelivery()
	{
		Delivery deliv = new Delivery(DayOfWeek.M, TimeOfDay.A);
		Delivery deli2 = new Delivery(DayOfWeek.M, TimeOfDay.M);
		
		assertEquals(deliv.getDOW().compareTo(deli2.getDOW()) == 0, true);
		deliv.setDOW(DayOfWeek.getDOW("R"));
		assertEquals(deliv.getDOW().compareTo(deli2.getDOW()) != 0, true);
		//default case
		deliv.setDOW(DayOfWeek.getDOW("Rsday"));
		assertEquals(deliv.getDOW().equals(DayOfWeek.M), true);
		assertEquals(deliv.getTOD().compareTo(deli2.getTOD()) != 0, true);
		deliv.setTOD(TimeOfDay.getTOD("morning"));
		assertEquals(deliv.getTOD().compareTo(deli2.getTOD()) != 0, false);
		deliv.setTOD(TimeOfDay.getTOD("tod"));
		assertEquals(deliv.getTOD().equals(TimeOfDay.A), true);
	}
	
	@Test
	public void testAddress()
	{
		Address add1 = new Address();
		Address add2 = new Address("Mulberry Lane", "Seussville", "12345", "IL");
		assertEquals(add1.isMatch(add1), true);
		assertEquals(add1.isMatch(add2), false);
	}

	@Test
	public void testSubscriber()
	{
		Subscriber sub1 = new Subscriber();
		Address add = new Address();
		Delivery deliv = new Delivery();
		Subscriber sub2 = new Subscriber("name","email","phone","facebook",
			"twitter",add,deliv,Vin.defaultSubsFile);
		Subscriber sub3 = new Subscriber("name","email","phone",
			add,deliv,Vin.defaultSubsFile);
		
		assertEquals(sub1.isMatch(sub1), true);
		assertEquals(sub1.isMatch(sub2), false);
		assertEquals(sub2.isMatch(sub3), false);
	}
	
	@Test
	public void testState()
	{
		List<String> illegalStates = new ArrayList<String>();
		for (int i = 0; i < State.values().length; i++)
		{
			if (State.values()[i].getLegality() == false)
				illegalStates.add(State.values()[i].getAbbrev());
		}
		assertEquals(illegalStates.size(), 10);
	}
	
	@Test
	public void testStatus()
	{
		String stat1 = "C", stat2 = "Cancelled", stat3 = "cancel";
		assertEquals(Status.getStatus(stat1).equals(Status.C), true);
		assertEquals(Status.getStatus(stat2).equals(Status.C), true);
		//default case
		assertEquals(Status.getStatus(stat3).equals(Status.C), false);
	}
	
	@Test
	public void testWine()
	{
		Wine wine1 = new Wine();
		Wine wine2 = new Wine();
		Wine wine3 = new Wine("swill","unknown","Lynchfield Prison","Tennessee","Prison Boiiees",2015,WineType.SPARKLING,WineVariety.RED,Vin.defaultWineFile);
		assertEquals(wine1.isMatch(wine2), true);
		assertEquals(wine1.isMatch(wine3), false);
	}
	
	@Test
	public void testShipment()
	{
		Shipment ship1 = new Shipment();
		Shipment ship2 = new Shipment();
		assertEquals(ship1.isMatch(ship2), true);
	}
	
	@Test
	public void testNote()
	{
		
	}
	//WINE /-selection/type/variety, MONTHLY SELECTION
}
