package test.java;

//import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.ContextConfiguration;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import main.java.classes.Status;
import main.java.wineClub.*;

//import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Vin.class)
@WebAppConfiguration
public class ControllersTest
{
	@SuppressWarnings("unused")
	private String content = "This is a default note for a class accepting a note, make sure the "
		+ "length of a note is appropriate.";
		
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mock;

    @Autowired
    RunSubscriber subController;

    @Before
    public void initialize()
    {
        mock = MockMvcBuilders.webAppContextSetup(wac).build();
        Vin.ResetFiles();
    }
	    
	//ADMIN CONTROLLER
	
	@Test
    public void testWineAdmin() throws Exception
    {
    	MvcResult result = mock.perform
        (
    		post("/vin/admin/0/wines").contentType(MediaType.APPLICATION_JSON)
    		.param("labelname", "pruno")
    		.param("grape", "champagne")
    		.param("region", "Champagne")
    		.param("country", "France")
    		.param("maker", "drunk boys")
    		.param("year", "2011")
    		.param("winetype", "any")
    		.param("winevariety", "any")
        ).andExpect(status().isCreated()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		post("/vin/admin/0/wines").contentType(MediaType.APPLICATION_JSON)
    		.param("labelname", "pruno")
    		.param("grape", "champagne")
    		.param("region", "Champagne")
    		.param("country", "France")
    		.param("maker", "drunk boys")
    		.param("year", "2011")
    		.param("winetype", "TABLE")
    		.param("winevariety", "RED")
        ).andExpect(status().isCreated()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		post("/vin/admin/0/wines/1").contentType(MediaType.APPLICATION_JSON)
    		.param("labelname", "pruno 2.0")
    		.param("grape", "champagne")
    		.param("region", "Brussels")
    		.param("country", "Germany")
    		.param("maker", "sober boys")
    		.param("year", "2012")
    		.param("winetype", "SWEET")
    		.param("winevariety", "WHITE")
        ).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
                
        result = mock.perform
        (
    		delete("/vin/admin/0/wines/0").contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }
	
	//SUBSCRIBER CONTROLLER
    
    @Test
    public void testSub() throws Exception
    {
        MvcResult result = mock.perform
        (
    		post("/vin/sub")
            .contentType(MediaType.APPLICATION_JSON)
            .param("name", "me")
            .param("email", "em@il.com")
            .param("phone", "123-456-7890")
            .param("state", "WY")
            .param("city", "Dirt")
            .param("zip", "12345")
            .param("street", "Inianapolis Way")
            .param("day", "M")
            .param("time", "A")
        ).andExpect(status().isCreated()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		post("/vin/sub/0")
            .contentType(MediaType.APPLICATION_JSON)
            .param("name", "richard")
            .param("email", "email@something.com")
            .param("phone", "234-567-8901")
            .param("state", "IL")
            .param("city", "Sod")
            .param("zip", "23456")
            .param("street", "Iniana Road")
            .param("day", "T")
            .param("time", "M")
        ).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());

        result = mock.perform
        (
    		get("/vin/sub/0")
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        //so we still have a user, ID = 1
        result = mock.perform
        (
    		post("/vin/sub")
            .contentType(MediaType.APPLICATION_JSON)
            .param("name", "me")
            .param("email", "em@il.com")
            .param("phone", "123-456-7890")
            .param("state", "WY")
            .param("city", "Dirt")
            .param("zip", "12345")
            .param("street", "Inianapolis Way")
            .param("day", "M")
            .param("time", "A")
        ).andExpect(status().isCreated()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
            
        //get all
        result = mock.perform(get("/vin/sub")).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
                
        result = mock.perform
        (
    		delete("/vin/sub/0")
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }
    
    @Test
    public void testShip() throws Exception
    {
    	mock.perform
        (
    		post("/vin/sub/1/shipments").contentType(MediaType.APPLICATION_JSON)
            .param("wsid", "0")
    		.param("status", Status.D.getDesc())
    		.param("year", "2012")
    		.param("month", "8")
        );//.andExpect(status().isCreated()).andReturn();
        //System.out.println(result.getResponse().getContentAsString());
                
        //keeps a shipment with ID = 1
    	mock.perform
            (
        		post("/vin/sub/1/shipments").contentType(MediaType.APPLICATION_JSON)
                .param("wsid", "0")
        		.param("status", Status.S.getDesc())
        		.param("year", "2012")
        		.param("month", "9")
            );//.andExpect(status().isCreated()).andReturn();
            //System.out.println(result.getResponse().getContentAsString());
                
        mock.perform
        (
    		get("/vin/sub/1/shipments").contentType(MediaType.APPLICATION_JSON)
        );//.andExpect(status().isOk()).andReturn();
        //System.out.println(result.getResponse().getContentAsString());
        
        mock.perform
        (
    		post("/vin/sub/1/shipments/0").contentType(MediaType.APPLICATION_JSON)
            .param("wsid", "0")
    		.param("status", Status.D.getDesc())
    		.param("year", "2112")
    		.param("month", "1")
        );//.andExpect(status().isOk()).andReturn();
        //System.out.println(result.getResponse().getContentAsString());
        
        MvcResult result = mock.perform
        (
    		get("/vin/sub/1/shipments/0").contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		delete("/vin/sub/1/shipments/0").contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }
    
    @Test
    public void testWine() throws Exception
    {
    	MvcResult result = mock.perform
        (
    		get("/vin/sub/1/wines").contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		get("/vin/sub/1/wines/1").contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		get("/vin/sub/1/wines/1/rating").contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		post("/vin/sub/1/wines/1/rating").contentType(MediaType.APPLICATION_JSON)
    		.param("rating", "7.5")
    	).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		post("/vin/sub/1/wines/1/rating").contentType(MediaType.APPLICATION_JSON)
    		.param("rating", "8.1")
    	).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
        
        result = mock.perform
        (
    		get("/vin/sub/1/wines/1/rating").contentType(MediaType.APPLICATION_JSON)
    	).andExpect(status().isOk()).andReturn();
        System.out.println(result.getResponse().getContentAsString());
    }
}