package main.java.rest;

import main.java.classes.Delivery;

public abstract class SubscriberModifyRequest
{
	//subscriber variables
	public String name, email, phone, facebook, twitter,ID;
	public String state, city, zip, street;
	public Delivery delivery;
	
	public SubscriberModifyRequest(String id,String name, String email, String phone, 
		String facebook, String twitter, String state, String city, String zip, String street, Delivery d)
	{
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.facebook = facebook;
		this.twitter = twitter;
		this.state = state;
		this.city = city;
		this.zip = zip;
		this.street = street;
		this.ID = id;
		this.delivery = d;
	}
	
	//methods
	public abstract RestResponse modifySubscriber();
}