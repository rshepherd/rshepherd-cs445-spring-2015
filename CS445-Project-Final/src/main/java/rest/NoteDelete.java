package main.java.rest;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Note;
import main.java.classes.Serializer;

public class NoteDelete
{
	//private variables
	List<Note> noteList = null;
	Serializer ser = new Serializer();
	private String fileName;
	
	//constructor(s)
	public NoteDelete(String fn)
	{
		fileName = fn;
		noteList = ser.deserializeNote(fn);
	}
	
	public RestResponse deleteByIDShipment(int uid, int sid, int noteID)
	{
		ser = new Serializer();
		noteList = new NoteView(fileName).getNoteByUidSid(uid, sid);
		for(int i = 0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getID() == noteID)
			{
				 noteList.remove(i);
				 ser.serializeNote(noteList, fileName);
				 return new RestResponse(HttpStatus.OK, "Note was deleted");
			}
		}
		return new RestResponse(HttpStatus.OK, "No note with ID = " + noteID + " found");
	}
	
	public RestResponse deleteByIDWine(int uid, int wid, int noteID)
	{
		ser = new Serializer();
		noteList = new NoteView(fileName).getNoteByUidWid(uid, wid);
		for(int i = 0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getID() == noteID)
			{
				 noteList.remove(i);
				 ser.serializeNote(noteList, fileName);
				 return new RestResponse(HttpStatus.OK, "Note was deleted");
			}
		}
		return new RestResponse(HttpStatus.OK, "No note with ID = " + noteID + " found");
	}
}
