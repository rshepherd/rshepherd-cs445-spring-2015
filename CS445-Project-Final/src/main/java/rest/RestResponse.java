package main.java.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class RestResponse
{
	private Object     _error;
	private HttpStatus _code;//private int    _code;

	public RestResponse(HttpStatus code, Object error)
	{
		this._code  = code;
		this._error = error;
	}

	public ResponseEntity<Object> response()
	{
		ResponseEntity<Object> result;
		//try{
		result = new ResponseEntity<Object>(_error, _code);//HttpStatus.valueOf(_code));
		//} catch(Exception e){result = new ResponseEntity<Object>(new CustomError(_code, _error.toString()), HttpStatus.SEE_OTHER);}
		return result;
	}
}
