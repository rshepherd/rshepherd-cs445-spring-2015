package main.java.rest;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Note;
import main.java.classes.Serializer;

public class NoteView
{
	//private variables
	private String fileName;
	
	//default constructor
	public NoteView(String fn)
	{
		fileName = fn;
	}
	
	public Note getNoteByID(List<Note> noteList, int noteID)
	{
		for(int i =0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getID() == noteID)
				return noteList.get(i);
		}
		return null;
	}
	
	public List<Note> getNoteByUidSid(int uid, int sid)
	{
		List<Note> noteList = new ArrayList<Note>();
		List<Note> result = new ArrayList<Note>();
		Serializer ser = new Serializer();
		noteList = ser.deserializeNote(fileName);
		for(int i =0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getSubID() == uid)
				if(noteList.get(i).getShipID() == sid)
					result.add(noteList.get(i));
		}
		return result;
	}
	
	public List<Note> getNoteByUidWid(int uid, int wid)
	{
		List<Note> noteList = new ArrayList<Note>();
		List<Note> result = new ArrayList<Note>();
		Serializer ser = new Serializer();
		noteList = ser.deserializeNote(fileName);
		for(int i =0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getSubID() == uid)
				if(noteList.get(i).getWineID() == wid)
					result.add(noteList.get(i));
		}
		return result;
	}
	
	public RestResponse viewInfo(int uid, int sid)
	{
		List<Note> result = getNoteByUidSid(uid, sid);
		if (result != null)
		{
			return new RestResponse(HttpStatus.OK, result);
		}
		return new RestResponse(HttpStatus.OK, "No notes match");
	}
	
	public RestResponse viewInfo(int uid, int sid, int noteID)
	{
		List<Note> result = getNoteByUidSid(uid, sid);
		Note temp = getNoteByID(result, noteID);
		if (result != null)
		{
			if(temp != null)
				return new RestResponse(HttpStatus.OK, temp);
			return new RestResponse(HttpStatus.OK, "No note with ID = " + noteID + " found");
		}
		return new RestResponse(HttpStatus.OK, "No notes match");
	}
	
	public RestResponse viewInfoWine(int uid, int wid)
	{
		List<Note> result = getNoteByUidWid(uid, wid);
		if (result != null)
			return new RestResponse(HttpStatus.OK, result.toArray());
		return new RestResponse(HttpStatus.OK, "No notes match");
	}
	
	public RestResponse viewInfoWine(int uid, int wid, int noteID)
	{
		List<Note> result = getNoteByUidWid(uid, wid);
		Note temp = getNoteByID(result, noteID);
		if (result != null)
		{
			if(temp != null)
				return new RestResponse(HttpStatus.OK, temp);
			return new RestResponse(HttpStatus.OK, "No note with ID = " + noteID + " found");
		}
		return new RestResponse(HttpStatus.OK, "No notes match");
	}
}