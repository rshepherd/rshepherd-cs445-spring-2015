package main.java.rest;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Delivery;
import main.java.classes.Serializer;
import main.java.classes.Subscriber;

public class SubscriberView
{
	private String fileName;
	
	public SubscriberView(String fn)
	{
		this.fileName = fn;
	}
	
	public Subscriber getSubscriberByID(int id)
	{	
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		for(int i = 0; i < subList.size(); i++)
		{
			if(subList != null)
				if(subList.get(i).getID() == id )
					return subList.get(i);
		}
		return null;
	}
	
	public int getSubscriberPosByID(int id)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		for(int i = 0; i < subList.size(); i++)
		{
			if(subList.get(i).getID() == id )
				return i;
		}
		return -1;
	}
	
	public RestResponse viewInfo()
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		if (subList != null)
		{
			return new RestResponse(HttpStatus.OK, subList.toArray());
		}
		return new RestResponse(HttpStatus.OK, "No subscribers match");
	}
	
	public RestResponse viewInfo(int id)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		if (subList != null)
		{
			Subscriber temp = getSubscriberByID(id);
			if(temp != null)
				return new RestResponse(HttpStatus.OK, temp);
			return new RestResponse(HttpStatus.OK, "No subscriber with ID = " + id + " found");
		}
		return new RestResponse(HttpStatus.OK, "No subscribers match");
	}
	
	public RestResponse viewDelivery(int id)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		if (subList != null)
		{
			Subscriber temp = getSubscriberByID(id);
			if(temp != null)
				return new RestResponse(HttpStatus.OK, temp.getDelivery());
			return new RestResponse(HttpStatus.OK, "No subscriber with ID = " + id + " found");
		}
		return new RestResponse(HttpStatus.OK, "No subscribers match");
	}
	
	public RestResponse updateDelivery(int id, Delivery d)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		int temp = getSubscriberPosByID(id);
		if (subList != null)
		{
			if(temp != -1)
			{
				subList.get(temp).setDelivery(d);
				ser.serializeSubscriber(subList, fileName);
				return new RestResponse(HttpStatus.OK, "Account updated");
			}
			return new RestResponse(HttpStatus.OK, "No subscriber with ID = " + id + " found");
		}
		return new RestResponse(HttpStatus.OK, "No subscribers match");
	}
}
