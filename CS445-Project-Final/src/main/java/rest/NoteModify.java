package main.java.rest;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Note;
import main.java.classes.Serializer;
import main.java.classes.Shipment;
import main.java.wineClub.Vin;

public class NoteModify extends NoteModifyRequest
{
	//private variables
	List<Note> noteList = null;
	Serializer ser = new Serializer();
	private String fileName;
	private int uid, sid;
	
	//constructor(s)
	public NoteModify(int uid, int sid, int nid, String content, String fn)
	{	
		super(content, nid);
		fileName = fn;
		//note.writeNote(note);
		noteList = ser.deserializeNote(fileName);
		this.uid = uid;
		this.sid = sid;
	}
	
	//TODO: error
	public RestResponse modifyNote()
	{
		ser = new Serializer();
		for(int i =0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getID() == this.noteID)
			{
				if (Note.checkNote(content))
					noteList.get(i).setContent(this.content);
				else
					return new RestResponse(HttpStatus.NOT_FOUND, null);
				if (new SubscriberView(Vin.defaultSubsFile).getSubscriberByID(uid) == null)
				{
					return new RestResponse(HttpStatus.NOT_FOUND, null);
				}
				List<Shipment> result = new ShipmentView(Vin.defaultShipFile).getShipmentByUID(uid);
				Shipment temp = new ShipmentView(Vin.defaultShipFile).getShipmentByID(result, sid);
				if (temp == null)
				{
					return new RestResponse(HttpStatus.NOT_FOUND, null);
				}
				ser.serializeNote(noteList, this.fileName);
				return new RestResponse(HttpStatus.OK, "Account updated");
			}
		}
		return new RestResponse(HttpStatus.OK, "No note with ID = " + this.noteID + " found");
	}
}
