package main.java.rest;

import main.java.classes.Delivery;

public abstract class SubscriberAddRequest
{
	//subscriber variables
	public String name, email, phone, facebook, twitter;
	public String state, city, zip, street;
	public Delivery delivery;
	
	//constructor
	public SubscriberAddRequest(String name, String email, String phone, String facebook, 
			String twitter, String state, String city, String zip, String street, Delivery d)
	{
		this.name     = name;
		this.email    = email;
		this.phone    = phone;
		this.city     = city;
		this.state    = state;
		this.zip      = zip;
		this.street   = street;
		this.facebook = facebook;
		this.twitter  = twitter;
		this.delivery = d;
	}
	
	//methods
	public abstract RestResponse addSubscriber();
}