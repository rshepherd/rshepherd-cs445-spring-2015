package main.java.rest;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Serializer;
import main.java.classes.Subscriber;

public class SubscriberDelete
{
	//private variables
	private List<Subscriber> subList = null;
	private Serializer       ser = new Serializer();
	private String           fileName;
	
	//constructor(s)
	public SubscriberDelete(String fn)
	{
		fileName = fn;
		subList = ser.deserializeSubscriber(fileName);
	}
	
	public RestResponse deleteByID(int id)
	{
		ser = new Serializer();	
		for(int i = 0; i < subList.size(); i++)
		{
			if(subList.get(i).getID() == id)
			{
				 subList.remove(i);
				 ser.serializeSubscriber(subList, fileName);
				 return new RestResponse(HttpStatus.OK, "Subscriber was deleted");
			}
		}
		return new RestResponse(HttpStatus.OK, "No subscriber with ID = " + id + " found");
	}
}
