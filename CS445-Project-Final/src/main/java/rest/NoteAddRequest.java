package main.java.rest;
public abstract class NoteAddRequest
{
	//subscriber variables
	public String content;
	public int subID, shipID;
	
	//constructor
	public NoteAddRequest(String cont, int subId, int shipId)
	{
		this.content = cont;
		this.subID = subId;
	}
	
	//methods
	public abstract RestResponse addNoteShipment();
	public abstract RestResponse addNoteWine();
}