package main.java.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Serializer;
import main.java.classes.Shipment;
import main.java.classes.Subscriber;
import main.java.classes.Wine;

public class WineView
{
	//private variables
	private String fileName;
	
	//default constructor
	public WineView(String fn)
	{
		fileName = fn;
	}
	
	public Wine getWineByID(int wineID)
	{
		List<Wine> wineList = new ArrayList<Wine>();
		Serializer ser = new Serializer();
		wineList = ser.deserializeWine(fileName);
		if (wineList != null)
			for(int i =0; i < wineList.size(); i++)
				if(wineList.get(i).getID() == wineID)
					return wineList.get(i);
		return null;
	}
	
	public int getWinePosByID(int wineID)
	{
		List<Wine> wineList = new ArrayList<Wine>();
		Serializer ser = new Serializer();
		wineList = ser.deserializeWine(fileName);
		for(int i =0; i < wineList.size(); i++)
		{
			if(wineList.get(i).getID() == wineID)
				return i;
		}
		return -1;
	}
	
	public List<Wine> getWineBySubscriber(Subscriber sub, String shipFileName)
	{
		List<Wine> wineList = new ArrayList<Wine>();
		List<Shipment> ships = new ShipmentView(shipFileName).getShipmentByUID(sub.getID());
		for(int i = 0; i < ships.size(); i++)
			if (ships.get(i).getWineList() != null)
				for(int j = 0; j < ships.get(i).getWineList().length; j++)
					wineList.add(ships.get(i).getWineList()[j]);
		return wineList;
	}

	public RestResponse viewInfo(int uid, String subFileName, String shipFileName)
	{
		//Subscriber sub = new SubscriberView(subFileName).getSubscriberByID(uid);
		List<Wine> wineList = new Serializer().deserializeWine(fileName);//getWineBySubscriber(sub, shipFileName);
		if (wineList != null)
		{
			return new RestResponse(HttpStatus.OK, wineList.toArray());
		}
		return new RestResponse(HttpStatus.OK, "No wines match");
	}
	
	public RestResponse viewInfo(int uid, int wid, String subFileName, String shipFileName)
	{
		//Subscriber sub = new SubscriberView(subFileName).getSubscriberByID(uid);
		List<Wine> wineList = new Serializer().deserializeWine(fileName);//getWineBySubscriber(sub, shipFileName);
		if (wineList != null)
		{
			Wine temp = getWineByID(wid);
			if(temp != null)
				return new RestResponse(HttpStatus.OK, temp);
			return new RestResponse(HttpStatus.OK, "No wine with ID = " + wid + " found");
		}
		return new RestResponse(HttpStatus.OK, "No wines found in file");
	}

	public RestResponse viewRating(int uid, int wid, String subFileName, String shipFileName)
	{
		//Subscriber sub = new SubscriberView(subFileName).getSubscriberByID(uid);
		List<Wine> wineList = new Serializer().deserializeWine(fileName);//getWineBySubscriber(sub, shipFileName);
		if (wineList != null)
		{
			Wine temp = getWineByID(wid);
			if(temp != null)
			{
				return new RestResponse(HttpStatus.OK, temp.getRating());
			}
			return new RestResponse(HttpStatus.OK, "No wine with ID = " + wid + " found");
		}
		return new RestResponse(HttpStatus.OK, "No wines found in file");
	}
	
	public RestResponse rate(int wid, double rating)
	{
		Serializer ser = new Serializer();
		List<Wine> wineList = ser.deserializeWine(fileName);
		if (wineList != null)
		{
			for(int i = 0; i < wineList.size(); i++)
			{
				if (wineList.get(i).getID() == wid)
				{
					if (rating >= 0.0 && rating <= 10.0)
					{
						wineList.get(i).addRating(rating);
						ser.serializeWine(wineList, fileName);
						return new RestResponse(HttpStatus.OK, "Account Updated");
					}
					return new RestResponse(HttpStatus.NOT_FOUND, "Incorrect rating (0.0 <= rating <= 10.0)");
				}
						
			}
			return new RestResponse(HttpStatus.OK, "No wine with ID = " + wid + " found");
		}
		return new RestResponse(HttpStatus.OK, "No wines found in file");
	}
}
