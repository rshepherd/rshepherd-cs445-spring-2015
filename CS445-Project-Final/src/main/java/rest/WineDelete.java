package main.java.rest;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Serializer;
import main.java.classes.Wine;

public class WineDelete
{
	//private variables
	private List<Wine> wineList = null;
	private Serializer ser = new Serializer();
	private String     fileName;
	
	//constructor(s)
	public WineDelete(String fn)
	{
		fileName = fn;
		wineList = ser.deserializeWine(fileName);
	}
	
	public RestResponse deleteByID(int id)
	{
		ser = new Serializer();	
		wineList = ser.deserializeWine(fileName);
		for(int i = 0; i < wineList.size(); i++)
		{
			if(wineList.get(i).getID() == id)
			{
				 wineList.remove(i);
				 ser.serializeWine(wineList, fileName);
				 return new RestResponse(HttpStatus.OK, "Wine was deleted");
			}
		}
		return new RestResponse(HttpStatus.OK, "No wine with ID = " + id + " found");
	}
}
