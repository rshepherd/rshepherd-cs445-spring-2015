package main.java.rest;
import java.time.YearMonth;

import main.java.classes.Status;

public abstract class ShipmentModifyRequest
{
	//subscriber variables
	int subID, /*noteID,*/ wsID, ID;
	Status status;
	YearMonth yearMonth;
	
	public ShipmentModifyRequest(int subId, /*int noteId,*/ int wsId, Status st, YearMonth ym, int id)
	{
		this.subID = subId;
		//this.noteID = noteId;
		this.wsID = wsId;
		this.status = st;
		this.yearMonth = ym;
		this.ID = id;
	}
	
	//methods
	public abstract RestResponse modifyShipment();
}