package main.java.rest;
import java.time.YearMonth;

//import main.java.classes.MonthlySelection;
import main.java.classes.Status;

public abstract class ShipmentAddRequest
{
	//subscriber variables
	int subID, /*noteID,*/ wsID;//WineSelection ws;
	Status status;
	YearMonth yearMonth;
	
	//constructor
	public ShipmentAddRequest(int subId, /*int noteId,*/ int wsId, Status st, YearMonth ym)
	{
		this.subID     = subId;
		//this.noteID    = noteId;
		this.wsID      = wsId;
		this.status    = st;
		this.yearMonth = ym;
	}
	
	//methods
	public abstract RestResponse addShipment();
}