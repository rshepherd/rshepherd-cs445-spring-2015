package main.java.rest;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
//import main.java.classes.Note;
import main.java.classes.Serializer;
import main.java.classes.Shipment;

public class ShipmentView
{
	//private variables
	private String fileName;
	
	//default constructor
	public ShipmentView(String fn)
	{
		fileName = fn;
	}
	
	public Shipment getShipmentByID(List<Shipment> shipList, int shipID)
	{
		for(int i =0; i < shipList.size(); i++)
		{
			if(shipList.get(i).getID() == shipID)
				return shipList.get(i);
		}
		return null;
	}
	
	public int getShipmentPosByID(List<Shipment> shipList, int shipID)
	{
		for(int i =0; i < shipList.size(); i++)
		{
			if(shipList.get(i).getID() == shipID)
				return i;
		}
		return -1;
	}
	
	public List<Shipment> getShipmentByUID(int uid)
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		List<Shipment> result = new ArrayList<Shipment>();
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment(fileName);
		for(int i =0; i < shipList.size(); i++)
		{
			if(shipList.get(i).getSubID() == uid)
				result.add(shipList.get(i));
		}
		return result;
	}

	public RestResponse viewInfo(int uid)
	{
		List<Shipment> result = getShipmentByUID(uid);
		if (result != null)
		{
			return new RestResponse(HttpStatus.OK, result.toArray());
		}
		return new RestResponse(HttpStatus.OK, "No shipments found for user");
	}
	
	public RestResponse viewInfo(int uid, int id)
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		List<Shipment> result = getShipmentByUID(uid);
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment(fileName);
		if (shipList != null)
		{
			Shipment temp = getShipmentByID(result, id);
			if(temp != null)
				return new RestResponse(HttpStatus.OK, temp);
			return new RestResponse(HttpStatus.OK, "No shipment with ID = " + id + " found");
		}
		return new RestResponse(HttpStatus.OK, "No shipments found for user");
	}
}
