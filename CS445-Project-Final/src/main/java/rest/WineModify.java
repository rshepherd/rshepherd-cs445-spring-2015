package main.java.rest;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import main.java.classes.Serializer;
import main.java.classes.Wine;
import main.java.classes.WineType;
import main.java.classes.WineVariety;

public class WineModify extends WineModifyRequest
{
	private String fileName;
	
	//constructor(s)
	public WineModify(int id, String labelname, String grape, String region, String country, 
			String maker, int year, WineType wts, WineVariety wvs, String fn)
	{
		super(id,labelname,grape,region,country,maker,year,wts,wvs);
		fileName = fn;
	}

	//TODO: error handling
	public RestResponse modifyWine()
	{
		List<Wine> wineList = new ArrayList<Wine>();
		int id = Integer.valueOf(this.ID);
		Serializer ser = new Serializer();
		wineList = ser.deserializeWine(fileName);
		int temp = new WineView(fileName).getWinePosByID(id);
		if(temp != -1)
		{
			if (labelname == null || labelname.equals(""))
			{
				//error
			}
			else
				wineList.get(temp).setLabelName(labelname);
			if (grape == null || grape.equals(""))
			{
				//error
			}
			else
				wineList.get(temp).setGrape(grape);	
			if (region == null || region.equals(""))
			{
				//error
			}
			else
				wineList.get(temp).setRegion(region);
			if (country == null || country.equals(""))
			{
				//error
			}
			else
				wineList.get(temp).setCountry(country);
			if (maker == null || maker.equals(""))
			{
				//error
			}
			else
				wineList.get(temp).setMaker(maker);
			if (year != -1)
			{
				//error
			}
			else
				wineList.get(temp).setYear(year);
			if (wts == null || wts.equals(""))
			{
				//error
			}
			else
				wineList.get(temp).setWineType(wts);
			if (wvs == null || wvs.equals(""))
			{
				//error
			}
			else
				wineList.get(temp).setWineVariety(wvs);
			ser.serializeWine(wineList, fileName);
			return new RestResponse(HttpStatus.OK, "Account updated");
		}
		else 
		{
			return new RestResponse(HttpStatus.OK, "No wine with ID = " + id + " found");
		}
	}
}

