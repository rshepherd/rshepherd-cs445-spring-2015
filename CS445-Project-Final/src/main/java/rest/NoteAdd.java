package main.java.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

//import main.java.classes.Address;
import main.java.classes.IDGenerator;
import main.java.classes.Note;
import main.java.classes.Serializer;
import main.java.classes.Shipment;
//import main.java.classes.State;
//import main.java.classes.Subscriber;
import main.java.wineClub.Vin;

public class NoteAdd extends NoteAddRequest
{
	//private members
	private Note note;
	List<Note> noteList = new ArrayList<Note>();
	private String fileName;
	private int wid;
	
    //constructor(s)
	public NoteAdd(String cont, int subId, int shipId, String fn)
	{
		super(cont, subId, shipId);
		fileName = fn;
	}
	
	public NoteAdd(String cont, int subId, int shipId, int wid, String fn)
	{
		super(cont, subId, shipId);
		fileName = fn;
		this.wid = wid;
	}
	
	public RestResponse addNoteShipment()
	{
		Serializer ser = new Serializer();
		noteList = ser.deserializeNote(fileName);
		//error handling
		if (!Note.checkNote(content))
			return new RestResponse(HttpStatus.NOT_FOUND, null);
		if (new SubscriberView(Vin.defaultSubsFile).getSubscriberByID(subID) == null)
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);
		}
		List<Shipment> result = new ShipmentView(Vin.defaultShipFile).getShipmentByUID(subID);
		Shipment temp = new ShipmentView(Vin.defaultShipFile).getShipmentByID(result, shipID);
		if (temp == null)
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);
		}
		//list handling
		if(noteList == null)
			noteList = new ArrayList<Note>();
		int id = IDGenerator.generateNoteID(fileName);
		for (int i = 0; i < (temp.getWineList()).length; i++)
		{
			note = new Note(this.content, this.subID, this.shipID, temp.getWineList()[i].getID(), this.fileName);
			note.setID(id);
			noteList.add(note);
			id++;
		}
		ser.serializeNote(noteList, fileName);
		return new RestResponse(HttpStatus.CREATED, note);
	}

	public RestResponse addNoteWine()
	{
		Serializer ser = new Serializer();
		noteList = ser.deserializeNote(fileName);
		//error handling
		if (!Note.checkNote(content))
			return new RestResponse(HttpStatus.NOT_FOUND, null);
		if (new SubscriberView(Vin.defaultSubsFile).getSubscriberByID(subID) == null)
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);
		}
		List<Shipment> result = new ShipmentView(Vin.defaultShipFile).getShipmentByUID(subID);
		Shipment temp = new ShipmentView(Vin.defaultShipFile).getShipmentByID(result, shipID);
		if (temp == null)
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);
		}
		//list handling
		if(noteList == null)
			noteList = new ArrayList<Note>();
		int id = IDGenerator.generateNoteID(fileName);
		note = new Note(this.content, this.subID, this.shipID, this.wid, this.fileName);
		note.setID(id);
		noteList.add(note);
		ser.serializeNote(noteList, fileName);
		return new RestResponse(HttpStatus.CREATED, note);
	}	
}
