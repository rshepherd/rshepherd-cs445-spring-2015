package main.java.rest;

import main.java.classes.WineType;
import main.java.classes.WineVariety;

public abstract class WineAddRequest
{
	//wine variables
	public String labelname, grape, region, country, maker; 
	public int year;
	public WineType wts;
	public WineVariety wvs;
	
	//constructor
	public WineAddRequest(String labelname, String grape, String region, String country, 
			String maker, int year, WineType wts, WineVariety wvs)
	{
		this.labelname = labelname;
		this.grape     = grape;
		this.region    = region;
		this.country   = country;
		this.maker     = maker;
		this.year      = year;
		this.wts       = wts;
		this.wvs       = wvs;
	}
	
	//methods
	public abstract RestResponse addWine();
}