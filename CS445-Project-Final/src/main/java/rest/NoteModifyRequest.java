package main.java.rest;
public abstract class NoteModifyRequest
{	
	public int noteID;
	public String content;
	
	public NoteModifyRequest(String cont, int nid)
	{
		this.content = cont;
		this.noteID = nid;
	}
	
	//methods
	public abstract RestResponse modifyNote();
}