package main.java.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Delivery;
import main.java.classes.Serializer;
import main.java.classes.Subscriber;

public class SubscriberModify extends SubscriberModifyRequest
{
	private String fileName;
	
	//constructor(s)
	public SubscriberModify(String id, String name, String email, String phone, String facebook,
		String twitter, String state, String city, String zip, String street, Delivery d, String fn)
	{
		super(id, name, email, phone, facebook, twitter, state, city, zip, street, d);
		fileName = fn;
	}

	//TODO: error handling
	public RestResponse modifySubscriber()
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		int id = Integer.valueOf(this.ID);
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		int temp = new SubscriberView(fileName).getSubscriberPosByID(id);
		if(temp != -1)
		{
			if(this.name == null || this.name.equals(""))
			{
				//name is empty
			} else {
				subList.get(temp).setName(this.name);
			}
			if(this.email == null || this.email.equals(""))
			{
				//email is empty
			} else {
				subList.get(temp).setEmail(this.email);
			}
			if(this.phone == null || this.phone.equals(""))
			{
				//phone is empty
			} else {
				subList.get(temp).setPhone(this.phone);
			}
			if(this.facebook == null || this.facebook.equals(""))
			{
				//facebook is empty
			} else {
				subList.get(temp).setFacebook(this.facebook);
			}
			if(this.twitter == null || this.twitter.equals(""))
			{
				//twitter is empty
			} else {
				subList.get(temp).setTwitter(this.twitter);
			}
			if(this.state == null || this.state.equals(""))
			{
				//state is empty
			} else {
				subList.get(temp).getAddress().setState(this.state);
			}
			if(this.state == null || this.state.equals(""))
			{
				//state is empty
			} else {
				subList.get(temp).getAddress().setCity(this.city);
			}
			if(this.zip == null || this.zip.equals(""))
			{
				//zip is empty
			} else {
				subList.get(temp).getAddress().setZip(this.zip);
			}
			if(this.street == null || this.street.equals(""))
			{
				//street is empty
			} else {
				subList.get(temp).getAddress().setStreet(this.street);
			}
			if(this.delivery != null)
				subList.get(temp).setDelivery(this.delivery);
			ser.serializeSubscriber(subList, fileName);
			return new RestResponse(HttpStatus.OK, "Account updated");
		}
		else 
		{
			return new RestResponse(HttpStatus.OK, "No subscriber with ID = " + id + " found");
		}
	}
}

