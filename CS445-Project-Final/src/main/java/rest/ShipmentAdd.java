package main.java.rest;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.IDGenerator;
//import main.java.classes.MonthlySelection;
import main.java.classes.Serializer;
import main.java.classes.Shipment;
import main.java.classes.Status;

public class ShipmentAdd extends ShipmentAddRequest
{
	//private members
	private Shipment ship;
	List<Shipment> shipList = new ArrayList<Shipment>();
	private String fileName;
	
    //constructor(s)
	public ShipmentAdd(int subId, /*int noteId,*/ int wsId, Status st, YearMonth ym, String fn)
	{
		super(subId, /*noteId,*/ wsId, st, ym);
		this.fileName = fn;
	}
	
	//TODO: error handling
	public RestResponse addShipment()
	{
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment(fileName);
		ship = new Shipment(subID, /*noteID,*/ wsID, status, yearMonth, fileName);
		if(shipList == null)
		{
			/*if(sub.getName() == null || sub.getName().equals(""))
			{
				return new SubscriberAddResponse(0,1000,"Name must be provided");
			}*/
			shipList = new ArrayList<Shipment>();
		}
		int id = IDGenerator.generateShipmentID(fileName);
		ship.setID(id);
		shipList.add(ship);
		ser.serializeShipment(shipList, fileName);
		return new RestResponse(HttpStatus.CREATED, ship);
	}	
}
