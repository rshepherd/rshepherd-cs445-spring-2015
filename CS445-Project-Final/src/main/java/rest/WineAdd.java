package main.java.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.IDGenerator;
import main.java.classes.Serializer;
import main.java.classes.Wine;
import main.java.classes.WineType;
import main.java.classes.WineVariety;

public class WineAdd extends WineAddRequest
{
	//private members
	private Wine wine;
	List<Wine> wineList = new ArrayList<Wine>();
	private String fileName;
	
    //constructor(s)
	public WineAdd(String labelname, String grape, String region, String country, 
			String maker, int year, WineType wts, WineVariety wvs, String fn)
	{
		super(labelname,grape,region,country,maker,year,wts,wvs);
		fileName = fn;
	}
	
	public List<Wine> getWines()
	{
		return wineList;
	}
	
	//TODO: error handling
	public RestResponse addWine()
	{
		wine = new Wine(labelname,grape,region,country,maker,year,wts,wvs,fileName);
		Serializer ser = new Serializer();
		wineList = ser.deserializeWine(fileName);
		if(wineList == null)
			wineList = new ArrayList<Wine>();
		//TODO: error
		int id = IDGenerator.generateWineID(fileName);
		wine.setID(id);
		wineList.add(wine);
		ser.serializeWine(wineList, fileName);
		return new RestResponse(HttpStatus.CREATED, wine);
	}
}
