package main.java.rest;

import main.java.classes.WineType;
import main.java.classes.WineVariety;

public abstract class WineModifyRequest
{
	//wine variables
	public String labelname, grape, region, country, maker; 
	public int year, ID;
	public WineType wts;
	public WineVariety wvs;
	
	//constructor
	public WineModifyRequest(int id, String labelname, String grape, String region, String country, 
		String maker, int year, WineType wts, WineVariety wvs)
	{
		this.labelname = labelname;
		this.grape     = grape;
		this.region    = region;
		this.country   = country;
		this.maker     = maker;
		this.year      = year;
		this.wts       = wts;
		this.wvs       = wvs;
		this.ID        = id;
	}
	
	//methods
	public abstract RestResponse modifyWine();
}