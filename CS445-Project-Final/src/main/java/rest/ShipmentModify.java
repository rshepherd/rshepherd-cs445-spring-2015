package main.java.rest;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Serializer;
import main.java.classes.Shipment;
import main.java.classes.Status;

public class ShipmentModify extends ShipmentModifyRequest
{
	private String fileName;
	
	//constructor(s)
	public ShipmentModify(int subId, /*int noteId,*/ int wsId, Status st, YearMonth ym, int id, String fn)
	{
		super(subId, /*noteId,*/ wsId, st, ym, id);
		fileName = fn;
	}

	//TODO: error handling
	public RestResponse modifyShipment()
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		int id = Integer.valueOf(this.ID);
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment(fileName);
		List<Shipment> result = new ShipmentView(fileName).getShipmentByUID(subID);
		int temp = new ShipmentView(fileName).getShipmentPosByID(result, id);
		if(temp != -1)
		{
			if(this.subID == -1)
			{
				//sub DNE
			} else {
				shipList.get(temp).setSubcriber(subID);
			}
			/*if(this.noteID == -1)
			{
				//note DNE
			} else {
				shipList.get(temp).setNote(noteID);
			}*/
			if(this.wsID == -1)
			{
				//ws DNE
			} else {
				shipList.get(temp).setWineSelection(wsID);
			}
			if(this.status == null || this.status.equals(""))
			{
				//status DNE
			} else {
				shipList.get(temp).setStatus(this.status);
			}
			if(this.yearMonth == null)
			{
				//ym DNE
			} else {
				shipList.get(temp).setDate(this.yearMonth);
			}
			ser.serializeShipment(shipList, fileName);
			return new RestResponse(HttpStatus.OK, "Account Updated");
		}
		return new RestResponse(HttpStatus.OK, "No shipment with ID = " + id + " found");
	}
}

