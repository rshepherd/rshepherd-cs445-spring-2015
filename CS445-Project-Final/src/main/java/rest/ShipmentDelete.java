package main.java.rest;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Shipment;
import main.java.classes.Serializer;

public class ShipmentDelete
{
	//private variables
	List<Shipment> shipList = null;
	Serializer ser = new Serializer();
	private String fileName;
	
	//constructor(s)
	public ShipmentDelete(String fn)
	{
		fileName = fn;
		shipList = ser.deserializeShipment(fn);
	}
	
	public RestResponse deleteByID(int shipID)
	{
		ser = new Serializer();	
		for(int i = 0; i < shipList.size(); i++)
		{
			if(shipList.get(i).getID() == shipID)
			{
				 shipList.remove(i);
				 ser.serializeShipment(shipList, fileName);
				 return new RestResponse(HttpStatus.OK, "Shipment was deleted");
			}
		}
		return new RestResponse(HttpStatus.OK, "No shipment with ID = " + shipID + " found");
	}
}
