package main.java.rest;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import main.java.classes.Address;
import main.java.classes.Delivery;
import main.java.classes.IDGenerator;
import main.java.classes.Serializer;
import main.java.classes.State;
import main.java.classes.Subscriber;

public class SubscriberAdd extends SubscriberAddRequest
{
	//private members
	private Address add;
	private Subscriber sub;
	List<Subscriber> subList = new ArrayList<Subscriber>();
	private String fileName;
	
    //constructor(s)
	public SubscriberAdd(String name, String email, String phone, String facebook, 
		String twitter, String state, String city, String zip, String street, Delivery d, String fn)
	{
		super(name, email, phone, facebook, twitter, state, city, zip, street, d);
		fileName = fn;
	}
	
	public List<Subscriber> getSubscribers()
	{
		return subList;
	}
	
	//TODO: 1001, 1003, 1004, 1005, 1011, 1013-1016
	public RestResponse addSubscriber()
	{
		Serializer ser = new Serializer();
		add = new Address(this.street, this.city, this.zip, this.state);
		sub = new Subscriber(this.name, this.email, this.phone, this.facebook, this.twitter, this.add, this.delivery, fileName);
		subList = ser.deserializeSubscriber(fileName);
		if(subList == null)
		{
			subList = new ArrayList<Subscriber>();
		}
		if(sub.getName() == null || sub.getName().equals(""))
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);//1000,"Name must be provided");
		}
		if(sub.getEmail() == null ||sub.getEmail().equals(""))
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);//1002,"Email must be provided");
		}
		if(sub.getAddress().getCity().equals("") || sub.getAddress().getCity() == null)
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);//1006,"City must be provided");
		}
		if(sub.getAddress().getState().equals("") ||sub.getAddress().getState() == null)
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);//1008,"State must be provided");
		}
		if(bannedState(add.getState()))
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);//1009,"We may not ship to this state"); 
		}
		if(sub.getAddress().getZip().equals("") || sub.getAddress().getZip() == null)
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);//1010,"ZIP code must be provided");
		}
		if(sub.getPhone() == null ||sub.getPhone().equals(""))
		{
			return new RestResponse(HttpStatus.NOT_FOUND, null);//1012,"Phone number must be provided");
		}
			
		int id = IDGenerator.generateID(fileName);
		sub.setID(id);
		subList.add(sub);
		ser.serializeSubscriber(subList, fileName);
		return new RestResponse(HttpStatus.CREATED, sub);
	}	
	
	public boolean bannedState(String st)
	{
		for(State state : State.values())
		{
			String stateName = "" + state;
			stateName = stateName.replace("_", " ");
			//banned state if passes
			if( st.equals(stateName) || st.equals(state.getAbbrev()) )
			{
				if (state.getLegality() == false)
					return true;
				else
					return false;
			}
		}
		//not a banned state
		return false;
	}
	
	public boolean isState(String st)
	{
		for(State state : State.values())
		{
			String stateName = "" + state;
			stateName = stateName.replace("_", " ");
			//valid state if passes
			if( st.equals(stateName) || st.equals(state.getAbbrev()) )
			{
				return true;
			}
		}
		//not a valid state
		return false;
	}
	
	public boolean isMatch(Subscriber sub1, Subscriber sub2)
	{
		if(sub1.isMatch(sub2))
			return true;
		return false;
    }
}
