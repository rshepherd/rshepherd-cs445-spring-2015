package main.java.wineClub;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import main.java.classes.*;
import main.java.rest.*;

@RestController
@RequestMapping("/vin")
public class RunAdmin
{
	@RequestMapping(value = "/admin/{aid}/wines", method = RequestMethod.POST)
	public ResponseEntity<Object> addWine
	(
		@RequestParam(value = "labelname"   ) String label,
		@RequestParam(value = "grape"       ) String grape,
		@RequestParam(value = "region"      ) String region,
		@RequestParam(value = "country"     ) String country,
		@RequestParam(value = "maker"       ) String maker,
		@RequestParam(value = "year"        ) int year,
		@RequestParam(value = "winetype"    ) String winetype,
		@RequestParam(value = "winevariety" ) String winevariety,
		@RequestParam(value = "file",       required = false) String fn
	)
	{
		WineType type = WineType.getType(winetype);
		WineVariety variety = WineVariety.getVariety(winevariety);
		String fileName = Vin.WineFile(fn);
		WineAddRequest war = new WineAdd(label, grape, region, country, maker, year, type, variety, fileName);
		RestResponse waResp = war.addWine();
		return waResp.response();
	}
	
	@RequestMapping(value = "/admin/{aid}/wines/{wid}", method = RequestMethod.POST)
	public ResponseEntity<Object> updateWine
	(
		@PathVariable(value = "wid")          int wid,
		@RequestParam(value = "labelname"   ) String label,
		@RequestParam(value = "grape"       ) String grape,
		@RequestParam(value = "region"      ) String region,
		@RequestParam(value = "country"     ) String country,
		@RequestParam(value = "maker"       ) String maker,
		@RequestParam(value = "year"        ) int year,
		@RequestParam(value = "winetype"    ) String winetype,
		@RequestParam(value = "winevariety" ) String winevariety,
		@RequestParam(value = "file",       required = false) String fn
	)
	{
		WineType type = WineType.getType(winetype);
		WineVariety variety = WineVariety.getVariety(winevariety);
		String fileName = Vin.WineFile(fn);
		WineModifyRequest war = new WineModify(wid, label, grape, region, country, maker, year, type, variety, fileName);
		RestResponse waResp = war.modifyWine();
		return waResp.response();
	}
	
	@RequestMapping(value = "/admin/{aid}/wines/{wid}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteWine
	(
		@PathVariable(value = "wid")  int wid,
		@RequestParam(value = "file", required = false) String fn
	)
	{
		String fileName = Vin.WineFile(fn);
		WineDelete wDel = new WineDelete(fileName);
		RestResponse wdResp = wDel.deleteByID(wid);
		return wdResp.response();
	}
	
	@RequestMapping(value = "/admin/revenue", method = RequestMethod.GET)
	public ResponseEntity<Object> getRevenue(@RequestParam(value = "file", required = false) String fn)
	{
		//TODO: implement revenue functionality
		//String fileName = Vin.WineFile(fn);
		RestResponse resp = new RestResponse(HttpStatus.OK, "Not yet implemented");
		return resp.response();
	}
}
