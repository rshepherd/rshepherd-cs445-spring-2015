package main.java.wineClub;

//import org.springframework.ui.ModelMap;
//import javax.print.DocFlavor.STRING;
import java.time.YearMonth;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import main.java.classes.*;
import main.java.rest.*;

@RestController
@RequestMapping("/vin")
public class RunSubscriber
{	
	//SUBSCRIBER
	
	@RequestMapping(value = "/sub", method = RequestMethod.POST)
	public ResponseEntity<Object> addSubscriber
	(
		@RequestParam(value = "name"      ) String name,
		@RequestParam(value = "email"     ) String email,
		@RequestParam(value = "phone"     ) String phone,
		@RequestParam(value = "facebook", required = false) String facebook,
		@RequestParam(value = "twitter",  required = false) String twitter,
		@RequestParam(value = "state"     ) String state,
		@RequestParam(value = "city"      ) String city,
		@RequestParam(value = "zip"       ) String zip,
		@RequestParam(value = "street"    ) String street,
		@RequestParam(value = "day",      required = false) String day,
		@RequestParam(value = "time",     required = false) String time,
		@RequestParam(value = "file",     required = false) String fn
	)
	{
		String fileName = SubFile(fn);
		Delivery deliv = null;
		if (day != null && time != null)
		{
			DayOfWeek dow = DayOfWeek.getDOW(day);
			TimeOfDay tod = TimeOfDay.getTOD(time);
			deliv = new Delivery(dow, tod);
		}
		SubscriberAddRequest sar = new SubscriberAdd(name, email, phone, facebook, twitter, state, city, zip, street, deliv, fileName);
		RestResponse saResp = sar.addSubscriber();
		return saResp.response();
	}

	//PUT no longer supported by html
	@RequestMapping(value = "/sub/{uid}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateSubscriber
    (
    	@PathVariable(value = "uid")      int id,
		@RequestParam(value = "name")     String name,
		@RequestParam(value = "email")    String email,
		@RequestParam(value = "phone")    String phone,
		@RequestParam(value = "facebook", required = false) String facebook,
		@RequestParam(value = "twitter",  required = false) String twitter,
		@RequestParam(value = "state")    String state,
		@RequestParam(value = "city")     String city,
		@RequestParam(value = "zip")      String zip,
		@RequestParam(value = "street")   String street,
		@RequestParam(value = "day",      required = false) String day,
		@RequestParam(value = "time",     required = false) String time,
		@RequestParam(value = "file",     required = false) String fn
    )
	{
		String fileName = SubFile(fn);
		Delivery deliv = null;
		if (day != null && time != null)
		{
			DayOfWeek dow = DayOfWeek.getDOW(day);
			TimeOfDay tod = TimeOfDay.getTOD(time);
			deliv = new Delivery(dow, tod);
		}
		SubscriberModifyRequest smr = new SubscriberModify(id+"", name, email, phone, facebook, twitter, state, city, zip, street, deliv, fileName);
		RestResponse smResp = smr.modifySubscriber();
		return smResp.response();
    }
	
    @RequestMapping(value = "/sub/{uid}", method = RequestMethod.GET)
    public ResponseEntity<Object> getSubscriber
    (
		@PathVariable(value = "uid")  int uid,
		@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = SubFile(fn);
    	SubscriberView subView = new SubscriberView(fileName);
		RestResponse sub = subView.viewInfo(uid);
	    return sub.response();
    }
    
    @RequestMapping(value = "/sub/{uid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteSubscriber
    (
    	@PathVariable(value = "uid")  int uid,
    	@RequestParam(value = "file", required = false) String fn
    )
	{
    	String fileName = SubFile(fn);
		SubscriberDelete sdr = new SubscriberDelete(fileName);
		RestResponse sdResp = sdr.deleteByID(uid);
		return sdResp.response();
    }
    
    //TODO: unnecessary
    @RequestMapping(value = "/sub", method = RequestMethod.GET)
    public ResponseEntity<Object> getSubscribers
    (
    	@RequestParam(value = "file", required = false) String fn
    )
    {
		String fileName = SubFile(fn);
    	SubscriberView subView = new SubscriberView(fileName);
		RestResponse sub = subView.viewInfo();
	    return sub.response();
    }
    
    //SHIPMENTS
    
    //PUT no longer supported by html
  	@RequestMapping(value = "/sub/{uid}/shipments/{id}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateShipment
    (
      	@PathVariable(value = "id")      int id,
      	@PathVariable(value = "uid")     int uid,
      	//@RequestParam(value = "nid")     int nid,
      	@RequestParam(value = "wsid")    int wsid,
  		@RequestParam(value = "content", required = false) String content,
  		@RequestParam(value = "status")  String status,
  		@RequestParam(value = "year")    int year,
  		@RequestParam(value = "month")   int month,
  		@RequestParam(value = "file",    required = false) String fn
    )
    {
  		String fileName = ShipFile(fn);
  		Status stat = Status.getStatus(status);
  		YearMonth ym = YearMonth.of(year, month);
  		ShipmentModifyRequest smr = new ShipmentModify(uid,/*nid,*/wsid,stat,ym,id,fileName);
  		RestResponse smResp = smr.modifyShipment();
  		return smResp.response();
    }
  	
  	@RequestMapping(value = "/sub/{uid}/shipments", method = RequestMethod.GET)
    public ResponseEntity<Object> getShipments
    (
    	@PathVariable(value = "uid")  int uid,
    	@RequestParam(value = "file", required = false) String fn
    )
    {
		String fileName = ShipFile(fn);
    	ShipmentView shipView = new ShipmentView(fileName);
		RestResponse ship = shipView.viewInfo(uid);
	    return ship.response();
    }
  	
  	@RequestMapping(value = "/sub/{uid}/shipments/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getShipment
    (
		@PathVariable(value = "id")  int id,
		@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = ShipFile(fn);
    	ShipmentView shipView = new ShipmentView(fileName);
		RestResponse ship = shipView.viewInfo(id);
	    return ship.response();
    }
  	
  	//TODO: unnecessary
  	@RequestMapping(value = "/sub/{uid}/shipments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteShipment
    (
    	@PathVariable(value = "id")  int id,
    	@RequestParam(value = "file", required = false) String fn
    )
	{
    	String fileName = ShipFile(fn);
		ShipmentDelete sdr = new ShipmentDelete(fileName);
		RestResponse sdResp = sdr.deleteByID(id);
		return sdResp.response();
    }
  	
  	//TODO: unnecessary
    @RequestMapping(value = "/sub/{uid}/shipments", method = RequestMethod.POST)
	public ResponseEntity<Object> addShipment
	(
		@PathVariable(value = "uid")     int uid,
    	//@RequestParam(value = "nid")     int nid,
    	@RequestParam(value = "wsid")    int wsid,
		@RequestParam(value = "content", required = false) String content,
		@RequestParam(value = "status")  String status,
		@RequestParam(value = "year")    int year,
		@RequestParam(value = "month")   int month,
		@RequestParam(value = "file",    required = false) String fn
	)
	{
		String fileName = ShipFile(fn);
		Status stat = Status.getStatus(status);
		YearMonth ym = YearMonth.of(year, month);
		ShipmentAddRequest sar = new ShipmentAdd(uid,/*nid,*/wsid,stat,ym,fileName);
		RestResponse saResp = sar.addShipment();
		return saResp.response();
	}
    
    //WINE
    
    @RequestMapping(value = "/sub/{uid}/wines", method = RequestMethod.GET)
    public ResponseEntity<Object> getWines
    (
    	@PathVariable(value = "uid") int uid,
    	@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = WineFile(fn);
    	WineView wineView = new WineView(fileName);
		RestResponse wine = wineView.viewInfo(uid, Vin.defaultSubsFile, Vin.defaultShipFile);
	    return wine.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/wines/{wid}", method = RequestMethod.GET)
    public ResponseEntity<Object> getWine
    (
    	@PathVariable(value = "uid") int uid,
    	@PathVariable(value = "wid") int wid,
    	@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = WineFile(fn);
    	WineView wineView = new WineView(fileName);
		RestResponse wine = wineView.viewInfo(uid, wid, Vin.defaultSubsFile, Vin.defaultShipFile);
	    return wine.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/wines/{wid}/rating", method = RequestMethod.GET)
    public ResponseEntity<Object> getShipment
    (
    	@PathVariable(value = "uid") int uid,
    	@PathVariable(value = "wid") int wid,
    	@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = WineFile(fn);
    	WineView wineView = new WineView(fileName);
		RestResponse wine = wineView.viewRating(uid, wid, Vin.defaultSubsFile, Vin.defaultShipFile);
	    return wine.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/wines/{wid}/rating", method = RequestMethod.POST)
    public ResponseEntity<Object> getShipment
    (
    	@PathVariable(value = "uid") int uid,
    	@PathVariable(value = "wid") int wid,
    	@RequestParam(value = "rating") double rating,
    	@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = WineFile(fn);
    	WineView wineView = new WineView(fileName);
		RestResponse wine = wineView.rate(wid, rating);
	    return wine.response();
    }
    
    //NOTE
    
    @RequestMapping(value = "/sub/{uid}/shipments/{sid}/notes", method = RequestMethod.POST)
	public ResponseEntity<Object> addNote
	(
			@PathVariable(value = "uid")     int uid,
			@PathVariable(value = "sid")     int sid,
			@RequestParam(value = "content") String content,
			@RequestParam(value = "file",    required = false) String fn
	)
	{
		String fileName = NoteFile(fn);
		NoteAddRequest nar = new NoteAdd(content, uid, sid, fileName);
		RestResponse naResp = nar.addNoteShipment();
		return naResp.response();
	}
    
    //PUT no longer supported by html
  	@RequestMapping(value = "/sub/{uid}/shipments/{sid}/notes/{nid}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateNote
    (
      	@PathVariable(value = "uid")      int uid,
      	@PathVariable(value = "sid")      int sid,
      	@PathVariable(value = "nid")      int nid,
  		@RequestParam(value = "content") String content,
  		@RequestParam(value = "file",    required = false) String fn
    )
  	{
  		String fileName = NoteFile(fn);
  		NoteModifyRequest nmr = new NoteModify(uid, sid, nid, content, fileName);
  		RestResponse nmResp = nmr.modifyNote();
  		return nmResp.response();
    }
  	
  	@RequestMapping(value = "/sub/{uid}/shipments/{sid}/notes", method = RequestMethod.GET)
    public ResponseEntity<Object> getNotes
    (
		@PathVariable(value = "uid")  int uid,
		@PathVariable(value = "sid")  int sid,
    	@RequestParam(value = "file", required = false) String fn
    )
    {
		String fileName = NoteFile(fn);
    	NoteView noteView = new NoteView(fileName);
		RestResponse note = noteView.viewInfo(uid, sid);
	    return note.response();
    }
	
	@RequestMapping(value = "/sub/{uid}/shipments/{sid}/notes/{nid}", method = RequestMethod.GET)
    public ResponseEntity<Object> getNote
    (
		@PathVariable(value = "uid")  int uid,
		@PathVariable(value = "sid")  int sid,
		@PathVariable(value = "nid")  int nid,
		@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = NoteFile(fn);
    	NoteView noteView = new NoteView(fileName);
		RestResponse note = noteView.viewInfo(uid, sid, nid);
	    return note.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/shipments/{sid}/note/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteNote
    (
		@PathVariable(value = "uid")   int uid,
		@PathVariable(value = "sid")   int sid,
    	@PathVariable(value = "nid")   int nid,
    	@RequestParam(value = "file", required = false) String fn
    )
	{
    	String fileName = NoteFile(fn);
		NoteDelete ndr = new NoteDelete(fileName);
		RestResponse ndResp = ndr.deleteByIDShipment(uid,sid,nid);
		return ndResp.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/wines/{wid}/notes", method = RequestMethod.GET)
    public ResponseEntity<Object> getNotesWine
    (
		@PathVariable(value = "wid")  int wid,
		@PathVariable(value = "uid")  int uid,
		@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = NoteFile(fn);
    	NoteView noteView = new NoteView(fileName);
		RestResponse note = noteView.viewInfoWine(uid, wid);
	    return note.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/wines/{wid}/notes", method = RequestMethod.POST)
    public ResponseEntity<Object> getNotesWine
    (
		@PathVariable(value = "wid")     int wid,
		@PathVariable(value = "uid")     int uid,
		@RequestParam(value = "content") String content,
		@RequestParam(value = "file",    required = false) String fn
    )
    {
    	String fileName = NoteFile(fn);
    	NoteAddRequest naReq = new NoteAdd(content, wid, uid, fileName);
		RestResponse note = naReq.addNoteWine();
	    return note.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/wines/{wid}/notes/{nid}", method = RequestMethod.GET)
    public ResponseEntity<Object> getNoteWine
    (
		@PathVariable(value = "wid")  int wid,
		@PathVariable(value = "uid")  int uid,
		@PathVariable(value = "nid")  int nid,
		@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = NoteFile(fn);
    	NoteView noteView = new NoteView(fileName);
		RestResponse note = noteView.viewInfoWine(uid, wid, nid);
	    return note.response();
    }
    
    //PUT no longer supported by html
  	@RequestMapping(value = "/sub/{uid}/wines/{wid}/notes/{nid}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateNoteWine
    (
      	@PathVariable(value = "uid")      int uid,
      	@PathVariable(value = "wid")      int wid,
      	@PathVariable(value = "nid")      int nid,
  		@RequestParam(value = "content") String content,
  		@RequestParam(value = "file",    required = false) String fn
    )
  	{
  		String fileName = NoteFile(fn);
  		NoteModifyRequest nmr = new NoteModify(uid, wid, nid, content, fileName);
  		RestResponse nmResp = nmr.modifyNote();
  		return nmResp.response();
    }
  	
  	@RequestMapping(value = "/sub/{uid}/wines/{wid}/note/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteNoteWine
    (
		@PathVariable(value = "uid")   int uid,
		@PathVariable(value = "wid")   int wid,
    	@PathVariable(value = "nid")   int nid,
    	@RequestParam(value = "file", required = false) String fn
    )
	{
    	String fileName = NoteFile(fn);
		NoteDelete ndr = new NoteDelete(fileName);
		RestResponse ndResp = ndr.deleteByIDWine(uid,wid,nid);
		return ndResp.response();
    }
    
    //DELIVERY
    
    @RequestMapping(value = "/sub/{uid}/delivery", method = RequestMethod.POST)
    public ResponseEntity<Object> getDelivery
    (
		@PathVariable(value = "uid")  int uid,
		@RequestParam(value = "day",      required = false) String day,
		@RequestParam(value = "time",     required = false) String time,
		@RequestParam(value = "file",     required = false) String fn
    )
	{
		String fileName = SubFile(fn);
		Delivery deliv = null;
		if (day != null && time != null)
		{
			DayOfWeek dow = DayOfWeek.getDOW(day);
			TimeOfDay tod = TimeOfDay.getTOD(time);
			deliv = new Delivery(dow, tod);
		}
    	SubscriberView subView = new SubscriberView(fileName);
    	RestResponse sub = subView.updateDelivery(uid, deliv);
	    return sub.response();
    }
    
    @RequestMapping(value = "/sub/{uid}/delivery", method = RequestMethod.GET)
    public ResponseEntity<Object> updateDelivery
    (
		@PathVariable(value = "uid")  int uid,
		@RequestParam(value = "file", required = false) String fn
    )
    {
    	String fileName = SubFile(fn);
    	SubscriberView subView = new SubscriberView(fileName);
    	RestResponse sub = subView.viewDelivery(uid);
	    return sub.response();
    }
    
    //methods
    public String SubFile(String fileName)
    {
    	return Vin.SubFile(fileName);
    }
    public String ShipFile(String fileName)
    {
    	return Vin.ShipFile(fileName);
    }
    public String NoteFile(String fileName)
    {
    	return Vin.NoteFile(fileName);
    }
    public String WineFile(String fileName)
    {
    	return Vin.WineFile(fileName);
    }
}