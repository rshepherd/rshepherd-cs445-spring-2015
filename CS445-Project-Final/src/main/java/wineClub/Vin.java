package main.java.wineClub;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {RunSubscriber.class, RunAdmin.class, RunPartner.class})
public class Vin
{
	//public members
	public static String
		defaultSubsFile = "subscribers.txt",
		defaultNoteFile = "notes.txt",
		defaultShipFile = "shipments.txt",
		defaultWineFile = "wines.txt",
		defaultAdmiFile = "admins.txt";
	
    public static void main(String[] args)
    {
        SpringApplication.run(Vin.class, args);
    }
    
    //METHODS
    public static String SubFile(String fileName)
    {
    	return FileName(fileName, defaultSubsFile);
    }
    public static String NoteFile(String fileName)
    {
    	return FileName(fileName, defaultNoteFile);
    }
    public static String WineFile(String fileName)
    {
    	return FileName(fileName, defaultWineFile);
    }
    public static String ShipFile(String fileName)
    {
    	return FileName(fileName, defaultShipFile);
    }
    public static String AdminFile(String fileName)
    {
    	return FileName(fileName, defaultAdmiFile);
    }
    
    //aux-methods
    public static String FileName(String fileName, String alternate)
    {
    	return (fileName != null && !fileName.equals("")) ? fileName : alternate;
    }
    
    public static void ResetFiles()
    {
    	File[] files = {new File(defaultSubsFile), new File(defaultNoteFile),
    			new File(defaultWineFile), new File(defaultShipFile), new File(defaultAdmiFile)};
    	for (File f : files)
    		if(f.exists())
    			f.delete();
    }
}
