package main.java.classes;
import java.io.Serializable;

import main.java.wineClub.Vin;

public class Wine implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//public variables
	private WineVariety wv;
	private WineType    wt;
	private String      labelName;
	private String      grape;   // e.g. Merlot, Chardonnay, Riesling, etc.
	private String      region;  // e.g. Napa, Russian Valley, etc.
	private String      country; // e.g. France, USA, Australia, Chile
	private String      maker;   // the wine maker, e.g. Sterling, Krupp Brother, etc.
	private int         year;    // vintage year
	private int         numberOfRatings = 0;
	private double      rating = 0;
	private int         ID;
	private String      fileName = Vin.defaultWineFile;
	
	//default constructor
	public Wine()
	{
		this.labelName = "Bloody Mary";
		this.grape     = "Merlot";
		this.region    = "Whoville";
		this.country   = "Seussville";
		this.maker     = "The Dr.";
		this.year      = 1969;
		this.wt        = WineType.TABLE;
		this.wv        = WineVariety.ROSE;
		this.ID        = IDGenerator.generateWineID(fileName);
	}
	
	//constructor(s)
	public Wine(String labelname, String grape, String region, String country, 
		String maker, int year, WineType wts, WineVariety wvs, String fn)
	{
		this.labelName = labelname;
		this.grape = grape;
		this.region = region;
		this.country = country;
		this.maker = maker;
		this.year = year;
		this.wt = wts;
		this.wv = wvs;
		this.fileName = fn;
		this.ID = IDGenerator.generateWineID(fileName);
	}
	
	//accessor(s)
	public String getLabelName()
	{
		return this.labelName;
	}
	public String getGrape()
	{
		return this.grape;
	}
	public String getRegion()
	{
		return this.region;
	}
	public String getCountry()
	{
		return this.country;
	}
	public String getMaker()
	{
		return this.maker;
	}
	public int getYear()
	{
		return this.year;
	}
	public WineType getWineType()
	{
		return this.wt;
	}
	public WineVariety getWineVariety()
	{
		return this.wv;
	}
	public int getNumberOfRatings()
	{
		return this.numberOfRatings;
	}
	public double getRating()
	{
		return this.rating;
	}
	public int getID()
	{
		return this.ID;
	}
	
	//mutator(s)
	public void setID(int id)
	{
		this.ID = id;
	}
	public void setLabelName(String label)
	{
		this.labelName = label;
	}
	public void setGrape(String grape)
	{
		this.grape = grape;
	}
	public void setRegion(String region)
	{
		this.region = region;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public void setMaker(String maker)
	{
		this.maker = maker;
	}
	public void setWineType(WineType t)
	{
		this.wt = t;
	}
	public void setWineVariety(WineVariety v)
	{
		this.wv = v;
	}
	public void setYear(int y)
	{
		this.year = y;
	}
	
	public void addRating(double r)
	{
		if (r <= 10 && r >= 0)
		{
			numberOfRatings = numberOfRatings + 1;
			rating = rating * ((double)(numberOfRatings - 1)/numberOfRatings) + (double)r/numberOfRatings;
		}
		else
		{
			System.out.println("Rating must be between 0.0 and 10.0");
		}
	}
	
	//compares wine based on all variables
	public boolean isMatch(Wine w2)
	{
        if( isMatchVariety(w2.getWineVariety()) && 
        	isMatchType(w2.wt)    && 
        	isMatchLabel(w2.getLabelName())   && 
        	isMatchGrape(w2.getGrape())   && 
        	isMatchRegion(w2.getRegion())  && 
        	isMatchCountry(w2.getCountry()) && 
        	isMatchMaker(w2.getMaker())   && 
        	isMatchYear(w2.getYear()) )
        {
            return true;
        } 
        else return false;
	}
	    
    private boolean isMatchVariety(WineVariety wv)
    {
    	return this.getWineVariety().equals(wv);
    }

    private boolean isMatchType(WineType wt)
    {
    	return this.getWineType().equals(wt);
    }
    
    private boolean isMatchLabel(String kw)
    {
        String regex = "(?i).*" + kw + ".*";
        return this.getLabelName().matches(regex);
    }
    
    private boolean isMatchGrape(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.getGrape().matches(regex);
    }
    
    private boolean isMatchRegion(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.getRegion().matches(regex);
    }

    private boolean isMatchCountry(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.getCountry().matches(regex);
    }

    private boolean isMatchMaker(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.getMaker().matches(regex);
    }

    private boolean isMatchYear(int year)
    {
    	return this.getYear() == year;
    }
}
