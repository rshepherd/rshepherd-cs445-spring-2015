package main.java.classes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class Serializer
{
	//private members
	List<Subscriber> subList  = null;
	List<Note>       noteList = null;
	List<Wine>       wineList = null;
	List<Shipment>   shipList = null;
	List<Admin>      adminList = null;
	
	//default constructor
	public Serializer()
	{
		//List<Subscriber> derSubs = null;
	}
	
	//Subscriber methods
	public void serializeSubscriber(List<Subscriber> subs, String fileName)
	{
		try 
		{
			handleFile(fileName);
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(subs);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "resource" })
	public List<Subscriber> deserializeSubscriber(String fileName)
	{
		try
		{
			handleFile(fileName);
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return subList;
			} else {
				ObjectInputStream in = new ObjectInputStream(inputFile);
				subList = (List<Subscriber>)in.readObject();
				in.close();
			}
			inputFile.close();
			
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Subscriber class could not be located");
			c.printStackTrace();
		} catch(Exception e) {
			System.out.println("Error message:\n" + e.getMessage());
			e.printStackTrace();
		}
		return subList;
	}
	
	//Note methods
	public void serializeNote(List<Note> notes, String fileName)
	{
		try 
		{
			handleFile(fileName);
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(notes);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}

	@SuppressWarnings({ "resource", "unchecked" })
	public List<Note> deserializeNote(String fileName)
	{
		try
		{
			handleFile(fileName);
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return noteList;
			} else {
				//System.out.println(inputFile.available());
				ObjectInputStream in = new ObjectInputStream(inputFile);
				noteList = (List<Note>) in.readObject();
				in.close();
			}
			inputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Notes class could not be located");
			c.printStackTrace();
		}
		return noteList;
	}
	
	//Note methods
	public void serializeWine(List<Wine> wines, String fileName)
	{
		try 
		{
			handleFile(fileName);
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(wines);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "resource", "unchecked" })
	public List<Wine> deserializeWine(String fileName)
	{
		try
		{
			handleFile(fileName);
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return wineList;
			} else {
				ObjectInputStream in = new ObjectInputStream(inputFile);
				wineList = (List<Wine>)in.readObject();
				in.close();
			}
			inputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Wines class could not be located");
			c.printStackTrace();
		}
		return wineList;
	}
	
	//Shipment methods
	public void serializeShipment(List<Shipment> ships, String fileName)
	{
		try 
		{
			handleFile(fileName);
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(ships);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}

	@SuppressWarnings({ "resource", "unchecked" })
	public List<Shipment> deserializeShipment(String fileName)
	{
		try
		{
			handleFile(fileName);
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return shipList;
			} else {
				//System.out.println(inputFile.available());
				ObjectInputStream in = new ObjectInputStream(inputFile);
				shipList = (List<Shipment>)in.readObject();
				in.close();
			}
			inputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Shipment class could not be located");
			c.printStackTrace();
		}
		return shipList;
	}
	
	//Admin methods
	public void serializeAdmin(List<Admin> admins, String fileName)
	{
		try 
		{
			handleFile(fileName);
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(admins);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}

	@SuppressWarnings({ "resource", "unchecked" })
	public List<Admin> deserializeAdmin(String fileName)
	{
		try
		{
			handleFile(fileName);
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return adminList;
			} else {
				//System.out.println(inputFile.available());
				ObjectInputStream in = new ObjectInputStream(inputFile);
				adminList = (List<Admin>)in.readObject();
				in.close();
			}
			inputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Admin class could not be located");
			c.printStackTrace();
		}
		return adminList;
	}

	//methods
	public void handleFile(String name) throws IOException
	{
		File file = new File(name);
		if(!file.exists())
			file.createNewFile();
	}
}