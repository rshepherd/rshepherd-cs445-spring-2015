package main.java.classes;

public class Admin
{
	//private members
	private int id;
	private String name;
	
	//constructor(s)
	public Admin(String n)
	{
		this.name = n;
	}
	
	//accessor(s)
	public int getID()
	{
		return this.id;
	}
	public String getName()
	{
		return this.name;
	}
	
	//mutator(s)
	public void setID(int id)
	{
		this.id = id;
	}
	public void setName(String n)
	{
		this.name = n;
	}
}
