package main.java.classes;

import java.io.Serializable;

public class Delivery implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private members
	private DayOfWeek date;
	private TimeOfDay timeOfDay;
	
	//constructor(s)
	public Delivery()
	{
		this.date = DayOfWeek.M;
		this.timeOfDay = TimeOfDay.A;
	}
	
	public Delivery(DayOfWeek d, TimeOfDay tod)
	{
		this.date = d;
		this.timeOfDay = tod;
	}
	
	//accessor(s)
	public DayOfWeek getDOW()
	{
		return this.date;
	}
	public TimeOfDay getTOD()
	{
		return this.timeOfDay;
	}
	
	//mutator(s)
	public void setDOW(DayOfWeek dow)
	{
		this.date = dow;
	}
	public void setTOD(TimeOfDay tod)
	{
		this.timeOfDay = tod;
	}
}
