package main.java.classes;
public enum WineVariety
{
	RED, WHITE, ROSE;
	
	public static WineVariety getVariety(String name)
	{
		for (WineVariety variety : WineVariety.values())
			if (variety.name().equals(name))
				return variety;
		return RED;
	}
}
