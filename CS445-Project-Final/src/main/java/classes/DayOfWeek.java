package main.java.classes;
public enum DayOfWeek
{
	//days of week
	M ("Monday"), T ("Tuesday"), W ("Wednesday"), R ("Thursday"), F ("Friday"), Sa ("Saturday"), Su ("Sunday");
	
	//variable(s)
	private String desc;
	
	//constructor(s)
	private DayOfWeek(String mes)
	{
		this.setDesc(mes);
	}
	
	//accessor(s)
	public String getDesc()
	{
		return desc;
	}
	
	//mutator(s)
	public void setDesc(String desc)
	{
		this.desc = desc;
	}
	
	public static DayOfWeek getDOW(String dow)
	{
		for(int i = 0; i < DayOfWeek.values().length; i++)
		{
			if (DayOfWeek.values()[i].getDesc().equals(dow) || DayOfWeek.values()[i].name().equals(dow))
				return DayOfWeek.values()[i];
		}
		return M;
	}
}
