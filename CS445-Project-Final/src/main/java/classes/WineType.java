package main.java.classes;
public enum WineType
{
	TABLE,SWEET,SPARKLING;
	
	public static WineType getType(String name)
	{
		for (WineType type : WineType.values())
			if (type.name().equals(name))
				return type;
		return TABLE;
	}
}
