package main.java.classes;
import java.util.ArrayList;
import java.util.List;
//import java.util.concurrent.atomic.AtomicInteger;

public class IDGenerator
{
	//private static AtomicInteger IDnum = new AtomicInteger();
	
	public static int generateID(String fileName)
	{
		//return IDnum.incrementAndGet();
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		if (subList != null)
		{
			return subList.get(subList.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
	
	public static int generateNoteID(String fileName)
	{
		List<Note> list = new ArrayList<Note>();
		Serializer ser = new Serializer();
		list = ser.deserializeNote(fileName);
		if (list != null)
		{
			return list.get(list.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
	
	public static int generateWineID(String fileName)
	{
		List<Wine> list = new ArrayList<Wine>();
		Serializer ser = new Serializer();
		list = ser.deserializeWine(fileName);
		if (list != null)
		{
			return list.get(list.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
	
	public static int generateShipmentID(String fileName)
	{
		List<Shipment> list = new ArrayList<Shipment>();
		Serializer ser = new Serializer();
		list = ser.deserializeShipment(fileName);
		if (list != null)
		{
			return list.get(list.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
	
	public static int generateAdminID(String fileName)
	{
		List<Admin> list = new ArrayList<Admin>();
		Serializer ser = new Serializer();
		list = ser.deserializeAdmin(fileName);
		if (list != null)
		{
			return list.get(list.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
}
