package main.java.classes;
import java.io.Serializable;

public enum Status implements Serializable
{
	
	//types of statuses
	C("Cancelled"),
	D("Delivered"),
	P("Pending"),
	R("Returned"),
	S("Shipped");
	
	//private variables
	private static final long serialVersionUID = 1L;
	private String desc;
	
	//constructor(s)
	private Status(String mes)
	{
		this.setDesc(mes);
	}
	
	//accessor(s)
	public String getDesc()
	{
		return desc;
	}

	//mutator(s)
	public void setDesc(String desc)
	{
		this.desc = desc;
	}
	
	public static Status getStatus(String stat)
	{
		for (int i = 0; i < Status.values().length; i++)
		{
			if (stat.equals(Status.values()[i].name()) || stat.equals(Status.values()[i].getDesc()))
				return Status.values()[i];
		}
		return S;
	}
}
