package main.java.classes;
import java.io.Serializable;
import java.time.YearMonth;
//import java.util.List;
//import main.java.rest.NoteView;
import main.java.rest.SubscriberView;
import main.java.wineClub.Vin;

public class Shipment implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private members
	private int subID;
	//private int noteID;
	private Delivery delivery;
	private Wine[] wineList;
	private Status status;
	private YearMonth date;
	private int ID;
	//TODO: implement monthly selection instead
	private int ms;
	//private String noteFileName = Vin.defaultNoteFile;
	private String subsFileName = Vin.defaultSubsFile;
	private String shipFileName = Vin.defaultShipFile;
	
	//default constructor
	public Shipment()
	{
		this.subID  = 0;
		//this.noteID = 0;
		this.status = Status.P;
		this.date   = YearMonth.now();
		this.ID     = IDGenerator.generateShipmentID(shipFileName);
		this.ms     = 0;
	}
	
	//constructor(s)
	public Shipment(int subId, /*int noteId,*/ int mons, Status status, YearMonth date, String fileName)
	{
		this.shipFileName = fileName;
		this.subID  = subId;
		//this.noteID = noteId;
		this.status = status;
		this.date   = date;
		this.ID     = IDGenerator.generateShipmentID(shipFileName);
		this.ms     = mons;
	}
	
	//accessor(s)
	public Subscriber getSub()
	{
		SubscriberView sv = new SubscriberView(subsFileName);
		return sv.getSubscriberByID(this.subID);
	}
	public int getSubID()
	{
		return this.subID;
	}
	public int getWineSelection()//WineSelection getWineSelction()
	{
		return this.ms;
	}
	public int getID()
	{
		return this.ID;
	}
	/*public Note getNote()
	{
		NoteView nv = new NoteView(noteFileName);
		List<Note> noteList = nv.getNoteByUidSid(subID, ID);
		return nv.getNoteByID(noteList, this.noteID);
	}*/
	public Wine[] getWineList()
	{
		return wineList;
	}
	/*public int getNoteID()
	{
		return this.noteID;
	}*/
	public Status getStatus()
	{
		return this.status;
	}
	public YearMonth getDate()
	{
		return this.date;
	}
	public Delivery getDelivery()
	{
		return this.delivery;
	}
	
	//mutator(s)
	public void setSubcriber(int subId)
	{
		this.subID = subId;
	}
	public void setWineSelection(int mons)
	{
		this.ms = mons;
	}
	public void setID(int ID)
	{
		this.ID = ID;
	}
	/*public void setNote(int noteId)
	{
		this.noteID = noteId;
	}*/
	public void setStatus(Status status)
	{
		this.status = status;
	}
	public void setDate(YearMonth date)
	{
		this.date = date;
	}
	
	//compares shipment based on its variables
	public boolean isMatch(Shipment ship)
	{
		if ( (getSubID() == ship.subID) //&& (getNoteID() == ship.noteID)
			&& (getWineSelection() == ship.ms) && (getStatus().equals(ship.status)) )
		{
			return true;
		}
		else
			return false;
	}
}