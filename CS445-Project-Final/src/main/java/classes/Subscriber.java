package main.java.classes;
import java.io.Serializable;

public class Subscriber implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private variables
	private String name, email, phone, facebook, twitter;
	private Address address;
	private int ID;
	private WineSelection ws;
	private String fileName;
	private Delivery delivery;
	
	//default constructor
	public Subscriber(){
		this.name = "Jane Doe";
    	this.email = "jane.doe@example.com";
    	this.phone = "1234567890";
    	this.address = new Address();
    	this.facebook = "";
		this.twitter = "";
		this.fileName = "subscribers.txt";
    	this.ID = IDGenerator.generateID(fileName);
    	this.delivery = new Delivery();
		this.ws = WineSelection.AR;
	}
	
	//constructor(s)
	public Subscriber(String name, String email, String phone, String facebook,
		String twitter, Address address, Delivery d, String fn)
	{
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.facebook = facebook;
		this.twitter = twitter;
		this.fileName = fn;
		this.ID = IDGenerator.generateID(fileName);
		this.delivery = (d == null) ? new Delivery() : d;
		setWineSelection(WineSelection.AR);
	}
	public Subscriber(String name, String email, String phone, Address address, Delivery d, String fn)
	{
		this.name     = name;
		this.email    = email;
		this.phone    = phone;
		setFacebook("");
		setTwitter("");
		setAddress(address);
		this.fileName = fn;
		this.ID       = IDGenerator.generateID(fileName);
		setWineSelection(WineSelection.AR);
	}
	
	//accessor(s)
	public String getName()
	{
		return this.name;
	}
	public String getEmail()
	{
		return this.email;
	}
	public String getPhone()
	{
		return this.phone;
	}
	public String getFacebook()
	{
		return this.facebook;
	}
	public String getTwitter()
	{
		return this.twitter;
	}
	public int getID()
	{
		return this.ID;
	}
	public WineSelection getSelection()
	{
		return this.ws;
	}
	public Address getAddress()
	{
		return this.address;
	}
	public Delivery getDelivery()
	{
		return this.delivery;
	}
	
	//mutator(s)
	public void setName(String name)
	{
		this.name = name;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	public void setFacebook(String facebook)
	{
		this.facebook = facebook;
	}
	public void setTwitter(String twitter)
	{
		this.twitter = twitter;
	}
	public void setWineSelection(WineSelection ws)
	{
		this.ws = ws;
	}
	public void setAddress(Address address)
	{
		this.address = address;
	}
	public void setID(int id)
	{
		this.ID = id;
	}
	public void setDelivery(Delivery d)
	{
		this.delivery = d;
	}
	
	//compares subscriber by checking its variables
	public boolean isMatch(Subscriber sub)
	{
		if( getName().equals(sub.name)   &&
			getEmail().equals(sub.email) &&
			getPhone().equals(sub.phone) &&
			getID()==(sub.ID) &&
			getFacebook().equals(sub.facebook) &&
			getTwitter().equals(sub.twitter) &&
			getAddress().equals(sub.address) &&
			getSelection().equals(sub.ws) &&
			getDelivery().equals(sub.delivery))
		{
			return true;
		}
		else
			return false;
	}
}
