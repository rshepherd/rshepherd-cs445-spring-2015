package main.java.classes;
public enum TimeOfDay
{
	//types of wine selections
	M ("morning"), A ("afternoon"), N ("night");
	
	//variable(s)
	private String desc;
	
	//constructor(s)
	private TimeOfDay(String mes)
	{
		this.setDesc(mes);
	}
	
	//accessor(s)
	public String getDesc()
	{
		return desc;
	}
	
	//mutator(s)
	public void setDesc(String desc)
	{
		this.desc = desc;
	}
	
	public static TimeOfDay getTOD(String tod)
	{
		for(int i = 0; i < TimeOfDay.values().length; i++)
		{
			if (TimeOfDay.values()[i].getDesc().equals(tod) || TimeOfDay.values()[i].name().equals(tod))
				return TimeOfDay.values()[i];
		}
		return A;
	}
}
