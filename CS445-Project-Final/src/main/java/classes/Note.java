package main.java.classes;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Note implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private members
	private int ID;
	private String content;
	private int subID, shipID, wineID;
	private String fileName;
	
	//default constructor
	public Note()
	{
		this.content = "";
		this.fileName = "notes.txt";
	}
	
	//constructor(s)
	public Note(String note, int subId, int shipID, int wineID, String fn)
	{
		this.content  = note;
		this.subID    = subId;
		this.fileName = fn;
		this.shipID   = shipID;
		this.wineID   = wineID;
	}
	
	//accessor(s)
	public String getContent()
	{
		return this.content;
	}
	public int getSubID()
	{
		return this.subID;
	}
	public int getShipID()
	{
		return this.shipID;
	}
	public int getWineID()
	{
		return this.wineID;
	}
	public int getID()
	{
		return this.ID;
	}
	
	//mutator(s)
	public void setID(int id)
	{
		this.ID = id;
	}
	public void setSubID(int id)
	{
		this.subID = id;
	}
	public void setShipID(int id)
	{
		this.shipID = id;
	}
	public void setWineID(int id)
	{
		this.wineID = id;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	
	//checks length
	public static boolean checkNote(String content)
	{
		if( (content.length()>=128) && (content.length()<=1024) )
			return true;
		else
			return false;
	}
	
	public void writeNote(Note note)
	{
		List<Note> notes = new ArrayList<Note>();
		Serializer ser = new Serializer();
		notes = ser.deserializeNote(fileName);
		if(notes == null)
		{
			notes = new ArrayList<Note>();
		}
		notes.add(note);
		ser.serializeNote(notes, fileName);
	}
	
	//compares address given all of its variables
	public boolean isMatch(Note note)
	{
		if( isMatchContent(note.content) &&
			this.subID == (note.subID) )
		{
			return true;
		}
		else
			return false; 
	}
	
	private boolean isMatchContent(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.content.matches(regex);
    }
}
