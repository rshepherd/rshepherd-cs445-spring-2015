package main.java.classes;
import java.io.Serializable;

public class Address implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//public variables
	private String street, city, zip , state;
	
	//default constructor
    public Address()
    {
        this.street = "123 Main ST, Apt 2F";
        this.city = "Anytown";
        this.state = "Anystate";
        this.zip = "12345";
    }
	
	//constructor(s)
	public Address(String street, String city, String zip, String state)
	{
		this.street = street;
		this.city   = city;
		this.zip    = zip;
		this.state  = state;
	}
	
	//accessor(s)
	public String getCity()
	{
		return this.city;
	}
	public String getState()
	{
		return this.state;
	}
	public String getZip()
	{
		return this.zip;
	}
	public String getStreet()
	{
		return this.street;
	}
	
	//mutator(s)
	public void setCity(String city)
	{
		this.city = city;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public void setZip(String zip)
	{
		this.zip = zip;
	}
	public void setStreet(String street)
	{
		this.street = street;
	}
	
	//compares address given all of its variables
	public boolean isMatch(Address add)
	{
		if( isMatchCity(add.city)     &&
			isMatchZip(add.zip)       &&
			isMatchStreet(add.street) &&
			isMatchState(add.state) )
		{
			return true;
		}
		else
			return false; 
	}
	
	private boolean isMatchCity(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.getCity().matches(regex);
    }
	
	private boolean isMatchZip(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.getZip().matches(regex);
    }
	
	private boolean isMatchStreet(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.getStreet().matches(regex);
    }
	
	private boolean isMatchState(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.getState().matches(regex);
    }
}
