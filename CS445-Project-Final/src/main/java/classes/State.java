package main.java.classes;
//import java.io.Serializable;

public enum State
{
	//illegal states
	Alabama        ("AL",false),
	Arkansas       ("AR",false),
	Delaware       ("DE",false),
	Kentuky        ("KY",false),
	Massachusetts  ("MA",false),
	Mississippi    ("MS",false),
	Oklahoma       ("OK",false),
	Pennsylvania   ("PA",false),
	South_Dakota   ("SD",false),
	Utah           ("UT",false),
	//legal states
	Alaska         ("AK",true),
	Arizona        ("AZ",true),
	California     ("CA",true),
	Colorado       ("CO",true),
	Connecticut    ("CT",true),
	Florida        ("FL",true),
	Georgia        ("GA",true),
	Hawaii         ("HI",true),
	Idaho          ("ID",true),
	Illinois       ("IL",true),
	Indiana        ("IN",true),
	Iowa           ("IA",true),
	Kansas         ("KS",true),
	Louisiana      ("LA",true),
	Maine          ("ME",true),
	Maryland       ("MD",true),
	Michigan       ("MI",true),
	Minnesota      ("MN",true),
	Missouri       ("MO",true),
	Montana        ("MT",true),
	Nebraska       ("NE",true),
	Nevada         ("NV",true),
	New_Hampshire  ("NH",true),
	New_Jersey     ("NJ",true),
	New_Mexico     ("NM",true),
	New_York       ("NY",true),
	North_Carolina ("NC",true),
	North_Dakota   ("ND",true),
	Ohio           ("OH",true),
	Oregon         ("OR",true),
	Rhode_Island   ("RI",true),
	South_Carolina ("SC",true),
	Tennessee      ("TN",true),
	Texas          ("TX",true),
	Vermont        ("VT",true),
	Virginia       ("VA",true),
	Washington     ("WA",true),
	West_Virginia  ("WV",true),
	Wisconsin      ("WI",true),
	Wyoming        ("WY",true);
	
	//variable(s)
	private String  abbrev;
	private boolean isLegal;
	
	//constructor(s)
	private State(String abbrev, boolean isLegal)
	{
		this.setAbbrev(abbrev);
		this.setLegality(isLegal);
	}

	//accessor(s)
	public String getAbbrev()
	{
		return abbrev;
	}
	
	public boolean getLegality()
	{
		return isLegal;
	}

	//mutator(s)
	public void setAbbrev(String abbrev)
	{
		this.abbrev = abbrev;
	}
	
	public void setLegality(boolean legal)
	{
		this.isLegal = legal;
	}
}
