package main.java.classes;
//import java.util.ArrayList;
//import java.util.Iterator;
import java.util.List;
import java.io.Serializable;
import java.time.*;

public class MonthlySelection implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private members
	private int ID;
	private YearMonth date;
	private int[] wl = new int[6];
	protected double price;
	
	//constructor(s)
	public MonthlySelection()
	{
		//next month's selection
		this.date = YearMonth.now().plusMonths(1);
	}
	
	public MonthlySelection(YearMonth ym, double p)
	{
		//Must be in the yyyy-mm format
		this.date = ym;
		this.price = p;
	}
	
	//accessor(s)
	public int getID()
	{
		return this.ID;
	}
	//TODO: FINISH
	public List<Wine> getWines()
	{
		return null;//this.wl;
	}
	public YearMonth getDate()
	{
		return this.date;
	}	
	public double getPrice()
	{
		return this.price;
	}
	
	//mutator(s)
	public void setPrice(double p)
	{
		this.price = p;
	}
	public void setWine(int[] ids)
	{
		for(int i = 0; i < ids.length && i < this.wl.length; i++)
		{
			this.wl[i] = ids[i];
		}
	}
	
	public boolean isMatch(MonthlySelection ms)
	{
		if( ms.isMatch(getWines())
			&& ms.date.equals(getDate())
			&& ms.ID == getID())
			return true;
		return false;
	}
	
	public boolean isMatch(List<Wine> wl)
	{
		List<Wine> list = getWines();
		if (list.size() != wl.size())
			return false;
		for (int i = 0; i < wl.size(); i++)
			if (list.get(i) != wl.get(i))
				return false;
		return true;
	}
}
