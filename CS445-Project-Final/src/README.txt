Starting with a fresh copy of Ubuntu 14.04
------------------------------------------
1 - Execute these terminal commands
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
sudo apt-get install oracle-java8-set-default
sudo apt-get install maven
sudo apt-get install git

2 - Clone the project repository, type
git clone https://rshepherd@bitbucket.org/rshepherd/rshepherd-cs445-spring-2015.git

3 - Enter project folder, type
cd rshepherd-cs445-spring-2015/CS445-Project-Final

4 - Build and test the project, type (Maven is the build script)
sudo mvn clean
sudo mvn package

5 - Build and run application, from project directory type
cd target
java -jar CS445-Project-Final-0.0.1-SNAPSHOT.jar

6a - For test coverage, from project directory type
sudo mvn cobertura:cobertura

6b - Test coverage is located at ./target/site/cobertura/index.html