import org.apache.commons.cli.*;

public class Generator
{
	@SuppressWarnings("static-access")
	public static void main(String[] args)
	{
		//Using Apache CLI to parse user arguments
		Options options = new Options();
		//variables
		options.addOption(OptionBuilder.withLongOpt("algorithm")
                .withDescription("The algorithm used to generate a LaTeX document")
                .hasArg()
                .withArgName("ALGORITHM")
                .isRequired()
                .create('a'));
		options.addOption(OptionBuilder.withLongOpt("size")
                .withDescription("How many elements are in the permuted array")
                .hasOptionalArg()
                .withArgName("SIZE")
                .create('s'));
		options.addOption(OptionBuilder.withLongOpt("help")
				.withDescription("Displays help menu for arguments and usage")
				.create('h'));
		options.addOption(OptionBuilder.withLongOpt("version")
                .withDescription("Version number of the program")
                .create('v'));
		
		for (int i = 0; i < args.length; i++)
		{
			if (args[i].toLowerCase().equals("-h") || args[0].toLowerCase().equals("--help"))
			{
				String header = "\nGenerate a LaTeX sorting algorithm document\n"
					+ "- Give the algorithm and size of an array to output all possibilities of its evaluation\n\n";
				String footer = "\nPlease report issues at rshepherd1994@gmail.com\n";
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("Generator", header, options, footer, true);
			}
			else if (args[i].toLowerCase().equals("-v") || args[0].toLowerCase().equals("--version"))
			{
				System.out.println("Version: Richard Shepherd\'s \"Sorting Algorithm LaTeX Generator, 1.0.0\""
					+ "\nDeveloped for Professor Reingold at Illinois Institute of Technology"
					+ "\nMost recent version: July 25th, 2015");
			}
		}
		
		try
		{
			CommandLine line = new BasicParser().parse(options, args);
			String alg1  = line.getOptionValue("a");
			String alg2  = line.getOptionValue("alg");
			String alg3  = line.getOptionValue("algorithm");
			String size1  = line.getOptionValue("s");
			String size2  = line.getOptionValue("size");
			String[] alg = {alg1, alg2, alg3};
			String[] size = {size1, size2};
			
			int arg1 = 4;
			
			for (int i = 0; i < size.length; i++)
			{
				if (size[i] != null)
				{
					try
					{
						arg1 = Integer.parseInt(size[i]);
					}
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						arg1 = 4;
					}
				}
			}
			
			for (int i = 0; i < alg.length; i++)
			{
				if(alg[i] != null) 
				{
					String algorithm = alg[i];
					SortingAlgorithm sortingAlg = new QuickSort();
					switch(algorithm.toLowerCase())
					{
						case "bubblesort":
						case "bubble":
						case "b":
						case "bsort":
							sortingAlg = new BubbleSort();
							break;
						case "heapsort":
						case "heap":
						case "h":
						case "hsort":
							sortingAlg = new HeapSort();
							break;
						case "insertionsort":
						case "insertion":
						case "i":
						case "isort":
							sortingAlg = new InsertionSort();
							break;
						case "mergesort":
						case "merge":
						case "m":
						case "msort":
							sortingAlg = new MergeSort();
							break;
						case "quicksort":
						case "quick":
						case "q":
						case "qsort":
							sortingAlg = new QuickSort();
							break;
						case "selectionsort":
						case "selection":
						case "s":
						case "ssort":
							sortingAlg = new SelectionSort();
							break;
						default:
							break;
					}
					sortingAlg.sort(arg1);
				}
			}
		}
		catch( ParseException exp )
		{
		    System.out.println( "Unexpected exception: " + exp.getMessage() );
		}
	}
}


