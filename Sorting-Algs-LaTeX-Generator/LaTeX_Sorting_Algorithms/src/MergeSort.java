
public class MergeSort implements SortingAlgorithm
{
	private String[] resultSet;
	private int size;
	
	public MergeSort()
	{
		resultSet = new String[0];
	}
	
	public void setup(int arg0, String args[])
	{
		size = arg0;
	}
	
	public void sort(int arg0)
	{
		setup(arg0, null);
		sort();
	}
	
	public void sort()
	{
		int[] array;
		array = IntArray.Default(size);
		int[][] array2d = Int2DArray.Default(size);
		StringArray.setup("./LaTeX/mergeSort.tex");
		StringArray.latexTitle("Merge Sort", size);
		int[] temp = new int[size];
		//pre-calculates the order of merge-sorts so there is persistence with the temp list
		int[][] sorts = GetIndices(new int[0][2], 0, array.length-1);
		sort(array, temp, array2d, sorts, 1, 0, 0, 0, 0, 0, 0, 0, true);
		StringArray.latexCommitFile();
	}
	
	private void sort(int[] array, int[] temp, int[][] array2d, int[][] sorts, int func,
		int lowMerge, int middleMerge, int highMerge, int i, int j, int k, int deep, boolean determinate) //int lowSort,int highSort
	{
		switch(func)
		{
			case 1:
				if ((sorts != null) && (sorts.length > 0))
				{
					int low = sorts[0][0];
					int high = sorts[0][1];
		            int middle = low + (high - low) / 2;
		            //sort(low, middle);
		            //sort(middle + 1, high);
		            // set up the temp list
		            for (int itr8r = low; itr8r <= high; itr8r++) //lowMerge - highMerge
					{
			            temp[itr8r] = array[itr8r];
			        }
		            // Now merge both sides
		            sort(array, temp, array2d, sorts, 2, low, middle, high, low, middle+1, low, deep, true);
				}
				else
				{
					//prints the elements in order from smallest-to-largest each time the recursion bottoms-out
					if (Int2DArray.isDone(array2d))
					{
						String elements = Int2DArray.printResult(array2d);
						if (!StringArray.isIn(resultSet, elements))
						{
							StringArray.latexResult(elements, deep);
							resultSet = StringArray.add(resultSet, elements);
							//System.out.println(elements);
						}
					}
				}
				break;
			case 2:
		        //int i = lowMerge, j = middleMerge + 1, k = lowMerge;
		        if (i <= middleMerge && j <= highMerge)
		        {
		        	if (array2d[temp[i]-1][temp[j]-1] != 42)
		        	{
		        		int val;
		        		if (determinate = true)
		        			StringArray.latexNode(temp[i], temp[j], deep);
			            if ((val = array2d[temp[i]-1][temp[j]-1]) == -1) // temp[i]<=temp[j]
			            {
			                array[k] = temp[i];
			                i++;
			            }
			            else
			            {
			                array[k] = temp[j];
			                j++;
			            }
			            k++;
			            if (determinate == true)
						{
							StringArray.latexOpenBracket(deep);
							if (val == 1)
								StringArray.latexNull(deep + 1);
							sort(array, temp, array2d, sorts, 2, lowMerge, middleMerge, highMerge, i, j, k, deep + 1, true);
							if (val == -1)
								StringArray.latexNull(deep + 1);
							StringArray.latexCloseBracket(deep);
						}
						else
							sort(array, temp, array2d, sorts, 2, lowMerge, middleMerge, highMerge, i, j, k, deep, true);
			            return;
		        	}
		        	else
		        	{
		        		StringArray.latexNode(temp[i], temp[j], deep);
						//System.out.println(array[pos] + ":" + array[y]);
						//painful but necessary, need to make deep copies or else passing-by-reference ruins the data
						int[][] arr1 = Int2DArray.Copy(array2d), arr2 = Int2DArray.Copy(array2d);
						int[] array1, array2;
						array1 = IntArray.Copy(array); array2 = IntArray.Copy(array);
						//take the 2D-array of known comparisons and evaluate it both ways
						//first element < second element
						arr1[temp[i] - 1][temp[j] - 1] = -1;
						arr1[temp[j] - 1][temp[i] - 1] = 1;
						//first element > second element
						arr2[temp[i] - 1][temp[j] - 1] = 1;
						arr2[temp[j] - 1][temp[i] - 1] = -1;
						//extrapolate element orders
						for (int itr8r = 0; itr8r < arr1.length; itr8r++)
						{
							Int2DArray.verifyResult(arr1, temp[i] - 1, itr8r);
							Int2DArray.verifyResult(arr1, itr8r, temp[i] - 1);
							Int2DArray.verifyResult(arr2, temp[i] - 1, itr8r);
							Int2DArray.verifyResult(arr2, itr8r, temp[i] - 1);
						}
						//evaluate less-than and greater-than cases
						StringArray.latexOpenBracket(deep);
						sort(array1, temp, arr1, sorts, 2, lowMerge, middleMerge, highMerge, i, j, k, deep + 1, false);
						sort(array2, temp, arr2, sorts, 2, lowMerge, middleMerge, highMerge, i, j, k, deep + 1, false);
						StringArray.latexCloseBracket(deep);
						return;
		        	}
		        }
		        while (i <= middleMerge)
		        {
		            array[k] = temp[i];
		            k++;
		            i++;
		        }
		        sorts = Int2DArray.remove(sorts, 0);
		        sort(array, temp, array2d, sorts, 1, 0, 0, 0, 0, 0, 0, deep, true);
				break;
			default:
				break;
		}
	}
	
	private int[][] GetIndices(int[][] result, int low, int high)
	{
		int[][] returnInt;
		if (low < high)
		{
            int middle = low + (high - low) / 2;
            result = Int2DArray.add(result, GetIndices(result, low, middle));
            result = Int2DArray.add(result, GetIndices(result, middle+1, high));
            int[][] set = {{low, high}};
			result = Int2DArray.add(result, set);
			returnInt = result;
        }
		else
			returnInt = null;
		return returnInt;
	}
}