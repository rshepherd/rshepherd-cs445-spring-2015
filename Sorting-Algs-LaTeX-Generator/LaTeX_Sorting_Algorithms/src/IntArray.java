
public class IntArray
{
	public static int[] Default(int size)
	{
		int[] result = new int[size];
		for (int i = 0; i < size; i++)
		{
			result[i] = i + 1;
		}
		return result;
	}
	
	public static int[] Copy(int[] arg0)
	{
		int[] result = new int[arg0.length];
		for (int i = 0; i < arg0.length; i++)
		{
			result[i] = arg0[i];
		}
		return result;
	}
	
	public static int[] add(int[] array, int element)
	{
		int[] result = new int[array.length + 1];
		for (int i = 0; i < array.length; i++)
		{
			result[i] = array[i];
		}
		result[array.length] = element;
		return result;
	}
	
	public static int[] remove(int[] array)
	{
		if (array.length > 0)
		{
			int[] result = new int[array.length - 1];
			for (int i = 0; i < array.length - 1; i++)
			{
				result[i] = array[i];
			}
			return result;
		}
		else
			return array;
	}
	
	public static int[] swap(int[] arg0, int x, int y)
	{
		int[] result = Copy(arg0);
		int temp = arg0[x];
		result[x] = arg0[y];
		result[y] = temp;
		return result;
	}
}
