
public class HeapSort implements SortingAlgorithm
{
	private String[] resultSet;
	private int size;
	
	public HeapSort()
	{
		resultSet = new String[0];
	}
	
	public void setup(int arg0, String args[])
	{
		size = arg0;
	}
	
	public void sort(int arg0)
	{
		setup(arg0, null);
		sort();
	}
	
	public void sort()
	{
		int[] array;
		array = IntArray.Default(size);
		int[][] array2d = Int2DArray.Default(size);
		StringArray.setup("./LaTeX/heapSort.tex");
		StringArray.latexTitle("Heap Sort", size);
		int it2 = array.length / 2, it3 = array.length - 1;
		sort(array, array2d, 3, 0, it2, it3, 0, array.length, 0, true, true);
		StringArray.latexCommitFile();
	}
	
	private void sort(int[] array, int[][] array2d, int func, int prevFunc, int it2, int it3,
			int i, int heapSize, int deep, boolean det1, boolean det2)
	{
		switch (func)
		{
			//MAX-HEAPIFY
			case 1:
				int left = left(i), right = right(i), largest = i, val1 = 0, val2 = 0;
				if (left < heapSize)
				{
					if ((val1 = array2d[array[left]-1][array[i]-1]) == 1)
						largest = left;
					else if (val1 == 42)
					{
						StringArray.latexNode(array[left], array[i], deep);
						//painful but necessary, need to make deep copies or else passing-by-reference ruins the data
						int[][] arr1 = Int2DArray.Copy(array2d), arr2 = Int2DArray.Copy(array2d);
						int[] array1, array2;
						array1 = IntArray.Copy(array); array2 = IntArray.Copy(array);
						//take the 2D-array of known comparisons and evaluate it both ways
						//first element < second element
						arr1[array[left] - 1][array[i] - 1] = -1;
						arr1[array[i] - 1][array[left] - 1] = 1;
						//first element > second element
						arr2[array[left] - 1][array[i] - 1] = 1;
						arr2[array[i] - 1][array[left] - 1] = -1;
						//extrapolate element orders
						for (int itr8r = 0; itr8r < arr1.length; itr8r++)
						{
							Int2DArray.verifyResult(arr1, array[left] - 1,itr8r);
							Int2DArray.verifyResult(arr1,itr8r, array[left] - 1);
							Int2DArray.verifyResult(arr2, array[left] - 1,itr8r);
							Int2DArray.verifyResult(arr2,itr8r, array[left] - 1);
						}
						//evaluate less-than and greater-than cases
						StringArray.latexOpenBracket(deep);
						sort(array1, arr1, 1, prevFunc, it2, it3, i, heapSize, deep + 1, false, det2);
						sort(array2, arr2, 1, prevFunc, it2, it3, i, heapSize, deep + 1, false, det2);
						StringArray.latexCloseBracket(deep);
						return;
					}
				}
				if (right < heapSize)
				{
					if ((val2 = array2d[array[right]-1][array[largest]-1]) == 1)
						largest = right;
					else if (val2 == 42)
					{
						StringArray.latexNode(array[i], array[right], deep);
						//painful but necessary, need to make deep copies or else passing-by-reference ruins the data
						int[][] arr1 = Int2DArray.Copy(array2d), arr2 = Int2DArray.Copy(array2d);
						int[] array1, array2;
						array1 = IntArray.Copy(array); array2 = IntArray.Copy(array);
						//take the 2D-array of known comparisons and evaluate it both ways
						//first element < second element
						arr1[array[right] - 1][array[largest] - 1] = -1;
						arr1[array[largest] - 1][array[right] - 1] = 1;
						//first element > second element
						arr2[array[right] - 1][array[largest] - 1] = 1;
						arr2[array[largest] - 1][array[right] - 1] = -1;
						//extrapolate element orders
						for (int itr8r = 0; itr8r < arr1.length; itr8r++)
						{
							Int2DArray.verifyResult(arr1, array[right] - 1,itr8r);
							Int2DArray.verifyResult(arr1,itr8r, array[right] - 1);
							Int2DArray.verifyResult(arr2, array[right] - 1,itr8r);
							Int2DArray.verifyResult(arr2,itr8r, array[right] - 1);
						}
						//evaluate less-than and greater-than cases
						StringArray.latexOpenBracket(deep);
						sort(array2, arr2, 1, prevFunc, it2, it3, i, heapSize, deep + 1, det1, false);
						sort(array1, arr1, 1, prevFunc, it2, it3, i, heapSize, deep + 1, det1, false);
						StringArray.latexCloseBracket(deep);
						return;
					}
				}
				if (largest != i)
				{
					array = IntArray.swap(array, i, largest);
					sort(array, array2d, 1, prevFunc, it2, it3, largest, heapSize, deep, true, true);
				}
				else
				{
					if (prevFunc == 2)
						sort(array, array2d, 2, 1, it2 - 1, it3, 0, heapSize, deep, true, true);
					else
						sort(array, array2d, 3, 1, 0, it3 - 1, 0, heapSize, deep, true, true);
				}
				return;
			//BUILD-MAX-HEAP
			case 2:
				//for (int itr8r = array.length / 2; itr8r >= 0; itr8r--)
				if (it2 >= 0)
					sort(array, array2d, 1, 2, it2, it3, it2, heapSize, deep, true, true);
				else
					sort(array, array2d, 3, 2, 0, it3, 0, heapSize, deep, true, true);
				return;
			//HEAPSORT
			case 3:
				if (prevFunc == 0)
				{
					sort(array, array2d, 2, 3, it2, it3, 0, heapSize, deep, true, true);
					return;
				}
				//for (int itr8r = array.length - 1; itr8r >= 0; itr8r--)
				if (it3 >= 0)
				{
					array = IntArray.swap(array, 0, it3);
					heapSize--;
					sort(array, array2d, 1, 3, 0, it3, 0, heapSize, deep, true, true);
				}
				break;
			default:
				break;
		}
		//prints the elements in order from smallest-to-largest each time the recursion bottoms-out
		if (Int2DArray.isDone(array2d))
		{
			String elements = Int2DArray.printResult(array2d);
			if (!StringArray.isIn(resultSet, elements))
			{
				StringArray.latexResult(elements, deep);
				resultSet = StringArray.add(resultSet, elements);
			}
		}
	}
	
	private int left(int i)
	{
		return 2 * i;
	}
	
	private int right(int i)
	{
		return (2 * i) + 1;
	}
}