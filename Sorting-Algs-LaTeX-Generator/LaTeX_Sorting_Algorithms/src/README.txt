Makefile commands and usage:
	"make": compiles the TestCreature file
	"make clean": gets rid of *.class files
Running programs:
	Generator: If you have java installed, type "java Generator -a {algorithm} -s {size}"
	- supported algorithms: QuickSort, Insertion Sort, Bubble Sort, Selection Sort, Heap Sort
Executing in Windows terminal:
	Compile (from src/ folder): javac -g -d ../bin ./*.java
	Running: java Generator [-h/--help] [-v/--version] -a {algorithm} -s {size}