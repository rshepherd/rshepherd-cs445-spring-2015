
public class Int2DArray
{	
	public static int[][] Default(int size)
	{
		int[][] result = new int[size][size];
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				boolean isEqual = (i==j);
				if (isEqual != true)
					result[i][j] = 42;
				else
					result[i][j] = 0;
			}
		}
		return result;
	}
	
	public static int[][] add(int[][] arg0, int[] arg1)
	{
		int[][] result = new int[arg0.length + 1][arg1.length];
		for (int i = 0; i < arg0.length; i++)
		{
			for (int j = 0; j < arg1.length; j++)
			{
				result[i][j] = arg0[i][j];
			}
		}
		for (int i = 0; i < arg1.length; i++)
		{
			result[arg0.length][i] = arg1[i];
		}
		return result;
	}
	
	public static int[][] add(int[][] arg0, int[][] arg1)
	{
		if (arg1 != null)
		{
			for (int i = 0; i < arg1.length; i++)
			{
				boolean okay = true;
				for (int j = 0; j < arg1[0].length; j++)
				{
					for (int k = 0; k < arg0.length; k++)
					{
						if (arg1[i][j] != arg0[k][j])
						{
							break;
						}
						if (j == arg1[0].length - 1)
						{
							okay = false;
							break;
						}
					}
				}
				if (okay == true)
					arg0 = add(arg0, arg1[i]);
			}
		}
		return arg0;
	}
	
	public static int[][] remove(int[][] arg0, int index)
	{
		if ((arg0 != null) && (arg0.length > 0))
		{
			int[][] result = new int[arg0.length-1][arg0[0].length];
			int itr8r = 0;
			for (int i = 0; i < arg0.length; i++)
			{
				if (i != index)
					result[itr8r++] = arg0[i];
			}
			return result;
		}
		return arg0;
	}
	
	public static int[][] Copy(int[][] arg0)
	{
		int[][] result = new int[arg0.length][arg0.length];
		for (int i = 0; i < arg0.length; i++)
		{
			for (int j = 0; j < arg0.length; j++)
			{
				result[i][j] = arg0[i][j];
			}
		}
		return result;
	}
	
	public static boolean isDone(int[][] arg0)
	{
		for (int i = 0; i < arg0.length; i++)
		{
			for (int j = 0; j < arg0[i].length; j++)
			{
				if (arg0[i][j] == 42)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	public static String printResult(int[][] arg0)
	{
		int temp = 0;
		String result = "";
		for (int x = 0; x < arg0.length; x++)
		{
			for (int i = 0; i < arg0.length; i++)
			{
				for (int j = 0; j < arg0[i].length; j++)
				{
					if (arg0[i][j] == -1)
					{
						temp++;
					}
				}
				if (temp == arg0.length - 1 - x)
				{
					result += i+1;
					temp = 0;
					break;
				}
				temp = 0;
			}
		}
		return result;
	}
	
	public static void verifyResult(int[][] arg0, int x, int y)
	{
		int result = arg0[x][y];
		if ((result == -1) || (result == 1))
		{
			for (int i = 0; i < arg0.length; i++)
			{
				if (arg0[y][i] == result)
				{
					arg0[x][i] = result;
					arg0[i][x] = result * -1;
				}
			}
		}
	}
	
	public static int[][] returnVerifyResult(int[][] arg0, int x, int y)
	{
		int result = arg0[x][y];
		if ((result == -1) || (result == 1))
		{
			for (int i = 0; i < arg0.length; i++)
			{
				if (arg0[y][i] == result)
				{
					arg0[x][i] = result;
					arg0[i][x] = result * -1;
				}
			}
		}
		return arg0;
	}
}
