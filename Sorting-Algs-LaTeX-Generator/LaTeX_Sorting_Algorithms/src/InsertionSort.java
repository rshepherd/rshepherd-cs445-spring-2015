
public class InsertionSort implements SortingAlgorithm
{
	private String[] resultSet;
	private int size;
	
	public InsertionSort()
	{
		resultSet = new String[0];
	}
	
	public void setup(int arg0, String args[])
	{
		size = arg0;
	}
	
	public void sort(int arg0)
	{
		setup(arg0, null);
		sort();
	}
	
	public void sort()
	{
		int[] array;
		array = IntArray.Default(size);
		int[][] array2d = Int2DArray.Default(size);
		StringArray.setup("./LaTeX/insertionSort.tex");
		StringArray.latexTitle("Insertion Sort", size);
		sort(array, array2d, 1, 1, 0, true);
		StringArray.latexCommitFile();
	}
	
	private void sort(int[] array, int[][] array2d, int x, int y, int deep, boolean determinate)
	{
		if (1 < array.length)
		{
			if (x < array.length)//while (itr8r < array.length)
			{
				//bastardized version of the algorithm, adds elements to corresponding lists
				//...uses a 2D-array of known comparisons
				if (y > 0)
				{
					int val = 0;
					if ((val = array2d[array[y]-1][array[y-1]-1]) != 42)
					{
						if (determinate == true)
							StringArray.latexNode(array[y], array[y-1], deep);
						switch(array2d[array[y]-1][array[y-1]-1])
						{
							case -1: // less
								array = IntArray.swap(array, y, y-1);
								break;
							case 0:  // equal
							default: // 1, greater
								break;
						}
					}
					//SIMULATES NON-DETERMINANCE - this is separate from the actual algorithm
					else
					{
						StringArray.latexNode(array[y-1], array[y], deep);
						//System.out.println(array[y-1] + ":" + array[y]);
						//painful but necessary, need to make deep copies or else passing-by-reference ruins the data
						int[][] arr1 = Int2DArray.Copy(array2d), arr2 = Int2DArray.Copy(array2d);
						int[] array1, array2;
						array1 = IntArray.Copy(array); array2 = IntArray.Copy(array);
						//take the 2D-array of known comparisons and evaluate it both ways
						//first element < second element
						arr1[array[y] - 1][array[y-1] - 1] = -1;
						arr1[array[y-1] - 1][array[y] - 1] = 1;
						//first element > second element
						arr2[array[y] - 1][array[y-1] - 1] = 1;
						arr2[array[y-1] - 1][array[y] - 1] = -1;
						//extrapolate element orders
						for (int i = 0; i < arr1.length; i++)
						{
							Int2DArray.verifyResult(arr1, array[y] - 1, i);
							Int2DArray.verifyResult(arr1, i, array[y] - 1);
							Int2DArray.verifyResult(arr2, array[y] - 1, i);
							Int2DArray.verifyResult(arr2, i, array[y] - 1);
						}
						//evaluate greater-than and less-than cases
						StringArray.latexOpenBracket(deep);
						sort(array2, arr2, x, y, deep + 1, false);
						sort(array1, arr1, x, y, deep + 1, false);
						StringArray.latexCloseBracket(deep);
						return;
					}
					if (determinate == true)
					{
						StringArray.latexOpenBracket(deep);
						if (val == 1)
							StringArray.latexNull(deep + 1);
						sort(array, array2d, x, y - 1, deep + 1, true);
						if (val == -1)
							StringArray.latexNull(deep + 1);
						StringArray.latexCloseBracket(deep);
					}
					else
						sort(array, array2d, x, y - 1, deep, true);
					return;
				}
				sort(array, array2d, x + 1, x + 1, deep, true);
				return;
			}
			//prints the elements in order from smallest-to-largest each time the recursion bottoms-out
			if (Int2DArray.isDone(array2d))
			{
				String elements = Int2DArray.printResult(array2d);
				if (!StringArray.isIn(resultSet, elements))
				{
					StringArray.latexResult(elements, deep);
					resultSet = StringArray.add(resultSet, elements);
					//System.out.println(elements);
				}
			}	
		}
	}
}