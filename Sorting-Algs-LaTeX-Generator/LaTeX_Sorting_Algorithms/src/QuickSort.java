
public class QuickSort implements SortingAlgorithm
{
	private String[] resultSet;
	private int size;
	
	public QuickSort()
	{
		resultSet = new String[0];
	}
	
	public void setup(int arg0, String args[])
	{
		size = arg0;
	}
	
	public void sort(int arg0)
	{
		setup(arg0, null);
		sort();
	}
	
	public void sort()
	{
		int deep = 0; //itr8r = 0;
		int[] array, list = new int[0]; //l, e, g;
		//l = new int[0]; e = new int[0]; g = new int[0];
		array = IntArray.Default(size);
		int[][] array2d = Int2DArray.Default(size);
		StringArray.setup("./LaTeX/quickSort.tex");
		StringArray.latexTitle("QuickSort", size);
		int newJ = 0;
		sort(array, array2d, 0, size-1, newJ - 1, newJ, list, deep, true);
		StringArray.latexCommitFile();
	}
	
	//ALGORITHM (from CLSR)
	//partition(array, left, right);
	//sort(array, left, pivot - 1);
	//sort(array, pivot + 1, right);
	
	private void sort(int[] array, int[][] array2d, int left, int right,
		int i, int j, int[] list, int deep, boolean determinate)
	{
		// for j = left to right - 1
		if (j < right)
		{
			//if array[j] <= array[right]
			int val = array2d[array[j]-1][array[right]-1];
			if (val == 0 || val == -1)
			{
				i = i + 1;
				array = IntArray.swap(array, i, j);
			}
			else if (val == 42)
			{
				StringArray.latexNode(array[j], array[right], deep);
				//painful but necessary, need to make deep copies or else passing-by-reference ruins the data
				int[][] arr1 = Int2DArray.Copy(array2d), arr2 = Int2DArray.Copy(array2d);
				int[] array1, array2, listCopy;
				array1 = IntArray.Copy(array); array2 = IntArray.Copy(array); listCopy = IntArray.Copy(list);
				//take the 2D-array of known comparisons and evaluate it both ways
				//first element < second element
				arr1[array[j] - 1][array[right] - 1] = -1;
				arr1[array[right] - 1][array[j] - 1] = 1;
				//first element > second element
				arr2[array[j] - 1][array[right] - 1] = 1;
				arr2[array[right] - 1][array[j] - 1] = -1;
				//extrapolate element orders
				for (int itr8r = 0; itr8r < arr1.length; itr8r++)
				{
					Int2DArray.verifyResult(arr1, array[right] - 1, itr8r);
					Int2DArray.verifyResult(arr1, itr8r, array[right] - 1);
					Int2DArray.verifyResult(arr2, array[right] - 1, itr8r);
					Int2DArray.verifyResult(arr2, itr8r, array[right] - 1);
				}
				//evaluate greater-than and less-than cases
				StringArray.latexOpenBracket(deep);
				sort(array1, arr1, left, right, i, j, listCopy, deep + 1, false);
				sort(array2, arr2, left, right, i, j, listCopy, deep + 1, false);
				StringArray.latexCloseBracket(deep);
				return;
			}
			sort(array, array2d, left, right, i, j+1, list, deep, true);
			return;
		}
		else
		{
			array = IntArray.swap(array, i + 1, right);
	      	int pivot = i + 1;
	      	int el1 = left, el2 = pivot -1, el3 = pivot + 1, el4 = right;
	      	if (((el1 >= 0 && el1 < el2) || (el3 >= 0 && el3 < el4 + 1)) || list.length >= 2)
	      	{
	      		int newJ;
	      		if (pivot >= 0)
		      	{
		      		if (el3 < el4 + 1)
		      		{
		      			list = IntArray.add(list, el4);
		      			list = IntArray.add(list, el3);
		      		}
			      	if (el1 < el2)
			      	{
			      		newJ = el1;
			      		sort(array, array2d, el1, el2, newJ - 1, newJ, list, deep, true);
			      		return;
			      	}
	      		}
		      	if (list.length >= 2)
		      	{
		      		el3 = list[list.length - 1];
		      		list = IntArray.remove(list);
		      		el4 = list[list.length - 1];
		      		list = IntArray.remove(list);
		      		newJ = el3;
		      		sort(array, array2d, el3, el4, newJ - 1, newJ, list, deep, true);
		      		return;
		      	}
	      	}
	      	if (Int2DArray.isDone(array2d))
    		{
    			String elements = Int2DArray.printResult(array2d);
    			if (!StringArray.isIn(resultSet, elements))
    			{
    				StringArray.latexResult(elements, deep);
    				resultSet = StringArray.add(resultSet, elements);
    			}
    		}
		}
	}
}