import java.io.PrintWriter;

public class StringArray
{
	public static PrintWriter pw;
	
	public static String[] add(String[] array, String element)
	{
		String[] result = new String[array.length + 1];
		for (int i = 0; i < array.length; i++)
		{
			result[i] = array[i];
		}
		result[array.length] = element;
		return result;
	}
	
	public static boolean isIn(String[] arg0, String arg1)
	{
		for (int i = 0; i < arg0.length; i++)
		{
			if (arg1.equals(arg0[i]))
				return true;
		}
		return false;
	}
	
	public static void setup(String fileName)
	{
		try
		{
			pw = new PrintWriter(fileName);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public static void latexTitle(String algorithm, int size)
	{
		pw.print("% - - - - - - - - - - %"
			+ "\n%"
			+ "\n% Richard Shepherd"
			+ "\n% CS430 Spring 2015"
			+ "\n% Professor: Reingold"
			+ "\n%"
			+ "\n% - This is a binary tree displaying all of the possible outcomes"
			+ "\n%   of "+algorithm+" on a data set of "+size+" elements."
			+ "\n%"
			+ "\n% - - - - - - - - - - %"
			+ "\n\n\\documentclass{article}"
			+ "\n\\usepackage{pstricks}"
			+ "\n\\usepackage{pst-tree}"
			+ "\n\n\\begin{document}"
			+ "\n\n\\pstree[nodesep=0]{\\TR{ \\fbox{"+algorithm+"} }}{");
	}
	
	public static void latexCommitFile()
	{
		pw.print("}\n\n\\end{document}");
		pw.close();
	}
	
	public static void latexNode(int x, int y, int deep)
	{
		String output = spaces(deep);
		if (deep > 0)
			output += "\\pstree{\\Tcircle{"+x+":"+y+"}}";
		else
			output += "\\pstree[levelsep=25pt]{\\Tcircle{"+x+":"+y+"}}";
		pw.print(output);
	}
	
	public static void latexNull(int deep)
	{
		String output = spaces(deep);
		output += "\\Tn";
		pw.print(output);
	}
	
	public static void latexOpenBracket(int deep)
	{
		String output = spaces(deep);
		output += "{";
		pw.print(output);
	}
	
	public static void latexCloseBracket(int deep)
	{
		String output = spaces(deep);
		output += "}";
		pw.print(output);
	}
	
	public static void latexResult(String result, int deep)
	{
		String output = spaces(deep);
		output += "\\TR{\\fbox{"+result+"}}";
		pw.print(output);
	}
	
	private static String spaces(int deep)
	{
		String result = "\n";
		for (int i = 1; i <= deep; i++)
		{
			result += "    ";
		}
		return result;
	}
}
