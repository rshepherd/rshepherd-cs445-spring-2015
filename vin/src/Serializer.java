import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class Serializer
{
	//private members
	List<Subscriber> subList = null;
	List<Note> noteList      = null;
	List<Shipment> shipList = null;
	
	//default constructor
	public Serializer()
	{
		//List<Subscriber> derSubs = null;
	}
	
	public List<Subscriber> getSubs()
	{
		return this.subList;
	}
	public List<Note> getNotes()
	{
		return this.noteList;
	}
	public List<Shipment> getShipments()
	{
		return this.shipList;
	}
	
	//Subscriber methods
	public void serializeSubscriber(List<Subscriber> subs)
	{
		serializeSubscriber(subs, "subscribers.txt");
	}
	public void serializeSubscriber(List<Subscriber> subs, String fileName)
	{
		try 
		{
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(subs);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	public List<Subscriber> deserializeSubscriber()
	{
		return deserializeSubscriber("subscribers.txt");
	}
	@SuppressWarnings({ "unchecked", "resource" })
	public List<Subscriber> deserializeSubscriber(String fileName)
	{
		try
		{
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return subList;
			} else {
				ObjectInputStream in = new ObjectInputStream(inputFile);
				subList = (List<Subscriber>)in.readObject();
				in.close();
			}
			inputFile.close();
			
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Subscriber class could not be located");
			c.printStackTrace();
		} catch(Exception e) {
			System.out.println("Error message:\n" + e.getMessage());
			e.printStackTrace();
		}
		return subList;
	}
	
	//Note methods
	public void serializeNote(List<Note> notes)
	{
		serializeNote(notes, "notes.txt");
	}
	public void serializeNote(List<Note> notes, String fileName)
	{
		try 
		{
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(notes);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	public List<Note> deserializeNote()
	{
		return deserializeNote("notes.txt");
	}
	@SuppressWarnings({ "resource", "unchecked" })
	public List<Note> deserializeNote(String fileName)
	{
		try
		{
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return noteList;
			} else {
				//System.out.println(inputFile.available());
				ObjectInputStream in = new ObjectInputStream(inputFile);
				noteList = (List<Note>) in.readObject();
				in.close();
			}
			inputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Notes class could not be located");
			c.printStackTrace();
		}
		return noteList;
	}
		
	//Shipment methods
	public void serializeShipment(List<Shipment> ships)
	{
		serializeShipment(ships, "shipments.txt");
	}
	public void serializeShipment(List<Shipment> ships, String fileName)
	{
		try 
		{
			FileOutputStream outputFile = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(outputFile);
			out.writeObject(ships);
			out.close();
			outputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	public List<Shipment> deserializeShipment()
	{
		return deserializeShipment("shipments.txt");
	}
	@SuppressWarnings({ "resource", "unchecked" })
	public List<Shipment> deserializeShipment(String fileName)
	{
		try
		{
			FileInputStream inputFile = new FileInputStream(fileName);
			if(inputFile.available() == 0)
			{
				return shipList;
			} else {
				//System.out.println(inputFile.available());
				ObjectInputStream in = new ObjectInputStream(inputFile);
				shipList = (List<Shipment>)in.readObject();
				in.close();
			}
			inputFile.close();
		} catch(IOException i) {
			i.printStackTrace();
		} catch(ClassNotFoundException c) {
			System.out.println("Shipment class could not be located");
			c.printStackTrace();
		}
		return shipList;
	}
}