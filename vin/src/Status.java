import java.io.Serializable;

public enum Status implements Serializable
{
	//types of statuses
	C("Cancelled"),
	D("Delivered"),
	P("Pending"),
	R("Returned"),
	S("Shipped");
	
	//private variables
	private String desc;
	
	//constructor(s)
	private Status(String mes)
	{
		this.setDesc(mes);
	}
	
	//accessor(s)
	public String getDesc()
	{
		return desc;
	}

	//mutator(s)
	public void setDesc(String desc)
	{
		this.desc = desc;
	}
	
	public static Status getStatus(String stat)
	{
		if (stat.equals("C") || stat.equals(C.desc))
			return C;
		else if (stat.equals("D") || stat.equals(D.desc))
			return D;
		else if (stat.equals("P") || stat.equals(P.desc))
			return P;
		else if (stat.equals("R") || stat.equals(R.desc))
			return R;
		else
			return S;
	}
}
