import java.util.List;

public class NoteModify
{
	//private variables
	List<Note> noteList = null;
	Serializer ser = new Serializer();
	
	//constructor(s)
	public NoteModify()//int subID)
	{	
		//note.writeNote(note);
		noteList = ser.deserializeNote();
	}
	
	public String modifyByID(int subID, String content)
	{
		ser = new Serializer();
		for(int i =0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getID() == subID)
			{
				noteList.get(i).setContent(content);
				 ser.serializeNote(noteList);
				 return "Note was modified";
			}
		}
		return "Note could not be located";
	}
}
