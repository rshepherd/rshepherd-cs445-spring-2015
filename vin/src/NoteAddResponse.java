public class NoteAddResponse
{
	private int ID;
	private String error;
	private int code;
	
	public NoteAddResponse(int id, int code, String error)
	{
		this.ID = id;
		this.code = code;
		this.error = error;
	}
	
	public void printNoteAddResponse()
	{
		System.out.println("ID: " + ID + " Code: " + code + " error: " + error);
	}
}
