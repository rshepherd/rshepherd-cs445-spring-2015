import java.io.Serializable;

public class Wine implements Serializable
{
	//public variables
	private WineVariety wv;
	private WineType wt;
	private String labelName;
	private String grape;	// e.g. Merlot, Chardonnay, Riesling, etc.
	private String region;	// e.g. Napa, Russian Valley, etc.
	private String country; // e.g. France, USA, Australia, Chile
	private String maker;	// the wine maker, e.g. Sterling, Krupp Brother, etc.
	private int year;		// vintage year
	private int numberOfRatings = 0;
	private float rating = 0;
	private int ID;
	
	//default constructor
	public Wine()
	{
		this.labelName = "Bloody Mary";
		this.grape     = "Merlot";
		this.region    = "Whoville";
		this.country   = "Seussville";
		this.maker     = "The Dr.";
		this.year      = 1969;
		this.wt        = WineType.TABLE;
		this.wv        = WineVariety.ROSE;
		this.ID        = IDGenerator.generateID();
	}
	
	//constructor(s)
	public Wine(String labelname, String grape, String region, String country, 
		String maker, int year, WineType wts, WineVariety wvs)
	{
		this.labelName = labelname;
		this.grape = grape;
		this.region = region;
		this.country = country;
		this.maker = maker;
		this.year = year;
		this.wt = wts;
		this.wv = wvs;
		this.ID = IDGenerator.generateID();
	}
	
	//accessor(s)
	public String getLabelName()
	{
		return this.labelName;
	}
	public String getGrape()
	{
		return this.grape;
	}
	public String getRegion()
	{
		return this.region;
	}
	public String getCountry()
	{
		return this.country;
	}
	public String getMaker()
	{
		return this.maker;
	}
	public int getYear()
	{
		return this.year;
	}
	public WineType getWineType()
	{
		return this.wt;
	}
	public WineVariety getWineVariety()
	{
		return this.wv;
	}
	public int getNumberOfRatings()
	{
		return this.numberOfRatings;
	}
	public float getRating()
	{
		return this.rating;
	}
	public int getID()
	{
		return this.ID;
	}
	
	//mutator(s)
	public void setLabelName(String label)
	{
		this.labelName = label;
	}
	public void setGrape(String grape)
	{
		this.grape = grape;
	}
	public void setRegion(String region)
	{
		this.region = region;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public void setMaker(String maker)
	{
		this.maker = maker;
	}
	public void setWineType(WineType t)
	{
		this.wt = t;
	}
	public void setWineVariety(WineVariety v)
	{
		this.wv = v;
	}
	public void setYear(int y)
	{
		this.year = y;
	}
	
	public void addRating(float r)
	{
		numberOfRatings = numberOfRatings + 1;
		rating = rating*((float)(numberOfRatings - 1)/numberOfRatings) + (float)r/numberOfRatings;
	}
	
	//compares wine based on all variables
	public boolean isMatch(String kw)
	{
        if( isMatchVariety(kw) && 
        	isMatchType(kw)    && 
        	isMatchLabel(kw)   && 
        	isMatchGrape(kw)   && 
        	isMatchRegion(kw)  && 
        	isMatchCountry(kw) && 
        	isMatchMaker(kw)   && 
        	isMatchYear(kw) )
        {
            return true;
        } 
        else return false;
	}
	    
    private boolean isMatchVariety(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.wv.name().matches(regex);
    }

    private boolean isMatchType(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.wt.name().matches(regex);
    }
    
    private boolean isMatchLabel(String kw)
    {
        String regex = "(?i).*" + kw + ".*";
        return this.labelName.matches(regex);
    }
    
    private boolean isMatchGrape(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.grape.matches(regex);
    }
    
    private boolean isMatchRegion(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.region.matches(regex);
    }

    private boolean isMatchCountry(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.country.matches(regex);
    }

    private boolean isMatchMaker(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return this.maker.matches(regex);
    }

    private boolean isMatchYear(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
    	return (this.year+"").matches(regex);
    }
}
