import java.util.List;

public class NoteView
{
	//private variables
	List<Note> noteList = null;
	Serializer ser = new Serializer();
	
	//default constructor
	public NoteView()
	{
		noteList = ser.deserializeNote();
	}
	
	public String viewNoteByID(int noteID)
	{
		for(int i =0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getID() == noteID)
			{
				return noteList.get(i).ToString();
			}
		}
		return "Note could not be located";
	}
	
	public String viewNote()
	{
		if (noteList != null)
		{
			String temp = "";
			for(int i=0; i<noteList.size(); i++)
			{
				temp += noteList.get(i).ToString();
				temp += "\n";
			}
			return temp;
		}
		else
			return "Notes could not be found";
	}
	
	public Note getNote(int noteID)
	{
		if (noteList != null)
		{
			for(int i = 0; i < noteList.size(); i++)
			{
				if(noteList.get(i).getID() == noteID)
				{
					return noteList.get(i);
				}
			}
			return null;
		}
		return null;
	}
}