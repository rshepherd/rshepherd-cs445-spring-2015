import java.io.Serializable;
import java.time.YearMonth;

public class Shipment implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private members
	private int subID;
	private int noteID;
	private Status status;
	private YearMonth date;
	private int ID;
	//TODO: implement monthly selection instead
	private int wsID;
	
	//default constructor
	public Shipment()
	{
		this.subID  = 0;
		this.noteID = 0;
		this.status = Status.P;
		this.date   = YearMonth.now();
		this.ID     = IDGenerator.generateShipmentID();
		this.wsID   = 0;
	}
	
	//constructor(s)
	public Shipment(int subId, int noteId, int wsId, Status status, YearMonth date)
	{
		this.subID  = subId;
		this.noteID = noteId;
		this.status = status;
		this.date   = date;
		this.ID     = IDGenerator.generateShipmentID();
		this.wsID   = wsId;
	}
	
	//accessor(s)
	public Subscriber getSub()
	{
		SubscriberView sv = new SubscriberView();
		return sv.getSubscriber(this.subID);
	}
	public int getWineSelection()//WineSelection getWineSelction()
	{
		return this.wsID;
	}
	public int getID()
	{
		return this.ID;
	}
	public Note getNote()
	{
		NoteView nv = new NoteView();
		return nv.getNote(this.noteID);
	}
	public Status getStatus()
	{
		return this.status;
	}
	public YearMonth getDate()
	{
		return this.date;
	}
	
	//mutator(s)
	public void setSubcriber(int subId)
	{
		this.subID = subId;
	}
	public void setWineSelection(int wsId)
	{
		this.wsID = wsId;
	}
	public void setID(int ID)
	{
		this.ID = ID;
	}
	public void setNote(int noteId)
	{
		this.noteID = noteId;
	}
	public void setStatus(Status status)
	{
		this.status = status;
	}
	public void setDate(YearMonth date)
	{
		this.date = date;
	}
	
	public String toString()
	{
		SubscriberView sv = new SubscriberView();
		Subscriber scrub = sv.getSubscriber(this.subID);
		NoteView nv = new NoteView();
		Note nut = nv.getNote(this.subID);
		String result = "";
		result += "ID: " + this.ID + "\n";
		result += scrub.toString() + "\n";
		if(nut != null)
			result += "Note: " + nut.ToString() + "\n";
		else
			result += "Note: null\n";
		result += "Status: " + this.status.toString() + "\n";
		result += "Date: " + this.date.toString() + "\n";
		result += "Wine Selection: " + wsID;//this.ws.toString();
		return result;
	}
	
	//compares shipment based on its variables
	public boolean isMatch(Shipment ship)
	{
		if ( (this.subID == ship.subID) && (this.noteID == ship.noteID)
			&& (this.wsID == ship.wsID) && (this.status.equals(ship.status)) )
		{
			return true;
		}
		else
			return false;
	}
}