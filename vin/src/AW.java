public class AW extends MonthlySelection
{
	public AW()
	{
		super.ws = WineSelection.AW;
	}
	
	public AW(String ym)
	{
		super(ym);
		super.ws = WineSelection.AW;
	}
	
	@Override
	void addWine(Wine w)
	{
		if( w.getWineVariety().equals(WineVariety.WHITE) && wl.size()<6 )
		{
			wl.add(w);
		}
		else System.out.println("The type of wine was not white");
	}

	@Override
	void addPrice(double p)
	{
		price = p;
	}
}
