public abstract class SubscriberModifyRequest
{
	//subscriber variables
	String name, email, phone, facebook, twitter,ID;
	String state, city, zip, street;
	
	public SubscriberModifyRequest(String id,String name, String email, String phone, 
		String facebook, String twitter, String state, String city, String zip, String street)
	{
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.facebook = facebook;
		this.twitter = twitter;
		this.state = state;
		this.city = city;
		this.zip = zip;
		this.street = street;
		this.ID = id;
	}
	
	//methods
	public abstract SubscriberModifyResponse modifySubscriber(Subscriber subs);
}