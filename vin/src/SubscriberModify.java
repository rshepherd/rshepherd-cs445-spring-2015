import java.util.ArrayList;
import java.util.List;

public class SubscriberModify extends SubscriberModifyRequest
{
	//constructor(s)
	public SubscriberModify(String id, String name, String email, String phone, String facebook,
		String twitter, String state, String city, String zip, String street)
	{
		super(id, name, email, phone, facebook, twitter, state, city, zip, street);
	}

	//TODO: error handling
	public SubscriberModifyResponse modifySubscriber(Subscriber subs)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		int id = Integer.valueOf(this.ID);
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber();
		int loc = checkID(subList, id);
		if(loc != -1)
		{
			if(this.name == null || this.name.equals(""))
			{
				//name is empty
			} else {
				subList.get(loc).setName(this.name);
			}
			if(this.email == null || this.email.equals(""))
			{
				//email is empty
			} else {
				subList.get(loc).setEmail(this.email);
			}
			if(this.phone == null || this.phone.equals(""))
			{
				//phone is empty
			} else {
				subList.get(loc).setPhone(this.phone);
			}
			if(this.facebook == null || this.facebook.equals(""))
			{
				//facebook is empty
			} else {
				subList.get(loc).setFacebook(this.facebook);
			}
			if(this.twitter == null || this.twitter.equals(""))
			{
				//twitter is empty
			} else {
				subList.get(loc).setTwitter(this.twitter);
			}
			if(this.state == null || this.state.equals(""))
			{
				//state is empty
			} else {
				subList.get(loc).getAddress().setState(this.state);
			}
			if(this.state == null || this.state.equals(""))
			{
				//state is empty
			} else {
				subList.get(loc).getAddress().setCity(this.city);
			}
			if(this.zip == null || this.zip.equals(""))
			{
				//zip is empty
			} else {
				subList.get(loc).getAddress().setZip(this.zip);
			}
			if(this.street == null || this.street.equals(""))
			{
				//street is empty
			} else {
				subList.get(loc).getAddress().setStreet(this.street);
			}
			ser.serializeSubscriber(subList);
			subList.get(loc).toString();
			return new SubscriberModifyResponse(0, 1, "Account Updated");
		}
		else 
		{
			System.out.println("ID could not be located");
		}
		return null;
	}

	public int checkID(List<Subscriber> subList, int id)
	{
		for(int i = 0; i < subList.size(); i++)
		{
			if(subList.get(i).getID() == id)
			{
				return i;
			}
		}
		return -1;
	}
}

