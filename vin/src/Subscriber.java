import java.io.Serializable;

public class Subscriber implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private variables
	private String name, email, phone, facebook, twitter;
	private Address address;
	private int ID;
	private WineSelection ws;
	
	//default constructor
	public Subscriber(){
		this.name = "Jane Doe";
    	this.email = "jane.doe@example.com";
    	this.phone = "1234567890";
    	this.address = new Address();
    	this.facebook = "";
		this.twitter = "";
    	this.ID = IDGenerator.generateID();
		this.ws = WineSelection.AR;
	}
	
	//constructor(s)
	public Subscriber(String name, String email, String phone, String facebook,
		String twitter, Address address)
	{
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.facebook = facebook;
		this.twitter = twitter;
		this.ID = IDGenerator.generateID();
		this.ws = WineSelection.AR;
	}
	public Subscriber(String name, String email, String phone, Address address)
	{
		this.name     = name;
		this.email    = email;
		this.phone    = phone;
		this.facebook = "";
		this.twitter  = "";
		this.address  = address;
		this.ID       = IDGenerator.generateID();
		this.ws       = WineSelection.AR;
	}
	
	//accessor(s)
	public String getName()
	{
		return this.name;
	}
	public String getEmail()
	{
		return this.email;
	}
	public String getPhone()
	{
		return this.phone;
	}
	public String getFacebook()
	{
		return this.facebook;
	}
	public String getTwitter()
	{
		return this.twitter;
	}
	public int getID()
	{
		return this.ID;
	}
	public WineSelection getSelction()
	{
		return this.ws;
	}
	public Address getAddress()
	{
		return this.address;
	}
	
	//mutator(s)
	public void setName(String name)
	{
		this.name = name;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	public void setFacebook(String facebook)
	{
		this.facebook = facebook;
	}
	public void setTwitter(String twitter)
	{
		this.twitter = twitter;
	}
	public void setWineSelection(WineSelection ws)
	{
		this.ws = ws;
	}
	public void setAddress(Address address)
	{
		this.address = address;
	}
	public void setID(int id)
	{
		this.ID = id;
	}
	
	//toString
	public String toString()
	{
		return  "Subscriber:"      +
				"\n    ID: "       + this.ID +
				"\n    Name: "     + this.name +
				"\n    Phone: "    + this.phone +
				"\n    Email: "    + this.email +
				"\n    Facebook: " + this.facebook +
				"\n    Twitter: "  + this.twitter +
				"\n"   + this.getAddress().toString() +
				"\nWineSelction: " + this.getSelction().toString();
	}

	public void updateSubscriber(String name, String email, Address address, String phone)
	{
		this.name = name;
		this.email = email;
		this.address = address;
		this.phone = phone;
	}
	
	//compares subscriber by checking its variables
	public boolean isMatch(Subscriber sub)
	{
		if( this.name.equals(sub.name) &&
			this.email.equals(sub.email) &&
			this.phone.equals(sub.phone) &&
			this.ID==(sub.ID) )
		{
			return true;
		}
		else
			return false;
	}
}
