import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

public class ShipmentModify extends ShipmentModifyRequest
{
	//constructor(s)
	public ShipmentModify(int subId, int noteId, int wsId, Status st, YearMonth ym, int id)
	{
		super(subId, noteId, wsId, st, ym, id);
	}

	//TODO: error handling
	public ShipmentModifyResponse modifyShipment(Shipment ships)
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		int id = Integer.valueOf(this.ID);
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment();
		int loc = checkID(shipList, id);
		if(loc != -1)
		{
			if(this.subID == -1)
			{
				//sub DNE
			} else {
				shipList.get(loc).setSubcriber(subID);
			}
			if(this.noteID == -1)
			{
				//note DNE
			} else {
				shipList.get(loc).setNote(noteID);
			}
			if(this.wsID == -1)
			{
				//ws DNE
			} else {
				shipList.get(loc).setWineSelection(wsID);
			}
			if(this.status == null || this.status.equals(""))
			{
				//status DNE
			} else {
				shipList.get(loc).setStatus(this.status);
			}
			if(this.yearMonth == null)
			{
				//ym DNE
			} else {
				shipList.get(loc).setDate(this.yearMonth);
			}
			ser.serializeShipment(shipList);
			shipList.get(loc).toString();
			return new ShipmentModifyResponse(0, 1, "Account Updated");
		}
		else 
		{
			System.out.println("ID could not be located");
		}
		return null;
	}

	public int checkID(List<Shipment> shipList, int id)
	{
		for(int i = 0; i < shipList.size(); i++)
		{
			if(shipList.get(i).getID() == id)
			{
				return i;
			}
		}
		return -1;
	}
}

