import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Note implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	//private members
	private int ID;
	private String content;
	private int subID;
	
	//default constructor
	public Note()
	{
		this.content = "";
	}
	
	//constructor(s)
	public Note(String note)
	{
		this.content = note;
	}
	
	//accessor(s)
	public String getContent()
	{
		return this.content;
	}
	public int getSubID()
	{
		return this.subID;
	}
	public int getID()
	{
		return this.ID;
	}
	
	//mutator(s)
	public void setID(int id)
	{
		this.ID = id;
	}
	public void setSubID(int id)
	{
		this.subID = id;
	}
	public void setContent(String content)
	{
		
		if(checkNote(content) == true)
		{
			this.content = content;
		}
		else System.out.println("Note is not the correct length");
	}
	
	//checks length
	public static boolean checkNote(String content)
	{
		if( (content.length()>=128) && (content.length()<=1024) )
			return true;
		else
			return false;
	}
	
	public void writeNote(Note note)
	{
		List<Note> notes = new ArrayList<Note>();
		Serializer ser = new Serializer();
		notes = ser.deserializeNote();
		if(notes == null)
		{
			notes = new ArrayList<Note>();
		}
		notes.add(note);
		ser.serializeNote(notes);
	}
	
	public String ToString()
	{
		return "\nContent: " + this.content +
			   "\nId: "      + this.subID;
	}
	
	//compares address given all of its variables
	public boolean isMatch(Note note)
	{
		if( isMatchContent(note.content) &&
			this.subID == (note.subID) )
		{
			return true;
		}
		else
			return false; 
	}
	
	private boolean isMatchContent(String kw)
    {
    	String regex = "(?i).*" + kw + ".*";
        return this.content.matches(regex);
    }
}
