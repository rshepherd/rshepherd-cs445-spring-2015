import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.time.*;

public abstract class MonthlySelection
{
	//private members
	protected WineSelection ws;
	private YearMonth date;
	protected List<Wine> wl = new ArrayList<Wine>();
	protected double price;
	
	//methods
	abstract void addWine(Wine w);
	abstract void addPrice(double price);
	public boolean isMatch(String kw)
	{
		Iterator<Wine> it = this.wl.iterator();
		while (it.hasNext())
		{
			Wine w = it.next();
			if (w.isMatch(kw))
				return true;
		}
		return false;
	}
	
	//constructor(s)
	public MonthlySelection()
	{
		//next month's selection
		this.date = YearMonth.now().plusMonths(1);
	}
	
	public MonthlySelection(String ym)
	{
		//Must be in the yyyy-mm format
		this.date = YearMonth.parse(ym);
	}
	
	//accessor(s)
	public YearMonth getDate()
	{
		return this.date;
	}	
	public double getPrice()
	{
		return this.price;
	}
}
