public class NoteCreate
{
	//private members
	private Note note = new Note();
	
	//constructor(s)
	public NoteCreate(String content, int subID)
	{
		note.setContent(content);
		note.setID(subID);
		//writes to serialized file
		note.writeNote(note);
	}
}
