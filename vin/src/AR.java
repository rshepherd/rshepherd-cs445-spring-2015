public class AR extends MonthlySelection
{
	public AR()
	{
		super.ws = WineSelection.AR;
	}
	
	public AR(String ym)
	{
		super(ym);
		super.ws = WineSelection.AR;
	}
	
	@Override
	void addWine(Wine w)
	{
		//Make sure only Red wines are added
		if( ( w.getWineVariety().equals(WineVariety.RED) 
			|| w.getWineVariety().equals(WineVariety.ROSE) )
			&& wl.size()<6 ){
			wl.add(w);
		}
		else System.out.println("The type of wine was not red");
	}

	@Override
	void addPrice(double p)
	{
		price = p;
	}
}
