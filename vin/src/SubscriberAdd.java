import java.util.ArrayList;
import java.util.List;

public class SubscriberAdd extends SubscriberAddRequest
{
	//private members
	private Address add;
	private Subscriber sub;
	List<Subscriber> subList = new ArrayList<Subscriber>();
	
    //constructor(s)
	public SubscriberAdd(String name, String email, String phone, String facebook, 
		String twitter, String state, String city, String zip, String street)
	{
		super(name, email, phone, facebook, twitter, state, city, zip, street);
	}
	
	public List<Subscriber> getSubscribers()
	{
		return subList;
	}
	
	//TODO: 1001, 1003, 1004, 1005, 1011, 1013-1016
	public SubscriberAddResponse addSubscriber(Subscriber subs)
	{
		Serializer ser = new Serializer();
		add = new Address(this.street, this.city, this.zip, this.state);
		sub = new Subscriber(this.name, this.email, this.phone, this.facebook, this.twitter, this.add);
		subList = ser.deserializeSubscriber();
		if(subList == null)
		{
			if(sub.getName() == null || sub.getName().equals(""))
			{
				return new SubscriberAddResponse(0,1000,"Name must be provided");
			}
			if(sub.getEmail() == null ||sub.getEmail().equals(""))
			{
				return new SubscriberAddResponse(0,1002,"Email must be provided");
			}
			if(sub.getAddress().getCity().equals("") || sub.getAddress().getCity() == null)
			{
				return new SubscriberAddResponse(0,1006,"City must be provided");
			}
			if(sub.getAddress().getState().equals("") ||sub.getAddress().getState() == null)
			{
				return new SubscriberAddResponse(0,1008,"State must be provided");
			}
			if(bannedState(add))
			{
				return new SubscriberAddResponse(0,1009,"We may not ship to this state"); 
			}
			if(sub.getAddress().getZip().equals("") || sub.getAddress().getZip() == null)
			{
				return new SubscriberAddResponse(0,1010,"ZIP code must be provided");
			}
			if(sub.getPhone() == null ||sub.getPhone().equals(""))
			{
				return new SubscriberAddResponse(0,1012,"Phone number must be provided");
			}
			subList = new ArrayList<Subscriber>();
			int id = IDGenerator.generateID();
			sub.setID(id);
			subList.add(sub);
			ser.serializeSubscriber(subList);
			return new SubscriberAddResponse(sub.getID(),1, "Account Created");
		} else {
			//for(int i = 0; i < subList.size(); i++)
			//{
			if(sub.getName() == null || sub.getName().equals(""))
			{
				return new SubscriberAddResponse(0,1000,"Name must be provided");
			}
			if(sub.getEmail() == null ||sub.getEmail().equals(""))
			{
				return new SubscriberAddResponse(0,1002,"Email must be provided");
			}
			if(sub.getAddress().getCity().equals("") || sub.getAddress().getCity() == null)
			{
				return new SubscriberAddResponse(0,1006,"City must be provided");
			}
			if(sub.getAddress().getState().equals("") ||sub.getAddress().getState() == null)
			{
				return new SubscriberAddResponse(0,1008,"State must be provided");
			}
			if(bannedState(add))
			{
				return new SubscriberAddResponse(0,1009,"We may not ship to this state"); 
			}
			//if(subList.get(i).getAddress().getZip().equals("") || subList.get(i).getAddress().getZip() == null)
			//{
			//	return new SubscriberAddResponse(0,1010,"ZIP Code must be provided");
			//}
			if(sub.getAddress().getZip().equals("") || sub.getAddress().getZip() == null)
			{
				return new SubscriberAddResponse(0,1010,"ZIP Code must be provided");
			}
			if(sub.getPhone() == null ||sub.getPhone().equals(""))
			{
				return new SubscriberAddResponse(0,1012,"Phone number must be provided");
			} 
			//}
			int id = IDGenerator.generateID();
			sub.setID(id);
			subList.add(sub);
			ser.serializeSubscriber(subList);
			return new SubscriberAddResponse(sub.getID(), 1, "Account Created");
		} //return new SubscriberAddResponse(1,1,"Test outside of bounds");
	}	
	
	public boolean bannedState(Address ad)
	{
		for(State state : State.values())
		{
			//banned state if passes
			if( ad.getState().equals(state) || ad.getState().equals(state.getAbbrev()) )
				return true;
		}
		//not a banned state
		return false;
	}
	
	public boolean bannedState(String st)
	{
		for(State state : State.values())
		{
			//banned state if passes
			if( st.equals(state) || st.equals(state.getAbbrev()) )
				return true;
		}
		//not a banned state
		return false;
	}
	
	public boolean isMatch(Subscriber sub1, Subscriber sub2)
	{
		if(sub1.isMatch(sub2))
			return true;
		return false;
    }
	
}
