import java.util.List;

public class NoteDelete
{
	//private variables
	List<Note> noteList = null;
	Serializer ser = new Serializer();	
	
	//constructor(s)
	public NoteDelete()
	{
		noteList = ser.deserializeNote();
	}
	
	public String deleteByID(int subID)
	{
		ser = new Serializer();	
		for(int i = 0; i < noteList.size(); i++)
		{
			if(noteList.get(i).getID() == subID)
			{
				 noteList.remove(i);
				 ser.serializeNote(noteList);
				 return "Note was deleted";
			}
		}
		return "Note could not be located";
	}
}
