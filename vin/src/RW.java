public class RW extends MonthlySelection
{
	public RW()
	{
		super.ws = WineSelection.RW;
	}
	
	public RW(String ym)
	{
		super(ym);
		super.ws = WineSelection.RW;
	}
	
	@Override
	void addWine(Wine w)
	{
		if( wl.size()<6 )
		{
			wl.add(w);
		}
		else
			System.out.println("Too many wines");
	}

	@Override
	void addPrice(double p)
	{
		price = p;
	}
}
