//import java.io.Serializable;

public enum State
{
	//types of states
	Alabama       ("AL"),
	Arkansas      ("AR"),
	Delaware      ("DE"),
	Kentuky       ("KY"),
	Massachusetts ("MA"),
	Mississippi   ("MS"),
	Oklahoma      ("OK"),
	Pensilvania   ("PA"),
	SouthDakota   ("SD"),
	Utah          ("UT");
	
	//variable(s)
	private String abbrev;
	
	//constructor(s)
	private State(String abbrev)
	{
		this.setAbbrev(abbrev);
	}

	//accessor(s)
	public String getAbbrev()
	{
		return abbrev;
	}

	//mutator(s)
	public void setAbbrev(String abbrev)
	{
		this.abbrev = abbrev;
	}
}
