public class SubscriberAddResponse
{
	private int ID;
	private String error;
	private int code;
	
	public SubscriberAddResponse(int id, int code, String error)
	{
		this.ID = id;
		this.code = code;
		this.error = error;
	}
	
	public void printSubscriberAddResponse()
	{
		System.out.println("ID: " + ID + " Code: " + code + " error: " + error);
	}
}
