public class ShipmentModifyResponse
{	
	//private members
	private int ID;
	private String error;
	private int code;
	
	public ShipmentModifyResponse(int id, int code, String error)
	{
		this.ID = id;
		this.code = code;
		this.error = error;
	}
	
	public void printShipmentModifyResponse()
	{
		System.out.println("ID: " + ID + " Code: " + code + " error: " + error);
	}
	//public abstract void usedbyAddSub();
}