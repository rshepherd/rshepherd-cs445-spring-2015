import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadFromFile
{
	//private members
	private String fn;
	
	//constructor(s)
	public ReadFromFile(String fileName)
	{	
		this.fn = fileName;
	}
	
	public ArrayList<String> readSubscriber()
	{
		String name, email, phone, state, zip, city, street, wineSelection, facebook, twitter; 
		@SuppressWarnings("unused")
		WineType wt;
		@SuppressWarnings("unused")
		WineVariety wv;
		ArrayList<String> contents = new ArrayList<String>();
		File checkFile = new File(this.fn);
        if (!checkFile.exists())
        {
        	//TODO: error code AND exit handler
            System.err.println("File could not be located");
            System.exit(0);
        }
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new FileReader(this.fn));
            @SuppressWarnings("unused")
			String line;
            while ((line = br.readLine()) != null)
            {
                name          =  line;
                contents.add(name);
                email         = br.readLine();
                contents.add(email);
                phone         = br.readLine();
                contents.add(phone);
                state         = br.readLine();
                contents.add(state);
                zip           = br.readLine();
                contents.add(zip);
                city          = br.readLine();
                contents.add(city);
                street        = br.readLine();
                contents.add(street);
                wineSelection =  br.readLine();
                contents.add(wineSelection);
                facebook      = br.readLine();
                contents.add(facebook);
                twitter       = br.readLine();
                contents.add(twitter);
            }
            return contents;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try
            {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }    
        }
		return contents;
	}
	
	public ArrayList<String> readWine()
	{
		String wname, labelname, grape, region, country, maker, year, wt, wv; 
		ArrayList<String> contents = new ArrayList<String>();
		File checkFile = new File(this.fn);
        if (!checkFile.exists()) 
        {
            System.err.println("error - file does not exist");
            //TODO: implement exit handler
            System.exit(0); //terminate not-so-gracefully
        }
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new FileReader(this.fn));
            @SuppressWarnings("unused")
			String line;
            while ((line = br.readLine()) != null)
            {
                wname     =  br.readLine();
                contents.add(wname);
                labelname = br.readLine();
                contents.add(labelname);
                grape     = br.readLine();
                contents.add(grape);
                region    = br.readLine();
                contents.add(region);
                country   = br.readLine();
                contents.add(country);
                maker     = br.readLine();
                contents.add(maker);
                year      = br.readLine();
                contents.add(year);
                wt        =  br.readLine();
                contents.add(wt);
                wv        = br.readLine();
                contents.add(wv);
            }
            return contents;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try
            {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		return contents;
	}
}