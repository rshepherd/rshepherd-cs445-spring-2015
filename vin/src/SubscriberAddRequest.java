public abstract class SubscriberAddRequest
{
	//subscriber variables
	String name, email, phone, facebook, twitter;
	String state, city, zip, street;
	
	//constructor
	public SubscriberAddRequest(String name, String email, String phone, 
		String state, String city, String zip, String street, String facebook, String twitter)
	{
		this.name     = name;
		this.email    = email;
		this.phone    = phone;
		this.city     = city;
		this.state    = state;
		this.zip      = zip;
		this.street   = street;
		this.facebook = facebook;
		this.twitter  = twitter;
	}
	
	//methods
	public abstract SubscriberAddResponse addSubscriber(Subscriber subs);
}