import java.util.List;

public class SubscriberDelete
{
	//private variables
	List<Subscriber> subList = null;
	Serializer ser = new Serializer();	
	
	//constructor(s)
	public SubscriberDelete()
	{
		subList = ser.deserializeSubscriber();
	}
	
	public String deleteByID(int subID)
	{
		ser = new Serializer();	
		for(int i = 0; i < subList.size(); i++)
		{
			if(subList.get(i).getID() == subID)
			{
				 subList.remove(i);
				 ser.serializeSubscriber(subList);
				 return "Subscriber was deleted";
			}
		}
		return "Subscriber could not be located";
	}
}
