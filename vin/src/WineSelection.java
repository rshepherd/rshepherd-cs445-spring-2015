public enum WineSelection
{
	//types of wine selections
	AR ("all reds"), AW ("all whites"), RW ("reds and whites");
	
	//variable(s)
	private String desc;
	
	//constructor(s)
	private WineSelection(String mes)
	{
		this.setDesc(mes);
	}
	
	//accessor(s)
	public String getDesc()
	{
		return desc;
	}
	
	//mutator(s)
	public void setDesc(String desc)
	{
		this.desc = desc;
	}
	
	public static WineSelection getWS(String ws)
	{
		if ((ws.equals("AR") || ws.equals(AR.desc)))
			return AR;
		else if ((ws.equals("AW") || ws.equals(AW.desc)))
			return AW;
		else
			return RW;
	}
}
