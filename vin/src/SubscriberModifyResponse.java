public class SubscriberModifyResponse
{	
	//private members
	private int ID;
	private String error;
	private int code;
	
	public SubscriberModifyResponse(int id, int code, String error)
	{
		this.ID = id;
		this.code = code;
		this.error = error;
	}
	
	public void printSubscriberModifyResponse()
	{
		System.out.println("ID: " + ID + " Code: " + code + " error: " + error);
	}
	//public abstract void usedbyAddSub();
}