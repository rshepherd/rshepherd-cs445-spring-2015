import java.time.YearMonth;

public abstract class ShipmentAddRequest
{
	//subscriber variables
	Subscriber subscriber;
	Note note;
	Status status;
	YearMonth date;
	int ID;
	int ws;//WineSelection ws;
	
	//constructor
	public ShipmentAddRequest(Subscriber subscriber, Note note, Status status,
			YearMonth date, int id, int ws)//WineSelection ws)
	{
		this.subscriber = subscriber;
		this.note       = note;
		this.status     = status;
		this.date       = date;
		this.ID         = id;
		this.ws         = ws;
	}
	
	//methods
	public abstract ShipmentAddResponse addShipment(Shipment ship);
}