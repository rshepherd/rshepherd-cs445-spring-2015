import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

public class ShipmentAdd extends ShipmentAddRequest
{
	//private members
	//private Shipment ship;
	List<Shipment> shipList = new ArrayList<Shipment>();
	
    //constructor(s)
	public ShipmentAdd(Subscriber subscriber, Note note,
		Status status, YearMonth date, int dID, int ws)//WineSelection ws)
	{
		super(subscriber, note, status, date, dID, ws);
		//ship = new Shipment(subscriber, note, status, date, ws);
	}
	
	public ShipmentAdd(Shipment ship)
	{
		super(ship.getSub(), ship.getNote(), ship.getStatus(), ship.getDate(),
			ship.getID(), ship.getWineSelection());
		//this.ship = ship;
	}
	
	public List<Shipment> getShipments()
	{
		return shipList;
	}
	
	//TODO: 1001, 1003, 1004, 1005, 1011, 1013-1016
	public ShipmentAddResponse addShipment(Shipment ship)
	{
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment();
		if(shipList == null)
		{
			/*if(sub.getName() == null || sub.getName().equals(""))
			{
				return new SubscriberAddResponse(0,1000,"Name must be provided");
			}
			if(sub.getEmail() == null ||sub.getEmail().equals(""))
			{
				return new SubscriberAddResponse(0,1002,"Email must be provided");
			}
			if(sub.getAddress().getCity().equals("") || sub.getAddress().getCity() == null)
			{
				return new SubscriberAddResponse(0,1006,"City must be provided");
			}
			if(sub.getAddress().getState().equals("") ||sub.getAddress().getState() == null)
			{
				return new SubscriberAddResponse(0,1008,"State must be provided");
			}
			if(bannedState(add))
			{
				return new SubscriberAddResponse(0,1009,"We may not ship to this state"); 
			}
			if(sub.getAddress().getZip().equals("") || sub.getAddress().getZip() == null)
			{
				return new SubscriberAddResponse(0,1010,"ZIP code must be provided");
			}
			if(sub.getPhone() == null ||sub.getPhone().equals(""))
			{
				return new SubscriberAddResponse(0,1012,"Phone number must be provided");
			}*/
			shipList = new ArrayList<Shipment>();
			int id = IDGenerator.generateShipmentID();
			ship.setID(id);
			shipList.add(ship);
			ser.serializeShipment(shipList);
			return new ShipmentAddResponse(ship.getID(),1, "Account Created");
		} else {
			/*
			if(sub.getName() == null || sub.getName().equals(""))
			{
				return new SubscriberAddResponse(0,1000,"Name must be provided");
			}
			if(sub.getEmail() == null ||sub.getEmail().equals(""))
			{
				return new SubscriberAddResponse(0,1002,"Email must be provided");
			}
			if(sub.getAddress().getCity().equals("") || sub.getAddress().getCity() == null)
			{
				return new SubscriberAddResponse(0,1006,"City must be provided");
			}
			if(sub.getAddress().getState().equals("") ||sub.getAddress().getState() == null)
			{
				return new SubscriberAddResponse(0,1008,"State must be provided");
			}
			if(bannedState(add))
			{
				return new SubscriberAddResponse(0,1009,"We may not ship to this state"); 
			}
			//if(subList.get(i).getAddress().getZip().equals("") || subList.get(i).getAddress().getZip() == null)
			//{
			//	return new SubscriberAddResponse(0,1010,"ZIP Code must be provided");
			//}
			if(sub.getAddress().getZip().equals("") || sub.getAddress().getZip() == null)
			{
				return new SubscriberAddResponse(0,1010,"ZIP Code must be provided");
			}
			if(sub.getPhone() == null ||sub.getPhone().equals(""))
			{
				return new SubscriberAddResponse(0,1012,"Phone number must be provided");
			} 
			*/
			int id = IDGenerator.generateID();
			ship.setID(id);
			shipList.add(ship);
			ser.serializeShipment(shipList);
			return new ShipmentAddResponse(ship.getID(), 1, "Shipment Added");
		}
	}	
	
	public boolean isMatch(Shipment ship1, Shipment ship2)
	{
		if(ship1.isMatch(ship2))
			return true;
		return false;
    }
	
}
