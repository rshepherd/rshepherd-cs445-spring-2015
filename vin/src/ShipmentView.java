import java.util.ArrayList;
import java.util.List;

public class ShipmentView
{
	//private variables
	private Shipment ship;
	
	//default constructor
	public ShipmentView()
	{
		this.setShip(null);
	}
	
	//accessor(s)
	public Shipment getShip()
	{
		return ship;
	}

	//mutator(s)
	public void setShip(Shipment ship)
	{
		this.ship = ship;
	}
	
	public int checkID(List<Shipment> shipList, int id)
	{	
		for(int i = 0; i < shipList.size(); i++)
		{
			if(shipList.get(i).getID() == id )
			{
				return i;
			}
		}
		return -1;
	}

	public String viewInfo(int id)
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		String temp;
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment();
		int loc = checkID(shipList, id);
		if(loc != -1)
		{
			for(int i = 0; i < shipList.size(); i++)
			{
				if (shipList.get(i).getID() == id)
				{
					Shipment ship = shipList.get(i);
					temp = ship.toString();
					return temp;
				}
			}
		}
		return "Shipment could not be located";
	}
	
	public String viewInfo()
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment();
		if (shipList != null)
		{
			String temp = "";
			for(int i = 0; i < shipList.size(); i++)
			{
				temp += viewInfo(shipList.get(i).getID());
				temp += "\n";
			}
			return temp;
		}
		else
			return "No shipments in the file";
	}
	
	public String viewInfo(String fileName)
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment(fileName);
		if (shipList != null)
		{
			String temp = "";
			for(int i = 0; i < shipList.size(); i++)
			{
				temp += viewInfo(fileName, i);
				temp += "\n";
			}
			return temp;
		}
		else
			return "No shipments in the file";
	}
	
	public String viewInfo(String fileName, int id)
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		String temp;
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment(fileName);
		int loc = checkID(shipList, id);
		if(loc != -1)
		{
			for(int i = 0; i < shipList.size(); i++)
			{
				if (shipList.get(i).getID() == id)
				{
					Shipment ship = shipList.get(i);
					temp = ship.toString();
					return temp;
				}
			}
		}
		return "Shipment could not be located";
	}
	
	public Shipment getShipment(int id)
	{
		List<Shipment> shipList = new ArrayList<Shipment>();
		Shipment temp;
		Serializer ser = new Serializer();
		shipList = ser.deserializeShipment();
		int loc = checkID(shipList, id);
		if(loc != -1)
		{
			temp = shipList.get(loc);
			return temp;
		} else {
			return null;
		}
	}
}
