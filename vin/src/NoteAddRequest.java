public abstract class NoteAddRequest
{
	//subscriber variables
	String content;
	int subID;
	
	//constructor
	public NoteAddRequest(String cont, int subId)
	{
		this.content = cont;
		this.subID = subId;
	}
	
	//methods
	public abstract NoteAddResponse addNote(Note note);
}