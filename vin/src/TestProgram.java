import java.util.ArrayList;
//import java.util.List;
import java.time.YearMonth;

import org.apache.commons.cli.*;

public class TestProgram
{
	public static void main(String[] args)
	{
		//Using Apache CLI to parse user arguments
		Options options = new Options();
		//variables
		options.addOption("n", true, "subscriber\'s name");
		options.addOption("e", true, "subscriber\'s email");
		options.addOption("h", true, "subscriber\'s phone number");
		options.addOption("f", true, "subscriber\'s Facebook");
		options.addOption("t", true, "subscriber\'s Twitter");
		// address
		options.addOption("a", true, "subscriber address\' street");
		options.addOption("c", true, "subscriber address\' city");
		options.addOption("s", true, "subscriber address\' state");
		options.addOption("z", true, "subscriber address\' zip");
		// shipment
		options.addOption("id",  true, "user\'s id");
		options.addOption("nid", true, "note\'s id");
		options.addOption("msid", true, "monthly selection\'s id");
		options.addOption("st",  true, "shipment\'s status");
		options.addOption("m",   true, "shipment\'s month");
		options.addOption("y",   true, "shipment\'s year");
		options.addOption("note",   true, "shipment\'s note");
		//options.addOption("ws",  true, "shipment\'s Wine Selection");
		//other args
		options.addOption("add", true, "for adding a subscriber");
		options.addOption("uid", true, "for viewing a subscriber by ID");
		options.addOption("fn",  true, "file name");
		
		Subscriber subs  = null;
		Shipment   ships = null;
		Note       notes = null;
		try
		{
			CommandLine line = new BasicParser().parse(options, args);
			String name      = line.getOptionValue("n");
			String email     = line.getOptionValue("e");
			String address   = line.getOptionValue("a");
			String city      = line.getOptionValue("c");
			String state     = line.getOptionValue("s");
			String zip       = line.getOptionValue("z");
			String facebook  = line.getOptionValue("f");
			String twitter   = line.getOptionValue("t");
			String phone     = line.getOptionValue("h");
			String id        = line.getOptionValue("uid");
			String fileName  = line.getOptionValue("fn");
			//Shipment
			String subId  = line.getOptionValue("id");
			String noteId = line.getOptionValue("nid");
			String msId   = line.getOptionValue("msid");
			String status = line.getOptionValue("st");
			String month  = line.getOptionValue("m");
			String year   = line.getOptionValue("y");
			String note   = line.getOptionValue("note");
			//String wineS  = line.getOptionValue("ws");
			
			String addl = line.getOptionValue("add");
			int    uid  = 0;
			int    sid  = 0;
			int    nid  = 0;
			int    msid = 0;
			int    y    = 1999;
			int    m    = 1;
			
			if (id != null)
				uid = Integer.valueOf(id);
			else
				uid = -1;
			if (subId != null)
				sid = Integer.valueOf(subId);
			else
				sid = -1;
			if (noteId != null)
				nid = Integer.valueOf(noteId);
			if (msId != null)
				msid = Integer.valueOf(msId);
			else
				msid = -1;
			if (year != null)
				y = Integer.valueOf(year);
			if (month != null)
				m = Integer.valueOf(month);
			String arg0 = args[0].toLowerCase();
			String arg1 = "";
			if (args.length > 1)
				arg1 = args[1].toLowerCase();
			if (arg0.equals("subscriber") || arg0.equals("subscribers")) 
			{
				if (arg1.equals("add"))
				{
					//subscriber add -n hello -e we.built@this.city -a on -c rock -s and -z roll -h 1 -f 2 -t 3
					//subscriber add -n "martina blatsch" -e androgyny@feminism.net -a clit -c ableist -s confusion -z 12345 -h 1 -f fembot -t sjw
					SubscriberAddRequest sar = new SubscriberAdd(name, email,
						phone, facebook, twitter, state, city, zip, address);
					SubscriberAddResponse saResp = sar.addSubscriber(subs);
					saResp.printSubscriberAddResponse();
				}
				else if (arg1.equals("modify"))
				{
					//subscriber modify -uid 0 -n barbara
					SubscriberModifyRequest smr = new SubscriberModify(id, name,
						email, phone, facebook, twitter, state, city, zip, address);
					SubscriberModifyResponse smResp = smr.modifySubscriber(subs);
					smResp.printSubscriberModifyResponse();
				}
				else if (arg1.equals("view"))
				{
					SubscriberView subView = new SubscriberView();
					String sub;
					if (uid != -1)
						sub = subView.viewInfo(uid);
					else
						sub = subView.viewInfo();
					System.out.println(sub);
				}
				else if (arg1.equals("delete"))
				{
					SubscriberDelete sd = new SubscriberDelete();
					System.out.println(sd.deleteByID(uid));
				}
				else if (arg1.equals("load"))
				{
					//subscriber load -fn subscribers.txt
					SubscriberView subView = new SubscriberView();
					String sub = subView.viewInfo(fileName);
					System.out.println(sub);
				}
				else if (arg1.equals("search"))
				{
					//TODO - finish this later
				}
			}
			else if (arg0.equals("shipments") || arg0.equals("shipment"))
			{
				if (arg1.equals("view"))
				{
					ShipmentView shipView = new ShipmentView();
					String ship;
					if (sid != -1)
						ship = shipView.viewInfo(sid);
					else
						ship = shipView.viewInfo();
					System.out.println(ship);
				}
				else if (arg1.equals("load"))
				{
					//subscriber load -fn subscribers.txt
					ShipmentView shipView = new ShipmentView();
					String ship = shipView.viewInfo(fileName);
					System.out.println(ship);
				}
				else if (arg1.equals("modify"))
				{
					Status stat = Status.getStatus(status);
					YearMonth ym = YearMonth.of(y, m);
					ShipmentModifyRequest smr = new ShipmentModify(uid, nid, msid, stat, ym, sid);
					ShipmentModifyResponse smResp = smr.modifyShipment(ships);
					smResp.printShipmentModifyResponse();
				}
			}
			else if (arg0.equals("note") || arg0.equals("notes"))
			{
				if ((nid == -1) && (uid != -1))
					nid = uid;
				if (arg1.equals("view"))
				{
					NoteView noteView = new NoteView();
					note = noteView.viewNoteByID(nid);
					System.out.println(note);
				}
				else if (arg1.equals("add"))
				{
					//new NoteCreate(note, nid);
					NoteAddRequest nar = new NoteAdd(note, uid);
					NoteAddResponse naResp = nar.addNote(notes);
					naResp.printNoteAddResponse();
				}
				else if (arg1.equals("modify"))
				{
					NoteModify nm = new NoteModify();
					nm.modifyByID(nid, addl);
				}
				else if (arg1.equals("delete"))
				{
					NoteDelete nd = new NoteDelete();
					System.out.println(nd.deleteByID(nid));
				}
			}
			else if (arg0.equals("delivery") || arg0.equals("deliveries"))
			{
				if (arg1.equals("view"))
				{
					//TODO: finish
				}
				else if (arg1.equals("modify"))
				{
					//TODO: finish
				}
			}
			else if (arg0.equals("admin"))
			{
				//admin add_shipment -id 2 -nid 1 -st C -m 1 -y 2007 -ws RW
				if (arg1.equals("add_shipment"))
				{
					Status stat = Status.getStatus(status);
					YearMonth ym = YearMonth.of(y, m);
					Shipment ship = new Shipment(uid, nid, msid, stat, ym);
					ShipmentAddRequest saq = new ShipmentAdd(ship);
					ShipmentAddResponse sResp = saq.addShipment(ship);
					sResp.printShipmentAddResponse();
					
				}
				//admin add_monthly_selection -t type -fn exampleMonthlySelection.txt
				else if (arg1.equals("add_monthly_selection"))
				{
					ArrayList<String> selection;
					ReadFromFile rff = new ReadFromFile("examplemonthlySelection.txt");
					selection = rff.readWine();
					String wt = selection.get(6);
					String wv = selection.get(7);
					Wine wine = new Wine( selection.get(0), selection.get(1),
						selection.get(2), selection.get(3), selection.get(4),
						Integer.parseInt(selection.get(5)), WineType.valueOf(wt), WineVariety.valueOf(wv) );
					AR ar = new AR();
					RW rw = new RW();
					AW aw = new AW();
					//TODO: implement better args naming system
					if (twitter.equals("AR"))
					{
						ar.addWine(wine);
					}
					if (twitter.equals("AW"))
					{
						aw.addWine(wine);
					}
					if (twitter.equals("RW"))
					{
						rw.addWine(wine);
					}
					//TODO: add wine selection
				}
				//TODO: revenue, search, view_monthly_selection
			}
			else if (arg0.equals("partner"))
			{
				if (arg1.equals("subscriber_list"))
				{
					SubscriberView subView = new SubscriberView();
					String sub;
					sub = subView.viewInfo();
					System.out.println(sub);
				}
				//TODO: add_receipt, view_receipt
			}
			
			//TODO: wines, delivery
			
		}
		catch (ParseException exp)
		{
		    System.out.println( "Unexpected exception: " + exp.getMessage() );
		}
	}
}


