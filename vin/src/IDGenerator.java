import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class IDGenerator
{
	private static AtomicInteger IDnum = new AtomicInteger();
	
	public static int generateID()
	{
		//return IDnum.incrementAndGet();
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber();
		if (subList != null)
		{
			return subList.get(subList.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
	
	public static int generateNoteID()
	{
		List<Note> list = new ArrayList<Note>();
		Serializer ser = new Serializer();
		list = ser.deserializeNote();
		if (list != null)
		{
			return list.get(list.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
	
	public static int generateShipmentID()
	{
		List<Shipment> list = new ArrayList<Shipment>();
		Serializer ser = new Serializer();
		list = ser.deserializeShipment();
		if (list != null)
		{
			return list.get(list.size() - 1).getID() + 1;
		}
		else
			return 0;
	}
}
