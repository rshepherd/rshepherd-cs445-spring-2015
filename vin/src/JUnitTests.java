import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class JUnitTests
{
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();
	 String sep = System.getProperty("line.separator");
	
	@Before
	public void setPrintStream()
	{
	    System.setOut(new PrintStream(output));
	}
	
	@Test
	public void testAddress()
	{
		Address addDefault = new Address();
		Address add1 = new Address("SomeStreet", "SomeCity", "SomeZip", "SomeState");
		Address add2 = new Address("Homeless", "Homeless", "Homeless", "Homeless");
		assertEquals("123 Main ST, Apt 2F", addDefault.getStreet());
		add2.setStreet("SomeStreet"); add2.setCity("SomeCity");
		add2.setZip("SomeZip"); add2.setState("SomeState");
		assertTrue(add1.isMatch(add2));
		assertFalse(add1.isMatch(addDefault));
	}
	
	@Test
	public void testWine()
	{
		Wine wineDefault    = new Wine();
		YearMonth ym = YearMonth.now().plusMonths(1);
		
		wineDefault.setLabelName("defaultname");
		assertEquals("defaultname",wineDefault.getLabelName());
		wineDefault.setGrape("defaultgrape");
		assertEquals("defaultgrape",wineDefault.getGrape());
		wineDefault.setRegion("defaultRegion");
		assertEquals("defaultRegion",wineDefault.getRegion());
		wineDefault.setCountry("defaultCountry");
		assertEquals("defaultCountry", wineDefault.getCountry());
		wineDefault.setMaker("defaultMaker");
		assertEquals("defaultMaker", wineDefault.getMaker());
		wineDefault.setWineType(WineType.SWEET);
		assertEquals(WineType.SWEET, wineDefault.getWineType());
		wineDefault.setWineVariety(WineVariety.RED);
		assertEquals(WineVariety.RED, wineDefault.getWineVariety());
		wineDefault.setYear(1969);
		assertEquals("1969", wineDefault.getYear());
		
		//TODO: Red/White checks for AW and AR, number checks for all 3, isMatch
	}
	
	@Test
	public void testSubscriber()
	{
		String name   = "Jane Doe";
		String email  = "jane.doe@example.com";
		String phone  = "1234567890";
		String face   = "";
		String twit   = "";
		String state  = "AnyState";
		String city   = "AnyTown";
		String zip    = "12345";
		String street = "123 Main ST, Apt 2F";
		
		Address addDefault = new Address();
		Subscriber subDefault = new Subscriber();
		Address add1 = new Address(street, city, zip, state);
		Subscriber sub1 = new Subscriber(name, email, phone, face, twit, add1);
		
		//TODO: isMatch
	}
	
	//may be conflated with delivery
	@Test
	public void testShipment()
	{
		//TODO
	}
	
	@Test
	public void testDelivery()
	{
		//TODO:
	}
	
	@Test
	public void testNote()
	{
		//TODO
	}
	
	@Test
	public void testSubscriberMethods() throws IOException
	{
		PrintWriter pw = new PrintWriter("subscribers.txt");
		//overwrite file and close stream
		pw.print("");
		pw.close();
		
		String name   = "Jane Doe";
		String email  = "jane.doe@example.com";
		String phone  = "1234567890";
		String face   = "";
		String twit   = "";
		String state  = "AnyState";
		String city   = "AnyTown";
		String zip    = "12345";
		String street = "123 Main ST, Apt 2F";
		
		SubscriberAddRequest sar = new SubscriberAdd(name, email, phone, 
			face, twit, state, city, zip, street);
		SubscriberAddResponse saResp = sar.addSubscriber(null);
		saResp.printSubscriberAddResponse();
		assertEquals("ID: 1 Code: 1 error: Account Created" + sep,  output.toString());
		output.reset();
		//TODO: create another subscriber to see if id increments
		//TODO: create a subscriber with invalid arguments to check error handling
		
		//assertEquals("ID: 0 Code: 1000 error: Name must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1001 error: Bad name" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1002 error: Email address must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1003 error: Invalid email address" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1004 error: Address must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1005 error: Bad address" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1006 error: State must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1007 error: Bad city" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1008 error: State must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1010 error: ZIP code must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1011 error: Bad ZIP code" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1012 error: Phone number must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1013 error: Invalid ID" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1014 error: An account with this email address exists" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1015 error: File name must be provided" + sep,  output.toString());
			//output.reset();
		//assertEquals("ID: 0 Code: 1016 error: File not found" + sep,  output.toString());
			//output.reset();
	}
	
	//May have this conflated with delivery
	@Test
	public void testShipmentsMethods()
	{
		//TODO
	}
	
	@Test
	public void testNoteMethods()
	{
		//TODO
	}
	
	@Test
	public void testWineMethods()
	{
		//TODO
	}
	
	@Test
	public void testDeliveryMethods()
	{
		//TODO
	}
	
	@Test
	public void testAdminMethods()
	{
		//TODO
	}
	
	@Test
	public void testPartnerMethods()
	{
		//TODO
	}
}
