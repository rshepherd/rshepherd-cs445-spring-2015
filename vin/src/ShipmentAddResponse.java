public class ShipmentAddResponse
{
	private int ID;
	private String error;
	private int code;
	
	public ShipmentAddResponse(int id, int code, String error)
	{
		this.ID = id;
		this.code = code;
		this.error = error;
	}
	
	public void printShipmentAddResponse()
	{
		System.out.println("ID: " + ID + " Code: " + code + " error: " + error);
	}
}
