import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SubscriberToSerialized
{
	//default
	public void serializeSubscriber(Subscriber sub)
	{
		serializeSubscriber(sub, "subscriber.txt");
	}
	
	//specific
	public void serializeSubscriber(Subscriber sub, String fileName)
	{
		try 
		{
			//set output to the file where subscribers are stored
			FileOutputStream outputFile = new FileOutputStream(fileName, true);
			ObjectOutputStream out      = new ObjectOutputStream(outputFile);
			//write serialized data to a file
			out.writeObject(sub);
			//close streams
			out.close();
			outputFile.close();
		} catch(IOException ioEx) {
			System.out.println(ioEx.getMessage());
		}
	}
}
