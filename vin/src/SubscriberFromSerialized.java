import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class SubscriberFromSerialized
{
	//default
	public Subscriber GetFromFile()
	{
		return GetFromFile("subscriber.txt");
	}
	
	//specific
	public Subscriber GetFromFile(String fileName)
	{
		//instantiate
		Subscriber sub = new Subscriber();
		//work with serialized file
		try
		{
			//set input to the file where subscribers are stored
			FileInputStream inputFile = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(inputFile);
			//read subscriber from the serialized file
			sub = (Subscriber)in.readObject();
			//close streams
			in.close();
			inputFile.close();
		} catch(IOException ioEx) {
			System.out.println(ioEx.getMessage());
		} catch(ClassNotFoundException cnfEx) {
			System.out.println("Subscriber class could not be located");
			System.out.println(cnfEx.getMessage());
		}
		return sub;
	}

}
