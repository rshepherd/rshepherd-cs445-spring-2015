import java.util.ArrayList;
import java.util.List;

public class NoteAdd extends NoteAddRequest
{
	//private members
	List<Note> noteList = new ArrayList<Note>();
	
    //constructor(s)
	public NoteAdd(String cont, int subId)
	{
		super(cont, subId);
	}
	
	public List<Note> getNotes()
	{
		return noteList;
	}
	
	//TODO: Error handling
	public NoteAddResponse addNote(Note note)
	{
		Serializer ser = new Serializer();
		noteList = ser.deserializeNote();
		if(noteList == null)
		{
			if (!Note.checkNote(content))
			{
				//content incorrect
			}
			if (subID == -1)
			{
				//subscriber DNE
			}
			noteList = new ArrayList<Note>();
			int id = IDGenerator.generateNoteID();
			note.setID(id);
			noteList.add(note);
			ser.serializeNote(noteList);
			return new NoteAddResponse(note.getID(), 1, "Account Created");
		} else {
			if (!Note.checkNote(content))
			{
				//content incorrect
			}
			if (subID == -1)
			{
				//subscriber DNE
			}
			int id = IDGenerator.generateNoteID();
			note.setID(id);
			noteList.add(note);
			ser.serializeNote(noteList);
			return new NoteAddResponse(note.getID(), 1, "Account Created");
		} //return new SubscriberAddResponse(1,1,"Test outside of bounds");
	}	
	
	public boolean bannedState(Address ad)
	{
		for(State state : State.values())
		{
			//banned state if passes
			if( ad.getState().equals(state) || ad.getState().equals(state.getAbbrev()) )
				return true;
		}
		//not a banned state
		return false;
	}
	
	public boolean bannedState(String st)
	{
		for(State state : State.values())
		{
			//banned state if passes
			if( st.equals(state) || st.equals(state.getAbbrev()) )
				return true;
		}
		//not a banned state
		return false;
	}
	
	public boolean isMatch(Subscriber sub1, Subscriber sub2)
	{
		if(sub1.isMatch(sub2))
			return true;
		return false;
    }
	
}
