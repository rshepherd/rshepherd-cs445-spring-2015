import java.util.ArrayList;
import java.util.List;

public class SubscriberView
{
	//private variables
	private Subscriber sub;
	
	//default constructor
	public SubscriberView()
	{
		this.setSub(null);
	}
	
	//accessor(s)
	public Subscriber getSub()
	{
		return sub;
	}

	//mutator(s)
	public void setSub(Subscriber sub)
	{
		this.sub = sub;
	}
	
	public int checkID(List<Subscriber> subList, int id)
	{	
		for(int i = 0; i < subList.size(); i++)
		{
			if(subList.get(i).getID() == id )
			{
				return i;
			}
		}
		return -1;
	}

	public String viewInfo(int id)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		String temp;
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber();
		int loc = checkID(subList, id);
		if(loc != -1)
		{
			temp = subList.get(id).toString();
			return temp;
		} else {
			return "Subscriber could not be located";
		}
	}
	
	public Subscriber getSubscriber(int id)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Subscriber temp;
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber();
		int loc = checkID(subList, id);
		if(loc != -1)
		{
			temp = subList.get(id);
			return temp;
		} else {
			return null;
		}
	}
	
	public String viewInfo()
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber();
		if (subList != null)
		{
			String temp = "";
			for(int i = 0; i < subList.size(); i++)
			{
				temp += viewInfo(i);
				temp += "\n";
			}
			return temp;
		}
		else
			return "No subscribers in the file";
	}
	
	public String viewInfo(String fileName)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		if (subList != null)
		{
			String temp = "";
			for(int i = 0; i < subList.size(); i++)
			{
				temp += viewInfo(i);
				temp += "\n";
			}
			return temp;
		}
		else
			return "No subscribers in the file";
	}
	
	public String viewInfo(String fileName, int id)
	{
		List<Subscriber> subList = new ArrayList<Subscriber>();
		String temp;
		Serializer ser = new Serializer();
		subList = ser.deserializeSubscriber(fileName);
		int loc = checkID(subList, id);
		if(loc != -1)
		{
			temp = subList.get(id).toString();
			return temp;
		} else {
			return "Subscriber could not be located";
		}
	}
}
