
public class TestMain 
{
	public static void main(String[] args) 
	{
		// Test ImprovedRandom
		ImprovedRandom test = new ImprovedRandom();
		System.out.println(test.nextInt(3, 10));
		long seed = 100;
		ImprovedRandom test2 = new ImprovedRandom(seed);
		System.out.println(test2.nextInt(3, 10));
		
		// Test ImprovedStringTokenizer
		ImprovedStringTokenizer tst = new ImprovedStringTokenizer("hello world");
		String result[] = tst.allTokens();
		for(int i = 0; i < result.length; i++)
		{
			System.out.println(result[i]);
		}
		tst = new ImprovedStringTokenizer("hello,world", ",");
		result = tst.allTokens();
		for(int i = 0; i < result.length; i++)
		{
			System.out.println(result[i]);
		}
		tst = new ImprovedStringTokenizer("hello,world", ",", true);
		result = tst.allTokens();
		for(int i = 0; i < result.length; i++)
		{
			System.out.println(result[i]);
		}
	}
}