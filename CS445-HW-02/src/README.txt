Makefile commands and usage:
	"make": compiles the TestCreature file
	"make clean": gets rid of *.class files
	"make test": compiles the JUnit test
	"make all": compiles all of the files
	"make runtest": runs the JUnit test
Running programs:
	TestMain: If you have java installed, type "java TestMain"
	JUnit Test: Either type "make runtest"
	... or type "java -cp .:junit.jar:hamcrest.jar org.junit.runner.JUnitCore JUnitTesting"