import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.*;
import static org.junit.Assert.*;

public class JUnitTesting {

	// Classes
	long seed = 100;
	ImprovedRandom          irTest1  = new ImprovedRandom();
	ImprovedRandom          irTest2  = new ImprovedRandom(seed);
	ImprovedStringTokenizer istTest1 = new ImprovedStringTokenizer("a,b,c d");
	ImprovedStringTokenizer istTest2 = new ImprovedStringTokenizer("a,b,c d", ",");
	ImprovedStringTokenizer istTest3 = new ImprovedStringTokenizer("a,b,c d", ",", true);
	A a     = new A();
	B b     = new B();
	C c     = new C();
	D d     = new D();
	D ds1[] = {d, d, d};
	D ds2[] = {d};
	E e     = new E();
	F f     = new F();
	F fs1[] = {f, f, f, f, f, f};
	F fs2[] = {f};
	D d2    = new D(b, fs1);
	D d3    = new D(b, fs2);
	F f2    = new F(ds1);
	F fs3   = new F(ds2);
	
	// Captures the output for comparison for unit testing
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();
	String lineSep = System.getProperty("line.separator");
	
	@Before
	public void setUpStreams()
	{
		// redirects output to our byte array so we can compare it
		System.setOut(new PrintStream(output));
	}
	
	@Test
	public void TestImprovedRandom()
	{
		int result = irTest1.nextInt(3,9);
		assertTrue((result >= 3) && (result <= 9));
		result = irTest2.nextInt(20,-11);
		assertTrue((result >= -11) && (result <= 20));
	}
	
	@Test
	public void TestImprovedStringTokenizer()
	{
		String result1[] = {"a,b,c", "d"};
		assertArrayEquals(istTest1.allTokens(), result1);
		String result2[] = {"a", "b", "c d"};
		assertArrayEquals(istTest2.allTokens(), result2);
		String result3[] = {"a", ",", "b", ",", "c d"};
		assertArrayEquals(istTest3.allTokens(), result3);
	}
	
	@Test
	public void TestLetters()
	{
		// returns null
		a.useB(b);
		c.useD(d);
	}
	
	@Test
	public void TestMain()
	{
		// main method is already a test of sorts... should be fine if there's no error thrown
		TestMain.main(null);
	}
	
}
