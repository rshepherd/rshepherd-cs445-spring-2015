
public class F {
	// Object(s)
	//HW2 - F is associated with 2 D's
	D myDs[] = new D[2];
	
	// Constructor(s)
	public F() {}
	
	public F(D[] newD) 
	{
		int i = 0;
		while ((i < newD.length) && (i < 2)) 
		{
			myDs[i] = newD[i++];
		}
		
	}
}
