//HW2 - C is a subclass of A
public class C extends A {
	// Constructor(s)
	public C() {}
	
	// Method(s)
	//HW2 - C uses D
	public void useD(D someD) {}
}
