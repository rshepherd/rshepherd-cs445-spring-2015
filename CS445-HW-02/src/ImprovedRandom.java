import java.util.Random;

@SuppressWarnings("serial")
public class ImprovedRandom extends Random {
	// Constructor(s)
	public ImprovedRandom() {}
	
	public ImprovedRandom(long seed)
	{
		super(seed);
	}
	
	// Method(s)
	public int nextInt(int arg0, int arg1)
	{
		int min = (arg0 < arg1) ? arg0 : arg1;
		int max = (arg0 > arg1) ? arg0 : arg1;
		return (nextInt(max - min + 1) + min);
	}
}
