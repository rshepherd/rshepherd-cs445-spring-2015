import java.util.StringTokenizer;

public class ImprovedStringTokenizer extends StringTokenizer {
	// Constructor(s)
	public ImprovedStringTokenizer(String str)
	{
		super(str);
	}

	public ImprovedStringTokenizer(String str, String delim)
	{
		super(str, delim);
	}
	
	public ImprovedStringTokenizer(String str, String delim, boolean returnDelims)
	{
		super(str, delim, returnDelims);
	}
	
	// Method(s)
	public String[] allTokens()
	{
		int size = countTokens();
		String tokens[] = new String[size];
		for(int i = 0; i < size; i++)
		{
			tokens[i] = nextToken();
		}
		return tokens;
	}
}
