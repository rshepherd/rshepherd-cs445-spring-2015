
public class D {
	// Object(s)
	//HW2 - D is associated with 1 B
	B myB;
	//HW2 - D is associated with 5 F's
	F myFs[] = new F[5];
	
	// Constructor(s)
	public D() {}
	
	public D(B newB, F[] newF) 
	{
		myB = newB;
		int i = 0;
		while ((i < newF.length) && (i < 5)) 
		{
			myFs[i] = newF[i++];
		}
	}
}
